<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>C62StampInvoiceApprovalDate</fullName>
        <field>Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>C62StampInvoiceApprovalDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StampPaymentDateWhenPaid</fullName>
        <description>Stamp Payment Date when Invoice has been paid</description>
        <field>Payment_Date__c</field>
        <formula>TODAY()</formula>
        <name>StampPaymentDateWhenPaid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Member_Email</fullName>
        <field>Customer_Email__c</field>
        <formula>Customer__r.Email</formula>
        <name>Update Member Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>C62ApprovedInvoice</fullName>
        <actions>
            <name>C62StampInvoiceApprovalDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISBLANK( Approval_Date__c ) &amp;&amp; Approved__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>C62Invoice Amount Paid</fullName>
        <actions>
            <name>StampPaymentDateWhenPaid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( Amount_Paid__c ) &amp;&amp; Amount_Paid__c &gt;= Original_Invoice_Amount__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>C62InvoiceCreatedOrMemberChanged</fullName>
        <actions>
            <name>Update_Member_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISNEW() || ISCHANGED(Customer__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
