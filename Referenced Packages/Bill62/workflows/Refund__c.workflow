<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>C62_Member_Refund_Refunded</fullName>
        <description>C62 Member Refund Refunded</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bill6_Email_Templates/C62MemberRefundReceiptTemplate</template>
    </alerts>
    <alerts>
        <fullName>RefundReceiptEmailAlert</fullName>
        <description>RefundReceiptEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bill6_Email_Templates/C62RefundReceiptEmail</template>
    </alerts>
    <fieldUpdates>
        <fullName>C62_Populate_Member_Email</fullName>
        <field>Customer_Email__c</field>
        <formula>Customer__r.Email</formula>
        <name>C62 Populate Member Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Member_Refund_Approval_Date</fullName>
        <field>Approval_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Member Refund Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Member_Refund_Check_Approved</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Member Refund Check Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Member_Refund_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Member Refund Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Member_Refund_Status_Denied</fullName>
        <field>Status__c</field>
        <literalValue>Denied</literalValue>
        <name>Member Refund Status Denied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>C62Member Refund Refunded</fullName>
        <actions>
            <name>C62_Member_Refund_Refunded</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>C62_Populate_Member_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c,&apos;Refunded&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Refund Receipt</fullName>
        <actions>
            <name>RefundReceiptEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c,&apos;Approved&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
