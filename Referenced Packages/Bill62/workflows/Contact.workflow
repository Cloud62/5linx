<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JoinDateToday</fullName>
        <field>Join_Date__c</field>
        <formula>today()</formula>
        <name>JoinDateToday</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>C62Fill Member Id on Create</fullName>
        <actions>
            <name>JoinDateToday</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Member is created fill in the appropriate member id</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
