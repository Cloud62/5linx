<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Payment_Receipt</fullName>
        <description>Email Payment Receipt</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bill6_Email_Templates/C62Payment_Receipt_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Manual_Email_Payment_Receipt</fullName>
        <description>Manual Email Payment Receipt</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bill6_Email_Templates/C62Payment_Receipt_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Member_Email</fullName>
        <field>Customer_Email__c</field>
        <formula>Customer__r.Email</formula>
        <name>Set Member Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>C62EmailReceipt</fullName>
        <actions>
            <name>Manual_Email_Payment_Receipt</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( ISCHANGED( Send_Receipt_Email__c ) , ISPICKVAL( Status__c , &quot;Paid&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>C62Payment Paid</fullName>
        <actions>
            <name>Email_Payment_Receipt</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c,&apos;Paid&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>C62Set Member Email</fullName>
        <actions>
            <name>Set_Member_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(  Customer__r.Email &lt;&gt; Customer_Email__c,  ISNEW()  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rejected Payment</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Payment__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>If a payment has been rejected too many times, this workflow rule will catch it and add it to Rejected Payment Queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
