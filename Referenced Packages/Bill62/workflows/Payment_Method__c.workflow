<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>C62Payment_Method_Notification_Alert</fullName>
        <description>C62Payment Method Notification Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bill6_Email_Templates/C62PaymentMethodExpirationNotification</template>
    </alerts>
</Workflow>
