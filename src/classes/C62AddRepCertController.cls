/*
@Name            : C62AddRepCertController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Add Rep Certification page
*/


public class C62AddRepCertController {
    public Bill62__Order__c theOrder {get;set;}
    public List<Bill62__Subscription__c> theOrderLines {get;set;}
    public String result {get;set;}
    public String errorMsg {get;set;}
    public String winMsg {get;set;}
    public Date expDate {get;set;}
    public Profile pr {get;set;}
    public Boolean isCSR {get;set;}
    public Boolean isCert {get;set;}
    public String certType {get;set;}
    
    public C62AddRepCertController(ApexPages.StandardController controller) {
         theOrder  = [SELECT Id, Bill62__Account__c, Certification_Expiration_Date__c, Order_IDtxt__c FROM Bill62__Order__c WHERE Id = :controller.getId() LIMIT 1];
         theOrderLines = [SELECT Id, Bill62__Product__r.External_ID__c, Bill62__Product__r.Name 
                          FROM Bill62__Subscription__c 
                          WHERE Order__c = :theOrder.Id AND (Bill62__Product__r.External_ID__c = '1600' OR Bill62__Product__r.External_ID__c = '1602' OR Bill62__Product__r.External_ID__c = '1604' OR Bill62__Product__r.External_ID__c = '1605' OR Bill62__Product__r.External_ID__c = '1615')];
         errorMsg = null;   
         isCSR = false;
         isCert = false;
         Id pId = userinfo.getProfileId();
         pr = [select Name from profile where id = :pId];
         if (pr.Name == '5Linx CSR') isCSR = true;  
         for (Bill62__Subscription__c ol : theOrderLines){
            if (ol.Bill62__Product__r.External_ID__c == '1600' || 
                ol.Bill62__Product__r.External_ID__c == '1602' || 
                ol.Bill62__Product__r.External_ID__c == '1604' || 
                ol.Bill62__Product__r.External_ID__c == '1605'){
                isCert = true;
                certType = 'Year';
            }
            if (ol.Bill62__Product__r.External_ID__c == '1615'){
                isCert = true;
                certType = 'Month';
            }
         } 
    }
    
    public PageReference AddCert(){
           
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"intOrderId":"'+theOrder.Order_IDtxt__c+'"}';        
                       
         String path = apiHelp.URL__c + 'AddManualRepCertification';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         String result;
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);
             result = res.getBody();
         } else {
             result = 'certification does not exist Certification added Current certification is still active and it expires on 08/12/15';
         }
    
        if (result.contains('Certification added')){
            List<Rep_Certification__c> existingRC = [SELECT Expiration_Date__c FROM Rep_Certification__c WHERE Rep__c = :theOrder.Bill62__Account__c];
            if (existingRC.size() > 0){
                if (certType == 'Year') existingRC[0].Expiration_Date__c = system.today().addYears(1);
                if (certType == 'Month') existingRC[0].Expiration_Date__c = system.today().addMonths(1);
            } else {
                Rep_Certification__c rc = New Rep_Certification__c();
                    rc.Name = 'Energy';
                    rc.Date_Completed__c = system.today();
                    rc.Rep__c = theOrder.Bill62__Account__c;
                    rc.Order_ID__c = theOrder.Order_IDtxt__c;
                    if (certType == 'Year') rc.Expiration_Date__c = system.today().addYears(1);
                    if (certType == 'Month') rc.Expiration_Date__c = system.today().addMonths(1);
                insert rc;
                errorMsg = null;
                winMsg = 'Certification Successfully Added';
            }
        } else if (result.contains('Current certification is still active')){
            result = result.substring(result.indexof('expires on')+11,result.indexof('expires on')+21 );
            Date myDate = date.newinstance(integer.valueof(result.substring(6)),integer.valueof(result.substring(0,2)),integer.valueof(result.substring(3,5)));
            List<Rep_Certification__c> existingRC = [SELECT Expiration_Date__c FROM Rep_Certification__c WHERE Rep__c = :theOrder.Bill62__Account__c];
            existingRC[0].Expiration_Date__c = myDate;
            update existingRC[0];
            errorMsg = result;
            winMsg = null;
        } else if (result.contains('certification does not exist')){
            errorMsg = 'Certification does not exist.';
            winMsg = null;
        }
        return null;
    }
    
    public PageReference Cancel(){
        return new PageReference('/' + theOrder.Id);
    }
}