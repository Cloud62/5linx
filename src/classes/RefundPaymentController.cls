/*
@Name            : RefundPaymentController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Refund page.  Refunds existing Payments in Payflow
*/


public with sharing class RefundPaymentController {
    public Bill62__Payment__c thePayment{get;set;}
    public Bill62__Payment_Method__c thePayMeth{get;set;}
    public Decimal paymentAmount{get;set;}
    public Decimal refundAmount{get;set;}
    public Boolean display{get;set;}
    public Boolean message{get;set;}
    public Boolean voidAuth{get;set;}

    public RefundPaymentController(ApexPages.StandardController sc){
        display=true;
        message = false;
        List<Bill62__Payment__c> payList = [SELECT Id,Name,Bill62__Customer__c,Bill62__Customer__r.FirstName, 
        Bill62__Customer__r.Customer_ID__c,  Bill62__Subscription__c, TC_Integration_Error__c, Bill62__Status__c,
        Bill62__Order__r.Bill62__Account__r.RIN__c, Bill62__Order__r.Bill62__Account__r.Entity_ID__c, Bill62__Recurring_Payment__c,  
        Bill62__Customer__r.Address__r.Country__r.Country_ID__c,Bill62__Customer__r.Address__r.Bill62__City__c, Bill62__Customer__r.Address__r.Bill62__Street_1__c, 
        Bill62__Customer__r.Address__r.Bill62__State_Province__c, Bill62__Customer__r.Address__r.Bill62__Zip_Postal_Code__c, Bill62__Customer__r.HomePhone, 
        Bill62__Subscription__r.Order_ID__c, Bill62__Transaction_Id__c, Bill62__Approval_Code__c, Bill62__Currency_Code__c,
        Bill62__Subscription__r.Order__r.Order_IDtxt__c, Bill62__Subscription__r.Bill62__Product__c, Bill62__Payment_Date__c,
        Bill62__Order__r.Order_Id__c, Bill62__Subscription__r.Order_Line_ID__c, Bill62__Subscription__r.Bill62__Product__r.External_ID__c, 
        Bill62__Amount__c,Bill62__Payment_Method__c,Bill62__Payment_Method__r.Bill62__Card_Holder_Name__c, Bill62__Description__c,
        Bill62__Subscription__r.Bill62__Product__r.Has_Tax__c, Bill62__Subscription__r.Bill62__Product__r.Has_Shipping__c, External_ID__c,
        Bill62__Customer__r.LastName,Bill62__Number_Of_Authorization_Attempts__c, Bill62__Payment_Gateway__c, Bill62__Payment_Gateway__r.Name 
        FROM Bill62__Payment__c WHERE ID = :sc.getId()];
        if(payList.size() > 0) thePayment = payList[0];
        if(thePayment.Bill62__Status__c != 'Paid'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You can only refund Paid Payments!'));
            display=false;
            message = true;
        }
        
        List<Bill62__Payment_Method__c> payMethList = [SELECT ID, Bill62__Card_Type__c,Bill62__Card_Number__c,
         Bill62__CVV_Code__c, Bill62__Expiration_Date__c, Bill62__Billing_Address__r.Bill62__Street_1__c,
         Bill62__Billing_Address__r.Bill62__Street_2__c, Bill62__Billing_Address__r.Bill62__Street_3__c, 
         Bill62__Billing_Address__r.Bill62__City__c, Bill62__Billing_Address__r.Bill62__State_Province__c, 
         Bill62__Billing_Address__r.Bill62__Zip_Postal_Code__c, Bill62__Billing_Address__r.Bill62__Country__c,
         Bill62__Payment_Type__c, Name, Default__c, Payment_Method_ID__c, Bill62__Card_Holder_Name__c, 
         Bill62__Billing_Address__c, Bill62__Customer__r.FirstName, Bill62__Customer__r.LastName
         FROM Bill62__Payment_Method__c WHERE ID = :thePayment.Bill62__Payment_Method__c];// AND Default__c = TRUE LIMIT 1];
        if(payMethList.size() > 0) thePayMeth = payMethList[0];
        
        paymentAmount = thePayment.Bill62__Amount__c;
        refundAmount = paymentAmount;
        
        //check whether to void or authorize
        try{
            Bill62.PaymentUtilities utils = new Bill62.PaymentUtilities();
            Bill62.C62PaymentGatewayClasses.InquiryResponse result = utils.Inquiry(thePayment);
            if(result != null){
                if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                    Set<String> voidNumbers = new Set<String>{'3','6','9','10','12','14'};
                    Set<String> creditNumbers = new Set<String>{'0','7','8'};
                    if(voidNumbers.contains(result.NonStandardValues.get('TRANSSTATE'))){
                        voidAuth = true;
                    } else if(creditNumbers.contains(result.NonStandardValues.get('TRANSSTATE'))){
                        voidAuth = false;
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Inquiry request format error!'));
                    display=false;
                    message = true;
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Inquiry Error: '+e.getMessage()));
            display=false;
            message = true;
        }
        
        if(voidAuth == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Inquiry Error! Please manually check Paypal.'));
            display=false;
            message = true;
        }
    }
    
    public pageReference makeRefund(){
        message = false;
        Bill62.PaymentUtilities utils = new Bill62.PaymentUtilities();
        
        if(refundAmount > thePayment.Bill62__Amount__c){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The refund amount cannot be greater than the payment amount!'));
            message = true;
            return null;
        }
        
        try{
            if(voidAuth){
                Bill62.C62PaymentGatewayClasses.VoidAuthorizationResponse result = utils.voidAuthorization(thePayment);
                if(result != null){
                    if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                        //If successful mark as Approved
                        thePayment.Bill62__Status__c='Refunded';
                        thePayment.Bill62__Amount_Refunded__c = refundAmount;
                        thePayment.Bill62__Transaction_Id__c = result.TransactionID;
                        thePayment.Bill62__Approval_Code__c = result.AuthorizationID;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Void Successful'));
                        message = true;
                        if(C625LinxAPIHelper.createRefundPaymentEntry(thePayment.External_ID__c, refundAmount, 
                            thePayment.Bill62__Approval_Code__c, thePayment.Bill62__Transaction_Id__c)){
                        
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Void pushed to TC'));
                        } else {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Void not pushed to TC'));
                        }
                    } else {
                        //If not successful mark as Rejected
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,result.ErrorDescription));
                        message = true;
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Void Failed'));
                    message = true;
                }
            }else{
                Bill62.C62PaymentGatewayClasses.RefundPaymentResponse result = utils.RefundPayment3(thePayMeth, 
                    thePayment, refundAmount, null, thePayment.Bill62__Transaction_ID__c);
                if(result != null){
                    if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                        //If successful mark as Approved
                        thePayment.Bill62__Status__c='Refunded';
                        thePayment.Bill62__Amount_Refunded__c = refundAmount;
                        thePayment.Bill62__Transaction_Id__c = result.TransactionID;
                        thePayment.Bill62__Approval_Code__c = result.AuthorizationID;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Refund Successful'));
                        message = true;
                        if(C625LinxAPIHelper.createRefundPaymentEntry(thePayment.External_ID__c, 
                            refundAmount, thePayment.Bill62__Approval_Code__c, thePayment.Bill62__Transaction_Id__c)){
                        
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Refund pushed to TC'));
                        } else {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Refund not pushed to TC'));
                        }
                    } else {
                        //If not successful mark as Rejected
                        //thePayment.Bill62__Status__c='Rejected';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,result.ErrorDescription));
                        message = true;
                    }
                }else{
                    //thePayment.Bill62__Status__c = 'Failed';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Refund Failed'));
                    message = true;
                }
            }
        }catch(Exception e){
            //thePayment.Bill62__Status__c = 'Exception';
            thePayment.Bill62__Processing_Message__c = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            message = true;
        }
        display=false;
        update thePayment;
        return null;
    }
    
    public pageReference back(){
        return new pageReference('/'+thePayment.Id);
    }
    

    
}