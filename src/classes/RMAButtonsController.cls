/*
@Name            : RMAButtonsController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for RMA Buttons page
*/


public with sharing class RMAButtonsController {
    public Bill62__Order__c theOrder {get;set;}
    public Bill62__Subscription__c thisOL {get;set;}
    public Boolean showLink {get;set;}
    public String linkPrefix {get;set;}
    public String errorMsg {get;set;}
    public RMAButtonsController(ApexPages.StandardController controller){
        showLink=false;
        thisOL = [SELECT Id, Order__c, Order_Line_ID__c, Bill62__Product__r.Name FROM Bill62__Subscription__c WHERE Id=:controller.getId() LIMIT 1];
        theOrder = [SELECT Id, Order_ID__c FROM Bill62__Order__c WHERE Id=:thisOL.Order__c];
        if( thisOL.Bill62__Product__r.Name.contains('PowerPlay') || thisOL.Bill62__Product__r.Name.contains('Velocity')){
            showLink=true;
        }
        errorMsg = null;
    }
    
    public PageReference rmaEligible(){
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         TC_URL__c tcHelp = TC_URL__c.getOrgDefaults();
         String xml_req = '{"intOrderItemId":"'+thisOL.Order_Line_ID__c+'"}';
         String path = apiHelp.URL__c + 'CheckRMAEligibility';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         String result;
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);    
             system.debug('xml_req: '+xml_req);        
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody();
         }
         errorMsg = null;
         if(result.contains('Item eligible for RMA.')){
            String redirctURL = tcHelp.URL__c+'rma_wizard.aspx?oiid='+thisOL.Order_Line_ID__c;
            system.debug(redirctURL);
            return new PageReference(redirctURL);
         } else {
            errorMsg = 'Item not eligible for RMA';
            return null;
         }
         
    }
    
    public PageReference rmaCancel(){
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         TC_URL__c tcHelp = TC_URL__c.getOrgDefaults();
         String xml_req =   '{"intOrderItemId":"'+thisOL.Order_Line_ID__c+'"}';
         String path = apiHelp.URL__c + 'CheckRMACancel';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         String result;
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);    
             system.debug('xml_req: '+xml_req);        
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody();
         }
         errorMsg = null;
         if(result.contains('Item eligible for RMA cancel.')){
            String redirctURL = tcHelp.URL__c+'rma_cancel.aspx?oiid='+thisOL.Order_Line_ID__c+'&oid='+theOrder.Order_ID__c;
            return new PageReference(redirctURL);
         } else { 
            errorMsg = 'Item not eligible for RMA cancel';
            return null;
         }
    }
}