global class PayflowSessionHeartbeat implements Schedulable,Database.Stateful,Database.AllowsCallouts {


    global PayflowSessionHeartbeat() {

    }


    global void execute(SchedulableContext ctx) {
        makeCallout();
        
    }
    
    @future(callout=true)
    public static void makeCallout(){
        Bill62__Payment_Gateway__c pg = Database.query('SELECT ' + C62Utilities.getFields2('Bill62__Payment_Gateway__c')
                                + ' FROM Bill62__Payment_Gateway__c'
                                + ' WHERE Name = \'PayFlow\'');
        String cClientId, cSecret, cBaseURI;
        if(!pg.Bill62__Is_Testing__c){
            cClientId = pg.Bill62__Login__c;
            cSecret = pg.Bill62__Password__c;
            cBaseURI = pg.Bill62__Production_URL__c;
        }else{
            cClientId = pg.Bill62__Test_Login__c;
            cSecret = pg.Bill62__Test_Password__c;
            cBaseURI = pg.Bill62__Test_URL__c;
        }
        
        Http cHttp = new Http();
        HttpRequest cRequest = new HttpRequest();
System.Debug('Getting new OAuth');
        cRequest.setEndpoint(cBaseURI + '/v1/oauth2/token');
        cRequest.setMethod('GET');
        Blob headerValue = Blob.valueOf(cClientId + ':' + cSecret);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        cRequest.setHeader('Authorization', authorizationHeader);
        cRequest.setHeader('Accept','application/json');
        cRequest.setHeader('Accept-Language','en_US');
        cRequest.setTimeout(60000);
                
        cRequest.setBody('grant_type=client_credentials&content-type=application/x-www-form-urlencoded');
        
        String cResponseBodyStr;
        if(!Test.isRunningTest()){
            HttpResponse cResponse = cHttp.send(cRequest);
            cResponseBodyStr = cResponse.getBody();
        }else{
            cResponseBodyStr = '{"scope":"ok","access_token":"0000","token_type":"GOLD","app_id":"1234","expires_in":"2022"}';
        }
        
System.Debug('Oauth Response: ' + cResponseBodyStr);
        
        C62ParsePayFlowTokenResponse parser = new C62ParsePayFlowTokenResponse();
        string response = parser.parse(cResponseBodyStr).access_token;
        
        String cAccessToken = response;
        pg.Bill62__OAuth_Token__c = cAccessToken;
        update pg;
    }
    
        /*
    @Name           : C62ParsePayFlowTokenResponse 
    @Description    : Inner Class used to deserialize the JSON response returned when requesting an Access token for PayFlow
    */
    public class C62ParsePayFlowTokenResponse {
        public String scope;
        public String access_token;
        public String token_type;
        public String app_id;
        public Integer expires_in;
        
        public C62ParsePayFlowTokenResponse parse(String json) {
            return (C62ParsePayFlowTokenResponse) System.JSON.deserialize(json, C62ParsePayFlowTokenResponse.class);
        }
    }

}