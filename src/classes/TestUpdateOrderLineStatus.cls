/*
@Name            : TestUpdateOrderLineStatus
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for updating Order Line statuses
*/


@IsTest
public with sharing class TestUpdateOrderLineStatus {
    @IsTest
    public static void test01(){
        Bill62__Address__c addy = New Bill62__Address__c();
        insert addy;
        Contact c = New Contact(LastName = 'test',Bill62__Birthdate__c = system.today(), Address__c = addy.Id);
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        insert c;
        Bill62__Subscription__c ol = New Bill62__Subscription__c(Status_ID__c = '0', Bill62__Customer__c = c.id, Bill62__Start_Date__c = system.today());
        insert ol;
        ol.Status_ID__c = '-6';
        update ol;
    }
    
    @IsTest
    public static void test02(){
        Account acc = New Account(Name = 'test', Status_ID__c = '1', Status__c = 'blah');
        insert acc;
        acc.Status_ID__c = '2';
        acc.Status__c = '2';
        update acc;
    }
     
    @IsTest
    public static void test03(){
        Bill62__Address__c addy = New Bill62__Address__c();
        insert addy;
        Contact c = New Contact(LastName = 'test',Bill62__Birthdate__c = system.today(), Address__c = addy.Id);
        c.Business_Name__c = 'test';
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        insert c;
        Vendor__c vend = New Vendor__c(Vendor_Code__c = 'US');
        insert vend;
        Carrier__c car = New Carrier__c(Name = 'Test', Foreign_Code__c = 'US');
        insert car;
        Product2 prod = New Product2(Name = 'Test');
        insert prod;
        Bill62__Order__c o = New Bill62__Order__c(Bill62__Customer__c = c.id, Carrier__c = car.id);
        insert o;
        c.LastName = 'Test';
        update c;
        c.LastName = 'Test';
        c.Business_Name__c = null;
        update c;
    }
}