/*
@Name            : TestBatches
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for all batches
*/


@isTest
public class TestBatches{

    //setup utility methods
    public static Contact getCon(){
        Bill62__Address__c a = new Bill62__Address__c(Address_ID__c = '12345');
        insert a;
        Contact c = new Contact();  
            c.FirstName = 'Percy';
            c.LastName = 'Weatherby';          
            c.Bill62__Last_Name__c = 'Weatherby';
            c.Bill62__First_Name__c = 'Percy';
            c.Middle_Initial__c = 'M';
            c.Bill62__Birthdate__c = system.today();
            c.Bill62__SSN__c = '111111111';
            c.Bill62__Home_Phone__c = '1111111111';
            c.Email = 'pweatherby@mom.org';
            c.Address__c = a.id;
            c.Customer_ID__c = '1';
        insert c;
        return c;
    } 
    
    public static Bill62__Subscription__c getSub(){
        Bill62__Subscription__c s = new Bill62__Subscription__c();
        s.Bill62__Customer__c = getCon().Id;
        s.Bill62__Product__c = getProd().Id;
        s.Order__c = getOrder().Id;
        s.Bill62__Start_Date__c = system.date.today();
        s.Status_Id__c = '10';
        insert s;
        return s;
    }
    
    public static Product2 getProd(){
        Product2 p = new Product2();
        p.Name = 'test Prod';
        Product_Category__c pc = new Product_Category__c();
        pc.Name = 'test pc';
        insert PC;
        p.Product_Category__c = pc.Id;
        p.Bill62__Auto_Renew__c = true;
        Product2 p2 = new Product2();
        p2.Name = 'Changer';
        p2.Product_Category__c = pc.Id;
        p2.Bill62__Auto_Renew__c = true;
        insert p2;
        p.Changes_To__c = p2.Id;
        insert p;
        Payment_Reason__c pr = new Payment_Reason__c();
        pr.Months__c = 12;
        pr.Product__c = p.Id;
        pr.External_ID__c = '1';
        Payment_Reason__c pr2 = new Payment_Reason__c();
        pr2.Months__c = 12;
        pr2.Product__c = p2.Id;
        pr2.External_ID__c = '2';
        insert pr;
        insert pr2;
        return p;
    }
    
    public static Bill62__Order__c getOrder(){
        Bill62__Order__c o = new Bill62__Order__c();
        o.Bill62__End_Date__c = system.date.today().addDays(-15);
        o.Name = 'test Order';
        o.Has_Current_Payment__c = false;
        o.NOP_ID__c = '12345';
        insert o;
        return o;
    }
    
    public static Bill62__Payment_Method__c getPayMeth(){
        Bill62__Payment_Method__c pm = new Bill62__Payment_Method__c();
        pm.Bill62__Card_Holder_Name__c = 'Tom Smith'; 
        pm.Bill62__Card_Number__c = '11111111111111';
        pm.Bill62__Expiration_Date__c = system.date.newInstance(2100, 10, 10);
        pm.Bill62__Routing_Number__c = '11111111111111';
        pm.Bill62__Account_Number__c = '11111111111111';
        insert pm;
        return pm;
    }
    
    public static Bill62__Payment_Gateway__c getPayGate(){
        Bill62__Payment_Gateway__c theGate = new Bill62__Payment_Gateway__c(Name='Authorize.net',Bill62__Default__c=TRUE,Bill62__Login_Encrypted__c='63kF4pjcgZ2Z',Bill62__Password_Encrypted__c='7Yk49MRq86wU29j9',
            Bill62__Production_URL__c='https://secure.authorize.net/gateway/transact.dll',
            Bill62__Test_Code_Response__c='1|1|1|This transaction has been approved.|LDV9JR|Y|2160295577||0|600.00|CC|'
            +'auth_capture||aslam|bari||dfsdf||wa|5555||||||||||||||||||71AFA2003D02930E47E84F23E14E07C3|P|2||'
            +'|||||||||XXXX1111|Visa|||||||||||||||||false|1',Bill62__Test_URL__c='https://test.authorize.net/gateway/transact.dll');
        theGate.Bill62__Default__c = true;
        insert theGate;
        return theGate;
    }
    
    @isTest
    public static void testProcessPaymentBatch(){
        C62Utilities.cSetExecuted.add('paymentBatch');
        Bill62__Subscription__c theSub = getSub();
        Bill62__Order__c theOrder = [SELECT ID FROM Bill62__Order__c LIMIT 1];
        Contact theCustomer = [SELECT ID, Customer_ID__c FROM Contact LIMIT 1];
        Bill62__Payment__c thePayment = new Bill62__Payment__c();
        Payment_Reason__c thePR = [SELECT ID, External_ID__c FROM Payment_Reason__c LIMIT 1];
        thePayment.Bill62__Status__c = 'Queued';
        thePayment.Bill62__Order__c = theOrder.Id;
        thePayment.Bill62__Subscription__c = theSub.Id;
        thePayment.Bill62__Customer__c = theCustomer.Id;
        thePayment.Bill62__Payment_Date__c = system.date.Today();
        Bill62__Payment_Method__c thePayMeth = getPayMeth();
        thePayment.Bill62__Payment_Method__c = thePayMeth.Id;
        insert thePayment;
        Bill62__Payment_Gateway__c theGate = getPayGate();
        //all this stuff in the query isn't needed, but it's not worth the time to go through and pick and choose
        thePayment = [SELECT Id,Name,Bill62__Customer__c,Bill62__Customer__r.FirstName, 
        Bill62__Customer__r.Customer_ID__c,  Bill62__Subscription__c, TC_Integration_Error__c, 
        Bill62__Customer__r.Account.RIN__c, Bill62__Order__r.Bill62__Account__r.Entity_ID__c, Bill62__Recurring_Payment__c,  
        Bill62__Customer__r.Address__r.Country__r.Country_ID__c,Bill62__Customer__r.Address__r.Bill62__City__c, Bill62__Customer__r.Address__r.Bill62__Street_1__c, 
        Bill62__Customer__r.Address__r.Bill62__State_Province__c, Bill62__Customer__r.Address__r.Bill62__Zip_Postal_Code__c, Bill62__Customer__r.HomePhone, 
        Bill62__Subscription__r.Order_ID__c, Bill62__Transaction_Id__c, Bill62__Approval_Code__c, 
        Bill62__Subscription__r.Order__r.Order_IDtxt__c, Bill62__Subscription__r.Bill62__Product__c, 
        Bill62__Order__r.Order_Id__c, Bill62__Subscription__r.Order_Line_ID__c, Bill62__Subscription__r.Bill62__Product__r.External_ID__c, 
        Bill62__Amount__c,Bill62__Payment_Method__c,Bill62__Payment_Method__r.Bill62__Card_Holder_Name__c,
        Bill62__Subscription__r.Bill62__Product__r.Has_Tax__c, Bill62__Subscription__r.Bill62__Product__r.Has_Shipping__c,Bill62__Payment_Method__r.Bill62__Account_Number__c,
        Bill62__Customer__r.LastName,Bill62__Payment_Method__r.Bill62__Card_Number__c,Bill62__Payment_Method__r.Payment_Method_ID__c,
        Bill62__Payment_Method__r.Bill62__Card_Type__c,Bill62__Payment_Method__r.Bill62__Token__c,Bill62__Payment_Method__r.Bill62__CVV_Code__c,
        Bill62__Payment_Method__r.Bill62__Expiration_Date__c,Bill62__Payment_Method__r.Bill62__Payment_Type__c,
        Bill62__Payment_Method__r.Bill62__Routing_Number__c, Bill62__Payment_Method__r.Name,Bill62__Payment_Method__r.Bill62__Billing_Address__c,
        Bill62__Number_Of_Authorization_Attempts__c, Bill62__Payment_Gateway__c, Bill62__Payment_Gateway__r.Name 
        FROM Bill62__Payment__c LIMIT 1];
        
        Test.startTest();
        FiveLinxBatchProcessPayments batch = new FiveLinxBatchProcessPayments();
        batch.currentUser = [SELECT ID, TC_User_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
        database.executeBatch(batch, 10);
        batch.postPaymentProcess(thePayment, thePayMeth, thePR.External_ID__c);
        Test.stopTest();
    }
    
    
    @isTest
    public static void testRescheduleBillingBatch(){
        C62Utilities.cSetExecuted.add('paymentBatch');
        Bill62__Subscription__c theSub = getSub();
        Bill62__Order__c theOrder = [SELECT ID FROM Bill62__Order__c LIMIT 1];
        Contact theCustomer = [SELECT ID, Customer_ID__c FROM Contact LIMIT 1];
        Bill62__Payment__c thePayment = new Bill62__Payment__c();
        thePayment.Bill62__Status__c = 'Paid';
        thePayment.Bill62__Order__c = theOrder.Id;
        thePayment.Bill62__Subscription__c = theSub.Id;
        thePayment.Bill62__Customer__c = theCustomer.Id;
        thePayment.Bill62__Payment_Date__c = system.date.Today();
        insert thePayment;
        
        Bill62__Payment__c theOtherPayment= new Bill62__Payment__c();
        theOtherPayment.Bill62__Status__c = 'Queued Retry';
        theOtherPayment.Bill62__Order__c = theOrder.Id;
        theOtherPayment.Bill62__Subscription__c = theSub.Id;
        theOtherPayment.Bill62__Customer__c = theCustomer.Id;
        theOtherPayment.Bill62__Payment_Date__c = system.date.Today();
        insert theOtherPayment;
        
        Test.startTest();
        FiveLinxBatchRescheduleBilling batch = new FiveLinxBatchRescheduleBilling();
        database.executeBatch(batch, 10);
        Test.stopTest();
    }
    
    
    @isTest
    public static void testGeneratePaymentsBatch(){
        C62Utilities.cSetExecuted.add('paymentBatch');
        Bill62__Subscription__c theSub = getSub();
        
        Test.startTest();
        FiveLinxGeneratePayments batch = new FiveLinxGeneratePayments();
        database.executeBatch(batch, 10);
        Test.stopTest();
    }
    
    
    @isTest
    public static void testGenerateWellnessPaymentsBatch(){
        C62Utilities.cSetExecuted.add('paymentBatch');
        Bill62__Subscription__c theSub = getSub();
        List<Product_Category__c> pcl = [SELECT ID, Name FROM Product_Category__c];
        for(Product_Category__c pc: pcl){
            pc.Name = 'Coffee Recurring';
        }
        update pcl;
        
        Test.startTest();
        FiveLinxGenerateWellnessPayments batch = new FiveLinxGenerateWellnessPayments();
        database.executeBatch(batch, 10);
        Test.stopTest();
    }

}