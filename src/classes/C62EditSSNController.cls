/*
@Name            : C62EditSSNController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Edit SSN page.  Possibly deprecated
*/


public class C62EditSSNController {
    public Account myRep {get; set;}
    public String newSSN {get; set;}
    public String last4 {get;set;}
    public String errorMsg {get; set;}
    public String winMsg {get; set;}
    public Boolean hasSSN {get;set;}
    
    public C62EditSSNController(ApexPages.StandardController controller) {
         myRep = [SELECT Id, SSN__c, Valid_SSN__c FROM Account WHERE Id = :controller.getId()];
         if(myRep.SSN__c != null && myRep.SSN__c.length() > 4){
             last4='XXX-XX-'+myRep.SSN__c.substring(5);
        } else if (myRep.SSN__c != null && myRep.SSN__c.length() == 4){
             last4='XXX-XX-'+myRep.SSN__c;
        } else {
             last4= 'no current ssn';
        }
        errorMsg = null;
        winMsg = null;
        hasSSN = true;
        Profile pr = [SELECT Name FROM Profile WHERE Id =:userinfo.getProfileId()];
        if (pr.Name != '5Linx CSR') hasSSN = false;
        if (last4 == 'no current ssn') hasSSN = false;
    }
    
     public PageReference editSSN(){    
        String result;
        try{    
           result = C625LinxAPIHelper.isSSNUsable(newSSN);
        } catch (exception e) {
           errorMsg = 'There was an error verifying your SSN';
           return null;
        }
            
        if(Test.IsRunningTest()){
                result = '"isSSNUsable": true isSSNUsable": false';
        }
         if (result.contains('"isSSNUsable": true')){
            winMsg = 'SSN Changed Successfuly';
            errorMsg = null;
            myRep.SSN__c = newSSN;
            update myRep;
         } else if (result.contains('isSSNUsable": false')){
            errorMsg = 'This SSN is not valid';
            winMsg = null;
         }
         return null;
     }
     public PageReference cancel(){    
        return new PageReference('/' + myRep.id);
     }
}