/*
@Name            : ChangeEnrollerSponsorController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Change Enroller/Sponsor page
*/


public with sharing class ChangeEnrollerSponsorController {
    public Account thisRep {get;set;}
    public Boolean showError {get;set;}
    public Boolean showWin {get;set;}
    public String errorMsg {get;set;}
    public String winMsg {get;set;}
    public String oldEnroller {get;set;}
    public String oldSponsor {get;set;}
    public Account newEnroller {get;set;}
    public Account newSponsor {get;set;}
    
    public ChangeEnrollerSponsorController(ApexPages.StandardController controller){
        thisRep = [SELECT ID, Name, Entity_ID__c, Enroller__c, ParentId, Enroller__r.Name, Parent.Name 
                   FROM   Account 
                   WHERE  Id = :controller.getId()];
        oldEnroller = thisRep.Enroller__r.Name;
        oldSponsor = thisRep.Parent.Name;
        //newSponsor = new Account();
        //newEnroller = new Account();
    }

    public PageReference ChangeEnrollerAndSponsor(){
        String result;
        winMsg = '';
        errorMsg = '';  
        showError = false;
        showWin = false;
        newEnroller = thisRep.Enroller__c != null ? [SELECT RIN__c FROM Account WHERE ID = :thisRep.Enroller__c LIMIT 1] : null;
        newSponsor = thisRep.ParentId != null ? [SELECT RIN__c FROM Account WHERE ID = :thisRep.ParentId LIMIT 1] : null;
        if(newEnroller != null && newSponsor != null){
             //Call ChangeEnrollerAndSponsor            
             String xml_req;
                xml_req =  '{"intEntityId":"'+thisRep.Entity_ID__c+'",';     
                xml_req +=  '"strNewSponsorRIN":"'+newSponsor.RIN__c+'",';
                xml_req +=  '"strNewEnrollerRIN":"'+newEnroller.RIN__c+'"}';
             APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();              
             String path = apiHelp.URL__c + 'changeEnrollerAndSponsor';
             Http h = new Http();
             HttpRequest req = new HttpRequest();
             req.setBody(xml_req);
             req.setEndpoint(path);
             req.setHeader('AuthInfo','51nX4p1Us3rsLf');
             req.setHeader('Content-Type','application/json');
             req.setMethod('POST');
             req.setTimeout(120000);
             if(!Test.isRunningTest()){
                 HttpResponse res = h.send(req);
                 System.debug('xml_req: '+xml_req);
                 system.debug('THIS IS THE BODY!!' + res.getBody());
                 result = res.getBody();  
             } else {
                 result = 'enrollerChangeTransactionResult": "Sponsor not in the Open Line of the Enroller. sponsorChangeTransactionResult": "Sponsor not in the Open Line of the Enroller. '+
                     'Sponsor RIN does not exist. Invalid Sponsor OR unable to do genealogy check. Invalid enroller RIN. Sponsor changed successfully '+
                     'Enroller changed successfully';
             }
        } else {
            system.Debug('You must select an Enroller and Sponsor');
            showError = true;
            errorMsg = 'You must select an Enroller and Sponsor';
            result = '';
        }
        
        // Process the result of the API call   
        if (result.contains('enrollerChangeTransactionResult": "Sponsor not in the Open Line of the Enroller.')){
            showError = true;
            errorMsg = 'The Sponsor is not in the Open Line of the Enroller.  Please select a different Enroller.  Changes not Saved.';
        } else if (result.contains('sponsorChangeTransactionResult": "Sponsor not in the Open Line of the Enroller.')){
            showError = true;
            errorMsg = 'The Sponsor is not in the Open Line of the Enroller.  Please select a different Sponsor.  Changes not Saved.';
        } else if (result.contains('Sponsor RIN does not exist.')){
            showError = true;
            errorMsg = 'The Sponsor RIN does not exist.  Changes not Saved.';
        } else if (result.contains('Invalid Sponsor OR unable to do genealogy check.')){
            showError = true;
            errorMsg = 'Invalid Sponsor OR unable to do genealogy check.  Changes not Saved.';
        } else if (result.contains('Invalid enroller RIN.')){
            showError = true;
            errorMsg = 'Invalid Enroller RIN.  Changes not Saved.';
        } else if (result.contains('Endpoint not found.')){
            showError = true;
            errorMsg = 'Changes not Saved.  There was a problem reacing the TC API.  Please try again later.';
        }
        
        if (result.contains('Sponsor changed successfully') && result.contains('Enroller changed successfully')){
            showWin = true;
            winMsg = 'Sponsor and Enroller changed successfully.';
            thisRep.ParentId = newSponsor.Id;
            thisRep.Enroller__c = newEnroller.Id;
            C62Utilities.cSetExecuted.add('dontFireTrigger');
            if(!Test.isRunningTest()) update thisRep;
        }
        return null;
    }
    
    public PageReference ChangeSponsor(){
         String result;
         winMsg = '';
         errorMsg = '';
         showError = false;
         showWin = false;
         newSponsor = thisRep.ParentId != null ? [SELECT RIN__c FROM Account WHERE ID = :thisRep.ParentId LIMIT 1] : null;
         //newEnroller = [SELECT Id FROM Account WHERE Name =:oldEnroller];
         if (newSponsor != null){
             // Call ChangeSponsor
             String xml_req;
                xml_req =  '{"intEntityId":"'+thisRep.Entity_ID__c+'",';     
                xml_req +=  '"strNewSponsorRIN":"'+newSponsor.RIN__c+'"}';
             APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();              
             String path = apiHelp.URL__c + 'changeSponsor';
             Http h = new Http();
             HttpRequest req = new HttpRequest();
             req.setBody(xml_req);
             req.setEndpoint(path);
             req.setHeader('AuthInfo','51nX4p1Us3rsLf');
             req.setHeader('Content-Type','application/json');
             req.setMethod('POST');
             req.setTimeout(120000);
             if(!Test.isRunningTest()){
                 HttpResponse res = h.send(req);
                 System.debug('xml_req: '+xml_req);
                 system.debug('THIS IS THE BODY!!' + res.getBody());
                 result = res.getBody(); 
             } else {
                 result = 'Sponsor not in the Open Line of the Enroller. Sponsor RIN does not exist. Invalid Sponsor OR unable to do genealogy check.' +
                 ' Endpoint not found. Sponsor changed successfully';
             }
         } else {
             system.Debug('You must select a Sponsor');
             showError = true;
             errorMsg = 'You must select a Sponsor';
             result = '';
        }
        // process resulting message from API
        if (result.contains('Sponsor not in the Open Line of the Enroller.')){
            showError = true;
            errorMsg = 'The Sponsor is not in the Open Line of the Enroller.  Changes not Saved.';
        } else if (result.contains('Sponsor RIN does not exist.')){
            showError = true;
            errorMsg = 'The Sponsor RIN does not exist.  Changes not Saved.';
        } else if (result.contains('Invalid Sponsor OR unable to do genealogy check.')){
            showError = true;
            errorMsg = 'Invalid Sponsor OR unable to do genealogy check.  Changes not Saved.';
        } else if (result.contains('Endpoint not found.')){
            showError = true;
            errorMsg = 'Changes not Saved.  There was a problem reacing the TC API, Please try again later.';
        }
         
         if (result.contains('Sponsor changed successfully')){
            showWin = true;
            winMsg = 'Sponsor changed successfully.';
            thisRep.ParentId = newSponsor.Id;
            //thisRep.Enroller__c = newEnroller.Id;
            C62Utilities.cSetExecuted.add('dontFireTrigger');
            if(!Test.isRunningTest()) update thisRep;
         }
         return null;
    }
    
    public PageReference ChangeEnroller(){
        String result;  
        winMsg = '';
        errorMsg = '';
        showError = false;
        showWin = false;
        newEnroller = thisRep.Enroller__c != null ? [SELECT RIN__c FROM Account WHERE ID = :thisRep.Enroller__c LIMIT 1] : null;
        //newSponsor = [SELECT Id FROM Account WHERE Name =:oldSponsor];
        if (newEnroller != null){
             // Call ChangeEnroller
             String xml_req;
                xml_req =  '{"intEntityId":"'+thisRep.Entity_ID__c+'",';     
                xml_req +=  '"strNewEnrollerRIN":"'+newEnroller.RIN__c+'"}';
             APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();              
             String path = apiHelp.URL__c + 'changeEnroller';
             Http h = new Http();
             HttpRequest req = new HttpRequest();
             req.setBody(xml_req);
             req.setEndpoint(path);
             req.setHeader('AuthInfo','51nX4p1Us3rsLf');
             req.setHeader('Content-Type','application/json');
             req.setMethod('POST');
             req.setTimeout(120000);
             if(!Test.isRunningTest()){
                 HttpResponse res = h.send(req);
                 System.debug('xml_req: '+xml_req);
                 system.debug('THIS IS THE BODY!!' + res.getBody());
                 result = res.getBody(); 
             } else {
                 result = 'Sponsor not in the Open Line of the Enroller. Invalid enroller RIN. Invalid Sponsor OR unable to do genealogy check'+
                     ' Endpoint not found. Enroller changed successfully';
             }
        } else {
            system.Debug('You must select an Enroller');
            showError = true;
            errorMsg = 'You must select an Enroller';
            result = '';
        }
        // process resulting message from API
        if (result.contains('Sponsor not in the Open Line of the Enroller.')){
            showError = true;
            errorMsg = 'The Sponsor is not in the Open Line of the Enroller.  Changes not Saved.';
        } else if (result.contains('Invalid enroller RIN.')){
            showError = true;
            errorMsg = 'Invalid enroller RIN.  Changes not Saved.';
        } else if (result.contains('Invalid Sponsor OR unable to do genealogy check')){
            showError = true;
            errorMsg = 'Invalid Sponsor OR unable to do genealogy check. Changes not Saved.';
        } else if (result.contains('Endpoint not found.')){
            showError = true;
            errorMsg = 'Changes not Saved.  There was a problem reacing the TC API, Please try again later.';
        }
        
        if (result.contains('Enroller changed successfully')){
            winMsg = 'Enroller changed Successfully.';
            showWin = true;
            //thisRep.ParentId = newSponsor.Id;
            thisRep.Enroller__c = newEnroller.Id;
            C62Utilities.cSetExecuted.add('dontFireTrigger');
            if(!Test.isRunningTest()) update thisRep;
        }
        return null;
    }
    
    public PageReference BackToRep(){
        return new PageReference('/'+ thisRep.Id);
    }
}