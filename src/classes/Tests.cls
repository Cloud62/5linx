public with sharing class Tests {

	public static testmethod void test() {
		
		RIN__c rin = new RIN__c(
			RIN__c = 'ABC123',
			Name = 'Test RIN'
		);
		insert rin;
		
		Lead l = new Lead(
			FirstName = 'test',
			LastName = 'lead',  
			Company = 'Test',
			RIN__c = 'ABC123'
		);  
		  
		insert l;
		
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(l.Id);
		lc.setOpportunityName('Test');  
		LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true limit 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
		
		Database.convertLead(lc); 
		
	}  
	
	public static testmethod void testOpportunities() {
		
		Opportunity o = new Opportunity(
			Name = 'test',
			StageName = 'Closed - Won',
			CloseDate = Date.today()
		);
		
		insert o;
		
		PricebookEntry pbe = [select Id from PricebookEntry limit 1];
		
		OpportunityLineItem oli = new OpportunityLineItem(
			OpportunityId = o.Id,
			PricebookEntryId = pbe.Id,
			Quantity = 1,
			UnitPrice = 25,
			Shipping__c = 0,
			One_Time_Fee__c = 0
		);
		
		insert oli;
		
	}

}