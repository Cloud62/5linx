/*
@Name            : TestShippingChangeCheck
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for shipping change check trigger
*/


@IsTest
public class testShippingChangeCheck {
    
    // create and restrn an order with or without a child object with a specific status.
    public static Bill62__Order__c newOrder(String childStatus, Boolean child, String pType){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Address__c a = new Bill62__Address__c(Address_ID__c = '12345');
        insert a;
        Contact c = new Contact();  
            c.LastName = 'Baelish';          
            c.Bill62__Last_Name__c = 'Baelish';
            c.Bill62__First_Name__c = 'Petyr';
            c.Middle_Initial__c = 'T';
            c.Bill62__Birthdate__c = system.today();
            c.Bill62__SSN__c = '465738485';
            c.Bill62__Home_Phone__c = '7168675309';
            c.Email = 'pbaelish@kingslanding.gov';
            c.Address__c = a.id;
        insert c;
        Product_Type__c pt = New Product_Type__c (Name = pType);
        insert pt;
        if (pt.Name != 'Wellness'){
            Product_Type__c pt2 = New Product_Type__c (Name = 'Wellness');
            insert pt2;
        }
        Bill62__Order__c o = new Bill62__Order__c();
            o.Shipping_Address__c = a.id;
            o.Product_Type__c = pt.Id;
        insert o;
        if (child == true){
            Bill62__Subscription__c ol = New Bill62__Subscription__c();
                ol.Status_ID__c = childStatus;
                ol.Order__c = o.Id;
                ol.Bill62__Start_Date__c = system.today();
                ol.Bill62__Customer__c = c.id;
            insert ol;
        }
        return o;
    }

    @IsTest
    public static void test01(){
        Boolean exceptionThrown = false;
        Bill62__Order__c o = newOrder('80', true, 'Wellness');
        o.Shipping_Address__c = null;
        try {
            update o;
        } catch (exception e) {
            system.debug(e);
            exceptionThrown = true;
        }
    }

    @IsTest
    public static void test02(){
        Boolean exceptionThrown = false;
        Bill62__Order__c o = newOrder('90', true, 'Wellness');
        o.Shipping_Address__c = null;
        try {
            update o;
        } catch (exception e) {
            system.debug(e);
            exceptionThrown = true;
        }
    }

    @IsTest
    public static void test03(){
        Boolean exceptionThrown = false;
        Bill62__Order__c o = newOrder('0', true, 'Wellness');
        o.Shipping_Address__c = null;
        try {
            update o;
        } catch (exception e) {
            system.debug(e);
            exceptionThrown = true;
        }
        system.assertEquals(false, exceptionThrown);
    }
    
    @IsTest
    public static void test04(){
        Boolean exceptionThrown = false;
        Bill62__Order__c o = newOrder('90', true, 'Other');
        o.Shipping_Address__c = null;
        try {
            update o;
        } catch (exception e) {
            system.debug(e);
            exceptionThrown = true;
        }
        system.assertEquals(false, exceptionThrown);
    }    
    
    @IsTest
    public static void test05(){
        Boolean exceptionThrown = false;
        Bill62__Order__c o = newOrder('', false, 'Wellness');
        o.Shipping_Address__c = null;
        try {
            update o;
        } catch (exception e) {
            system.debug(e);
            exceptionThrown = true;
        }
        system.assertEquals(false, exceptionThrown);
    }
}