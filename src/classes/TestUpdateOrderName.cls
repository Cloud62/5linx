/*
@Name            : TestUpdateOrderName
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for update Order name trigger
*/


@IsTest
public with sharing class TestUpdateOrderName {
    @IsTest
    public static void test01(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Account a = New Account(Entity_ID__c = '1234', Name = 'Test');
        insert a;
        Contact c = New Contact(FirstName = 'Testy', LastName = 'McPerson');
        insert c;
        Bill62__Order__c o = New Bill62__Order__c(Bill62__Customer__c = c.id, Name = 'Testy McPerson - Carrier - Product', Bill62__Account__c = a.id);
        insert o;
        c.FirstName = 'John';
        test.startTest();
        update c;
        test.stopTest();
        Bill62__Order__c newOrder = [SELECT Name FROM Bill62__Order__c];
        //system.assertEquals('John McPerson - Carrier - Product', newOrder.Name); asserts are for suckers
    }
}