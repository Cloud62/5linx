/*
@Name            : C62AccountOverrideController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Account override page. Handles informational popup
*/


public with sharing class C62AccountOverrideController {
    public String event{get;set;}
    public String energy{get;set;}
    public String business{get;set;}
    public Account theAccount{get;set;}
    public Boolean showPopup {get;set;}
    public String accountId {get;set;}
    
    public C62AccountOverrideController(ApexPages.StandardController controller) {
        accountId = ((Account)controller.getRecord()).Id;
        theAccount = [SELECT ID, Name, Viewed_Record__c FROM Account WHERE Id = :accountId];
        showPopup = true;
        Profile pr = [SELECT Name FROM Profile WHERE Id =:userinfo.getProfileId()];
        if (pr.Name == 'Customer Experience Supervisor' ||
            pr.Name == 'Customer Experience Manager' ||
            pr.Name == 'Manager Field Operations' ||
            pr.Name == 'Finance Department' ||
            pr.Name == '5Linx Admin' ||
            pr.Name == 'System Administrator' ){
            showPopup = false;      
        }
        
        List<Bill62__Subscription__c> nationalEventList = [SELECT ID FROM Bill62__Subscription__c 
            WHERE Bill62__Product__r.Product_Category__r.External_ID__c = '1' 
            AND Order__r.Bill62__Account__c = :accountId
            AND (Status__c = 'ACTIVE' OR Status__c = 'PURGED')];
        system.debug('national event tickets size: '+nationalEventList.size());
        
        /* Energy Cert Criteria: Has Rep Certification in Related List
        List<Rep_Certification__c> energyList = [SELECT ID, Expiration_Date__c FROM Rep_Certification__c 
                                                 WHERE Rep__c = :theAccount.Id];
        system.debug('energy size: '+energyList.size());
        */
        
        // Energy Cert Criteria: Has Energy Certification Order
        List<Bill62__Subscription__c> energyList = [SELECT Id, Bill62__Product__c FROM Bill62__Subscription__c 
                                                    WHERE Order__r.Bill62__Account__c = :theAccount.Id AND 
                                                    (Bill62__Product__r.External_ID__c = '1600' OR Bill62__Product__r.External_ID__c = '1604')];
        
        List<Bill62__Subscription__c> businessEliteList = [SELECT ID FROM Bill62__Subscription__c 
            WHERE Bill62__Product__r.Product_Category__r.External_ID__c = '19' 
            AND Order__r.Bill62__Account__c = :accountId];
        system.debug('business elite size: '+businessEliteList.size());
        
        if(nationalEventList.size() <= 0){
            event = 'Rep has NO National Event tickets.';
        } else if(nationalEventList.size() == 1){
            event = 'Rep has 1 National Event ticket.';
        }else{
            event = 'Rep has '+nationalEventList.size()+' National Event tickets';
        }
        
        if(energyList.size() <= 0){
            energy = 'Rep is NOT Energy Certified.';
        } else {
            energy = 'Rep is Energy Certified.';
        } 
        
        if(businessEliteList.size() <= 0){
            business = 'Rep does NOT have Business Elite Services.';
        } else {
            business = 'Rep does have Business Elite Services.';
        }
        
    }
    
    public void viewRep(){
        if (theAccount.Viewed_Record__c == true) theAccount.Viewed_Record__c = false;
        else theAccount.Viewed_Record__c = true;
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        update theAccount;
    }

}