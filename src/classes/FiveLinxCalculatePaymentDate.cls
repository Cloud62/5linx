/*
@Name            : FiveLinxCalculatePaymentDate
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Utility Class to calculate Payment dates, a few others
*/



global class FiveLinxCalculatePaymentDate{

    /*
        NOTE: This class is designed to create payments for TOMORROW, not for today.
        This means that this calculation will happen after all payments have been processed for the day,
        setting up the data for the next day's run.
    */

    public FiveLinxCalculatePaymentDate(){
    
    }
    
    /* Required for Subscription:
    SELECT Order__r.Bill62__End_Date__c, Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Name
    FROM Subscription
    Required for Payment:
    SELECT Bill62__Payment_Date__c FROM Payment
    */
    public static Date calculatePaymentDate(Bill62__Payment__c payment, Bill62__Subscription__c theSub, Integer days){
        Date today = System.Date.TODAY().addDays(days);
        //Date today = System.Date.newInstance(2014, 11, 2); //FOR TEST PURPOSES ONLY
        Date newPayDate;
        String dateString;
        if(theSub.Order__r.Bill62__End_Date__c != null){
            dateString = String.valueof(theSub.Order__r.Bill62__End_Date__c);
        } else {
            return null;
        }
        Date endDate = Date.valueOf(dateString);

        
        //Date endDate = theSub.Order__r.Bill62__End_Date__c.Date();
        system.debug('trimmed down end date: '+endDate);
        String fam = theSub.Bill62__Product__r.Product_Category__r.Name;
        String prod = theSub.Bill62__Product__r.Name;
        
        // Auto Renew Schedule pages 1 - 2
        if(fam.Equals('BES Renew Schedule') || fam.Equals('TextAlertz') || fam.Equals('Business Elite')){
            // if Payment Date is before end date, set Payment Date to 2 days after end date
            if(today <= endDate.AddDays(2)){
                system.debug('here1');
                newPayDate = endDate.AddDays(2);
            // if Payment date is 2 days after end date, set to 4 days after end date,
            //unless the 2nd or 16th of the month comes first
            }else if(today == endDate.AddDays(3) || today == endDate.AddDays(4)){
                system.debug('here3');
                Date date1 = C62Utilities.GetNextDate2(today, endDate);
                Date date2 = endDate.AddDays(4);
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            // if Payment date is 4 days after end date, set to next Saturday (4-11 days after end date) 
            // OR the 2nd of the next month, whichever is first.
            }else if(today >= endDate.addDays(4) && today <= endDate.AddDays(11)){
                Date date1 = C62Utilities.GetNextDate1(today, endDate);
                Date date2 = C62Utilities.GetNextDate2(today, endDate);
                system.debug('here5');
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            //if more than 11 days past end date, set to the 2nd, 16th, or 15 days after, whichever is first
            }else if(today > endDate.addDays(11) && today < endDate.AddDays(15)){
                Date date1 = C62Utilities.GetNextDate2(today, endDate);
                Date date2 = endDate.AddDays(15);
                system.debug('here6');
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            // Otherwise set to 15 days after end date
            }else{
                system.debug('here7');
                newPayDate = endDate.AddDays(15);
            }
        
        // Auto Renew Schedule page 3
        }else if(fam.Equals('5LINX Velocity ERP')){
            // if today is before the end date, set to end date
            if(today <= endDate){
                system.debug('here0');
                newPayDate = endDate;
            //if between end date and 2 days after, set to 2 days after
            } else if(today <= endDate.AddDays(2)&& today > endDate){
                system.debug('here1');
                newPayDate = endDate.AddDays(2);
            // if Payment date is between 2 and 4 days after end date, set to 4 days after end date
            //OR 2nd or 16th, whichever is first
            }else if(today == endDate.AddDays(3) || today == endDate.AddDays(4)){
                system.debug('here3');
                Date date1 = C62Utilities.GetNextDate2(today, endDate);
                Date date2 = endDate.AddDays(4);
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            // if Payment date is 4 days after end date, set to next Saturday (4-11 days after end date) 
            // OR the 2nd of the next month, whichever is first.
            }else if(today >= endDate.addDays(4) && today <= endDate.AddDays(11)){
                Date date1 = C62Utilities.GetNextDate1(today, endDate);
                Date date2 = C62Utilities.GetNextDate2(today, endDate);
                system.debug('here5');
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            //if more than 11 days past end date, set to the 2nd, 16th, or 15 days after, whichever is first
            }else if(today > endDate.addDays(11) && today < endDate.AddDays(15)){
                Date date1 = C62Utilities.GetNextDate2(today, endDate);
                Date date2 = endDate.AddDays(15);
                system.debug('here6');
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            // Otherwise set to 15 days after end date
            }else{
                system.debug('here7');
                newPayDate = endDate.AddDays(15);
            }

        // Auto Renew Schedule page 4, Wellness
        }else if(fam.Equals('Wellness Reship') || fam.Equals('Coffee Recurring') || fam.Equals('Nutrition')
            || fam.Equals('Nutrition Recurring') || fam.Equals('Premium Bundle')){
            // set to 4 days before end date
            if(today <= endDate.addDays(-4)){
                system.debug('here1');
                newPayDate = endDate.AddDays(-4);
            //otherwise, set to end date
            } else {
                system.debug('here2');
                newPayDate = endDate;
            }
        // Auto Renew Schedule page 5
        }else if(fam.Equals('Membership Reactivations') || fam.Equals('Monthly Membership') 
            || fam.Equals('Yearly Membership')){
            // make payment every day from 0-15 days after end date
            if(today <= endDate.AddDays(60) && today >= endDate){
                newPayDate = today;
            }
            
        // Auto Renew Schedule page 6
        }else if(fam.Equals('Renew Energy Certification') || prod.Equals('Renewable Energy Certificate')){
            // if Payment date is before the end date, set Payment date to end date
            if(today <= endDate){
                newPayDate = endDate;
            //one days after end date
            } else if(today == endDate.addDays(1)){
                newPayDate = today;
            // if Payment date is 2 days after the end date, set to next Saturday (4-11 days after end date) 
            // OR the 2nd of the next month, whichever is first.
            }else if(today > endDate.addDays(1) && today <= endDate.AddDays(11)){
                Date date1 = C62Utilities.GetNextDate1(today, endDate);
                Date date2 = C62Utilities.GetNextDate2(today, endDate);
                system.debug('here5');
                if(date1 < date2){
                    if(date1 < endDate.addDays(4)){ //need to make sure Saturdays are only ever 4 days after end date
                        today = endDate.addDays(4);
                        date1 = C62Utilities.GetNextDate1(today, endDate);
                        date2 = C62Utilities.GetNextDate2(today, endDate);
                        if(date1 <= date2){
                            newPayDate = date1;
                        }else{
                            newPayDate = date2;
                        }
                    }
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            //if today is 11 to 15 days after end date, set to the 2nd, 16th, or 15 days after end date, whichever is first
            }else if(today > endDate.addDays(11) && today < endDate.AddDays(15)){
                Date date1 = C62Utilities.GetNextDate2(today, endDate);
                Date date2 = endDate.AddDays(15);
                system.debug('here6');
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            // Otherwise set to 15 days after end date
            }else{
                system.debug('here7');
                newPayDate = endDate.AddDays(15);
            }
            
        // Auto Renew Schedule pages 7 - 11
        }else if(fam.Equals('Email') || fam.Equals('OptiMYz') || fam.Equals('VBC') 
            || fam.Equals('Platinum Services') || fam.Equals('Standard Services')
            || fam.Equals('No Services')){
            // if today is before the end date, set to 4 days before end date
            if(today <= endDate.AddDays(-4)){
                system.debug('here1');
                newPayDate = endDate.AddDays(-4);
                //new payment within correct range, set to 1 day before end date
            }else if(today > endDate.AddDays(-4) && today <= endDate.AddDays(-1)){
                system.debug('here3');
                newPayDate = endDate.AddDays(-1);
            //new payment within correct range, set to 1 day after end date
            }else if(today > endDate.AddDays(-1) && today <= endDate.AddDays(1)){
                system.debug('here4');
                newPayDate = endDate.AddDays(1);
            // if Payment date 1 days after the end date, set to next Saturday (4-11 days after end date) 
            // OR the 2nd of the next month, whichever is first.
            }else if(today > endDate.addDays(1) && today <= endDate.AddDays(11)){
                Date date1 = C62Utilities.GetNextDate1(today, endDate);
                Date date2 = C62Utilities.GetNextDate2(today, endDate);
                system.debug('here5');
                if(date1 < date2){
                    if(date1 < endDate.addDays(4)){ //need to make sure Saturdays are only ever 4 days after end date
                        today = endDate.addDays(4);
                        date1 = C62Utilities.GetNextDate1(today, endDate);
                        date2 = C62Utilities.GetNextDate2(today, endDate);
                        if(date1 <= date2){
                            newPayDate = date1;
                        }else{
                            newPayDate = date2;
                        }
                    }
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            //if today is between 11 and 15 days past end date, set to 2nd, 16th, or 15 days after end date, whichever is first
            }else if(today > endDate.addDays(11) && today < endDate.AddDays(15)){
                Date date1 = C62Utilities.GetNextDate2(today, endDate);
                Date date2 = endDate.AddDays(15);
                system.debug('here6');
                if(date1 <= date2){
                    newPayDate = date1;
                }else{
                    newPayDate = date2;
                }
            // Otherwise set to 15 days after end date
            }else{
                system.debug('here7');
                newPayDate = endDate.AddDays(15);
            }
        }
        
        return newPayDate;
    }
    
    //moves the end date forward on successfully paid Orders
    public static List<Bill62__Order__c> advanceEndDate(List<Bill62__Order__c> orderList){
        Set<String> orderIds = new Set<String>();
        for(Bill62__Order__c theOrder: orderList){
            orderIds.add(theOrder.Id);
        }
        Map<String, List<Bill62__Subscription__c>> orderToOrderLineMap = new Map<String, List<Bill62__Subscription__c>>();
        Map<String, Payment_Reason__c> payReasonMap = new Map<String, Payment_Reason__c>();
        List<Payment_Reason__c> payReasonList = [SELECT ID, Months__c, Product__c FROM Payment_Reason__c];
        for(Payment_Reason__c p: payReasonList){
            if(p.Product__c != null) payReasonMap.put(p.Product__c, p);
        }
        List<Bill62__Subscription__c> orderLineList = [SELECT ID, Order__c, Bill62__Product__c 
            FROM Bill62__Subscription__c WHERE Order__c IN :orderIds];
        for(Bill62__Subscription__c ol: orderLineList){
            if(orderToOrderLineMap.containsKey(ol.Order__c)){
                List<Bill62__Subscription__c> myList = orderToOrderLineMap.get(ol.Order__c);
                myList.add(ol);
                orderToOrderLineMap.put(ol.Order__c, myList);
            } else {
                List<Bill62__Subscription__c> myList = new List<Bill62__Subscription__c>();
                myList.add(ol);
                orderToOrderLineMap.put(ol.Order__c, myList);
            }
        }
        for(Bill62__Order__c theOrder: orderList){
            //system.debug(orderToOrderLineMap.get(theOrder.Id));
            //system.debug(payReasonMap.get(orderToOrderLineMap.get(theOrder.Id)[0].Bill62__Product__c));
            //system.debug(payReasonMap.get(orderToOrderLineMap.get(theOrder.Id)[0].Bill62__Product__c).Months__c);
            if(payReasonMap.containsKey(orderToOrderLineMap.get(theOrder.Id)[0].Bill62__Product__c)){
                Integer months = Integer.valueof(payReasonMap.get(orderToOrderLineMap.get(theOrder.Id)[0].Bill62__Product__c).Months__c);
                theOrder.Bill62__End_Date__c = theOrder.Bill62__End_Date__c.addMonths(months);
            }
        }
        return orderList;
    }
    
    //required: Subscription.Bill62__Product__r.Changes_To__c
    public static List<Bill62__Subscription__c> mutateProducts(List<Bill62__Subscription__c> subList){ 
        for(Bill62__Subscription__c sub: subList){
            if(sub.Bill62__Product__r.Changes_To__c != null) 
                sub.Bill62__Product__c = sub.Bill62__Product__r.Changes_To__c;
        }
        return subList;
    }
    
    //moves the end date backward on unsuccessfully paid Orders.  This is only temporary, don't keep it around!
    public static List<Bill62__Order__c> rollbackEndDate(List<Bill62__Order__c> orderList){
        Set<String> orderIds = new Set<String>();
        for(Bill62__Order__c theOrder: orderList){
            orderIds.add(theOrder.Id);
        }
        Map<String, List<Bill62__Subscription__c>> orderToOrderLineMap = new Map<String, List<Bill62__Subscription__c>>();
        Map<String, Payment_Reason__c> payReasonMap = new Map<String, Payment_Reason__c>();
        List<Payment_Reason__c> payReasonList = [SELECT ID, Months__c, Product__c FROM Payment_Reason__c];
        for(Payment_Reason__c p: payReasonList){
            if(p.Product__c != null) payReasonMap.put(p.Product__c, p);
        }
        List<Bill62__Subscription__c> orderLineList = [SELECT ID, Order__c, Bill62__Product__c 
            FROM Bill62__Subscription__c WHERE Order__c IN :orderIds];
        for(Bill62__Subscription__c ol: orderLineList){
            if(orderToOrderLineMap.containsKey(ol.Order__c)){
                List<Bill62__Subscription__c> myList = orderToOrderLineMap.get(ol.Order__c);
                myList.add(ol);
                orderToOrderLineMap.put(ol.Order__c, myList);
            } else {
                List<Bill62__Subscription__c> myList = new List<Bill62__Subscription__c>();
                myList.add(ol);
                orderToOrderLineMap.put(ol.Order__c, myList);
            }
        }
        for(Bill62__Order__c theOrder: orderList){
            if(payReasonMap.containsKey(orderToOrderLineMap.get(theOrder.Id)[0].Bill62__Product__c)){
                Integer months = Integer.valueof(payReasonMap.get(orderToOrderLineMap.get(theOrder.Id)[0].Bill62__Product__c).Months__c)*-1;
                theOrder.Bill62__End_Date__c = theOrder.Bill62__End_Date__c.addMonths(months);
            }
        }
        return orderList;
    }
}