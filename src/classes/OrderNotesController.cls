/*
@Name            : OrderNotesController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for inline Order Notes page
*/


public with sharing class OrderNotesController {
  public Bill62__Order__c theOrder {get;set;}
  
  public OrderNotesController(ApexPages.StandardController controller){
    theOrder = [SELECT Id, Order_IDtxt__c, Name FROM Bill62__Order__c WHERE Id=:controller.getId()];
  }
  
  public ApexPages.StandardSetController setCon {
        get {
            
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [SELECT Name, Note_Date__c, Priority__c, Note__c, CreatedDate FROM Rep_Notes__c WHERE Order__c = :theOrder.Id ORDER BY Priority__c DESC, Note_Date__c]));
            }
            return setCon;
        }
        set;
   }
    
   public List<Rep_Notes__c> getList() {
         setCon.setpagesize(5);
         return (List<Rep_Notes__c>) setCon.getRecords();
   }
}