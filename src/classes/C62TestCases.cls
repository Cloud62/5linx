/*
@Name            : C62TestCases
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for Case code
*/


@IsTest
public class C62TestCases {
    
    @IsTest
    public static void test01(){
        User u = [SELECT Id FROM User WHERE Alias = 'nhall'];
        System.RunAs(u) {
            Case c = New Case();
            insert c;
            c.AssignmentRule__c = true;
            update c;
        }
    }
}