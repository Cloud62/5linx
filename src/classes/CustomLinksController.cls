/*
@Name            : CustomLinksController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Custom Links page
*/


public with sharing class CustomLinksController{
      public Account thisRep {get;set;}
      public CustomLinksController(ApexPages.StandardController controller){
        thisRep = [SELECT Id, Entity_ID__c FROM Account WHERE Id = :controller.getId() LIMIT 1];
      }
      
      public PageReference PayInfo(){
          return new PageReference('https://tc.5linx.com/reps_PaymentInformation.aspx?ID='+thisRep.Entity_ID__c);
      }
      
        public PageReference PerCust(){
          return new PageReference('https://tc.5linx.com/reps_PersonalCustomers.aspx?ID='+thisRep.Entity_ID__c+'&GroupBy=B');
      }
      
        public PageReference PerCustProd(){
          return new PageReference('https://tc.5linx.com/reps_PersonalCustomers.aspx?ID='+thisRep.Entity_ID__c+'&GroupBy=P');
      }
      
        public PageReference PosGoal(){
          return new PageReference('https://tc.5linx.com/reps_PositionGoals.aspx?ID='+thisRep.Entity_ID__c);
      }
      
      public PageReference Hierarchy(){
          return new PageReference('http://tcreports.5linx.com/ReportServer/Pages/ReportViewer.aspx?%2fTC_Reports_Production%2fSalesForce%2freps_ULandFL&rs:Command=Render&intEntityId='+thisRep.Entity_ID__c);
      }
}