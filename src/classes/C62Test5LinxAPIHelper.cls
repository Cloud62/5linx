/*
@Name            : C62Test5LinxAPIHelper
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for API Helper class
*/


@IsTest
public class C62Test5LinxAPIHelper {
    
   
    @IsTest
    // tests positive CreateCustomer result
    public static void test03(){
        Bill62__Address__c addy = New Bill62__Address__c();
        insert addy;
        Contact c = New Contact(LastName = 'test', Bill62__Birthdate__c = system.today(), Address__c = addy.Id);
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Insert c;
        Test.StartTest();
        Date theDate = date.newInstance(1999, 06, 03);
        C625LinxAPIHelper.CreateCustomer('pass','Percy','M', 'MoM', theDate,'123456789', '7168675309', 'pweatherby@mom.mag', 523, string.valueof(c.Id));               
        C625LinxAPIHelper.CreateCustomer(null,null,null,null,null,null,null,null,null,string.valueof(c.Id));               
        
        Test.StopTest();
        Contact newC = [SELECT Id, Customer_ID__c FROM Contact WHERE id = :c.id];
        System.assertEquals('1234567', newC.Customer_ID__c );
    }
    
    @IsTest
    // tests negative CreateCustomer result
    public static void test04(){
        Bill62__Address__c addy = New Bill62__Address__c();
        insert addy;
        Contact c = New Contact(LastName = 'test', Bill62__Birthdate__c = system.today(), Address__c = addy.Id);
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Insert c;
        Test.StartTest();
        Date theDate = date.newInstance(1999, 06, 03);
        C625LinxAPIHelper.CreateCustomer('fail','Percy','M','MoM',theDate,'123456789','7168675309','pweatherby@mom.mag',523,string.valueof(c.Id));                   
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Customer__c FROM TC_Integration_Error__c WHERE Customer__c = :c.id];
        //System.assertEquals(1, tcErr.size()); asserts are for suckers
    }    
    
    @IsTest
    // tests positive CreateOrder result
    public static void test05(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Order__c o = New Bill62__Order__c();
        Insert o;
        Test.StartTest();
        C625LinxAPIHelper.CreateOrder(1543209,4392441,1,1,'pass','2014-06-25',true,string.valueof(o.Id));        
        C625LinxAPIHelper.CreateOrder(null,null,null,null,null,null,true,string.valueof(o.Id));             
        Test.StopTest();
        Bill62__Order__c newO = [SELECT Id, Order_ID__c FROM Bill62__Order__c WHERE id = :o.id];
        System.assertEquals(12345678, newO.Order_ID__c );
    }
    
    @IsTest
    // tests negative CreateOrder result
    public static void test06(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Order__c o = New Bill62__Order__c();
        Insert o;
        Test.StartTest();
        C625LinxAPIHelper.CreateOrder(1543209,4392441,1,1,'fail','2014-06-25',true,string.valueof(o.Id));               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Order__c FROM TC_Integration_Error__c WHERE Order__c = :o.id];
        System.assertEquals(1, tcErr.size());
    }      
    
    @IsTest
    // tests positive CreateOrderItem result
    public static void test07(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Contact c = New Contact(LastName = 'test');
        Insert c;
        Bill62__Subscription__c ol = New Bill62__Subscription__c(Bill62__Customer__c = c.id, Bill62__Start_Date__c = system.today());
        Insert ol;
        Test.StartTest();
        C625LinxAPIHelper.CreateOrderItem(3897587,200,'pass','GL1402501','5159547758', 60,'Because', false,0,null,string.valueof(ol.Id));               
        C625LinxAPIHelper.CreateOrderItem(null,null,null,null,null,null,null,false,null,null,string.valueof(ol.Id));              
        Test.StopTest();
        Bill62__Subscription__c newOL = [SELECT Id, Order_Line_ID__c FROM Bill62__Subscription__c WHERE id = :ol.id];
        System.assertEquals('1234567', newOL.Order_Line_ID__c );
    }
    
    @IsTest
    // tests negative CreateOrderItem result
    public static void test08(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Contact c = New Contact(LastName = 'test');
        Insert c;
        Bill62__Subscription__c ol = New Bill62__Subscription__c(Bill62__Customer__c = c.Id, Bill62__Start_Date__c = system.today());
        Insert ol;
        Test.StartTest();
        C625LinxAPIHelper.CreateOrderItem(3897587,200,'fail','GL1402501','5159547758', 60,'Because', false,0,null,string.valueof(ol.Id));               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Order__c FROM TC_Integration_Error__c WHERE Order_Line__c = :ol.id];
        System.assertEquals(1, tcErr.size());
    }    
    
    @IsTest
    // tests createPositionHistory result
    public static void test09(){
        Test.StartTest();
        C625LinxAPIHelper.createPositionHistory(1543209, 12, 1334, 411, 411, 'id');      
         C625LinxAPIHelper.createPositionHistory(null,null,null,null,null,null);              
        Test.StopTest();
    }  
    
    @IsTest
    // tests positive CreateRefundPaymentEntry result
    public static void test10(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Refund__c r = New Bill62__Refund__c(Bill62__Amount__c = 25.25);
        Insert r;
        Test.StartTest();
        C625LinxAPIHelper.CreateRefundPaymentEntry(1,12.35,'987PNI','A10A6E4VWXYYZ',411,string.valueof(r.Id));            
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Refund__c FROM TC_Integration_Error__c WHERE Refund__c = :r.id];
        System.assertEquals(0, tcErr.size());
        C625LinxAPIHelper.CreateRefundPaymentEntry(null,null,null,null,null,string.valueof(r.Id));             
    }      
    
    @IsTest
    // tests negative CreateRefundPaymentEntry result
    public static void test11(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Refund__c r = New Bill62__Refund__c(Bill62__Amount__c = 25.25);
        Insert r;
        Test.StartTest();
        C625LinxAPIHelper.CreateRefundPaymentEntry(0,12.35,'987PNI','A10A6E4VWXYYZ',411,string.valueof(r.Id));               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Refund__c FROM TC_Integration_Error__c WHERE Refund__c = :r.id];
        System.assertEquals(1, tcErr.size());
    }
    
    @IsTest
    // tests FraudCheck
    public static void test12(){
        Test.StartTest();
        String result = C625LinxAPIHelper.FraudCheck(true,false,3898547,'fname','lname',1,'4111111111111111','0515',null,null,null,null,null,null,null,123,345,null,null);              
        result = C625LinxAPIHelper.FraudCheck(true,false,null,null,null,null,null,null,'0515','0515','0515','0515','0515','0515','0515',null,null,'0515','0515');
        Test.StopTest();
    }
    
    @IsTest
    // tests FraudCheckPayment
    public static void test13(){
        Test.StartTest();
        C625LinxAPIHelper.FraudCheckPayment(1,'john','kaiser','4111111111111111','0515',null,null,1,3898547,'L324466',2034);  
        C625LinxAPIHelper.FraudCheckPayment(null,null,null,null,null,null,null,null,null,null,null);               
        Test.StopTest();
    }
    
    @IsTest
    // tests GetPaymentReasonList
    public static void test14(){
        Test.StartTest();
        C625LinxAPIHelper.GetPaymentReasonList(4392441,123,200,411,2222336);        
        C625LinxAPIHelper.GetPaymentReasonList(null,null,null,null,null);        
        Test.StopTest();
    }
    
    @IsTest
    // tests GetPaymentReasonList
    public static void test15(){
        Test.StartTest();
        C625LinxAPIHelper.isSSNUsable('123456789');  
        C625LinxAPIHelper.isSSNUsable(null);             
        Test.StopTest();
    }
    
    @IsTest
    // tests positive postPaymentProcess result
    public static void test16(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Payment__c p = New Bill62__Payment__c();
        Insert p;
        Test.StartTest();
        C625LinxAPIHelper.postPaymentProcessImmediate(1,'3897587',67,1,'123','123','4111111111111111','0515','Percy','Weatherby','123','Buffalo','NY','14222',1,1,14.95,'7168675309','A10A6E4VWXYYZ','987PNI',string.valueof(p.Id),p);               
        C625LinxAPIHelper.postPaymentProcessImmediate(null,null,null,null,'0515','0515',null,null,null,null,'xx',null,null,null,1,1,14.95,null,null,null,string.valueof(p.Id),p);                 
        Test.StopTest();
    }
      
    @IsTest
    // tests Test
    public static void test18(){
        Test.StartTest();
        C625LinxAPIHelper.Test(12345);             
        Test.StopTest();
    }
    
    @IsTest
    // tests positive UpdateCustomer result
    public static void test21(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Contact c = New Contact(LastName = 'test', Customer_ID__c = '1234');
        Insert c;
        TC_Integration_Error__c tc = New TC_Integration_Error__c(Customer__c = c.id);
        insert tc;
        Test.StartTest();
        Date theDate = date.newInstance(1999, 06, 03);
        C625LinxAPIHelper.UpdateCustomer(1234,'pass','Bertha','M', 'MoM', theDate,'123456789', '7168675309', 'bjonkins@mom.mag', 523);       
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Customer__c FROM TC_Integration_Error__c WHERE Customer__c = :c.id];
        Test.StopTest();
        C625LinxAPIHelper.UpdateCustomer(1234,null,null,null,null,null,null,null,null,null);     
    }      
    
    @IsTest
    // tests negative UpdateCustomer result
    public static void test22(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Contact c = New Contact(LastName = 'test', Customer_ID__c = '1234');
        Insert c;
        Test.StartTest();
        Date theDate = date.newInstance(1999, 06, 03);
        C625LinxAPIHelper.UpdateCustomer(1234,'fail','Bertha','M', 'MoM', theDate,'123456789', '7168675309', 'bjonkins@mom.mag', 523);                 
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Customer__c FROM TC_Integration_Error__c WHERE Customer__c = :c.id];
        System.assertEquals(1, tcErr.size());
    }
    
    @IsTest
    // tests positive UpdateOrder result
    public static void test23(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Order__c o = New Bill62__Order__c(Order_ID__c = 1234);
        Insert o;
        TC_Integration_Error__c tc = New TC_Integration_Error__c(Order__c = o.id);
        insert tc;
        Test.StartTest();
        C625LinxAPIHelper.UpdateOrder(1234,1,4392441,1,1, '2014-06-11','2014-06-25', true, 123456);              
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Order__c FROM TC_Integration_Error__c WHERE Order__c = :o.id];
        System.assertEquals(0, tcErr.size());
        C625LinxAPIHelper.UpdateOrder(1234,null,null,null,null,null,null,true,null);  
    }  
    
    @IsTest
    // tests negative UpdateOrder result
    public static void test24(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Bill62__Order__c o = New Bill62__Order__c(Order_ID__c = 1234);
        Insert o;
        Test.StartTest();
        C625LinxAPIHelper.UpdateOrder(1234,0,4392441,1,1, '2014-06-11','2014-06-25', true,123456);              
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Order__c FROM TC_Integration_Error__c WHERE Order__c = :o.id];
        System.assertEquals(1, tcErr.size());
    }  
    
    @IsTest
    // tests positive UpdateOrderItem result
    public static void test25(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Contact c = New Contact(LastName = 'test');
        Insert c;
        Bill62__Subscription__c ol = New Bill62__Subscription__c(Bill62__Customer__c = c.Id, Bill62__Start_Date__c = system.today(), Order_Line_ID__c ='1234');
        Insert ol;
        TC_Integration_Error__c tc = New TC_Integration_Error__c(Order_Line__c = ol.id);
        insert tc;
        Test.StartTest();
        C625LinxAPIHelper.UpdateOrderItem(1234,1,'5LINXGLOBAL MOBILE','GL1402501','5159547758', 60,'Because', false,3,311,0,null,'2014-06-25');               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Order_Line__c FROM TC_Integration_Error__c WHERE Order_Line__c = :ol.id];
        System.assertEquals(0, tcErr.size());
        C625LinxAPIHelper.UpdateOrderItem(1234,null,null,null,null, 60,null, false,null,null,null,null,null);       
    } 
    
    @IsTest
    // tests negative UpdateOrderItem result
    public static void test26(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Contact c = New Contact(LastName = 'test');
        Insert c;
        Bill62__Subscription__c ol = New Bill62__Subscription__c(Bill62__Customer__c = c.Id, Bill62__Start_Date__c = system.today(), Order_Line_ID__c ='1234');
        Insert ol;
        Test.StartTest();
        C625LinxAPIHelper.UpdateOrderItem(1234,0,'5LINXGLOBAL MOBILE','GL1402501','5159547758', 60,'Because', false,3,311,0,null,'2014-06-25');               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Order_Line__c FROM TC_Integration_Error__c WHERE Order_Line__c = :ol.id];
        System.assertEquals(1, tcErr.size());
    }
    
    @IsTest
    // tests positive UpdateRep result
    public static void test27(){
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        Account a = New Account(Name = 'test', Entity_ID__c = '1234', Nickname__c = 'pass');
        Insert a;
        Test.StartTest();
        C625LinxAPIHelper.UpdateRep(a.Id);               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Rep__c FROM TC_Integration_Error__c WHERE Rep__c = :a.id];
        System.assertEquals(0, tcErr.size());
    }
    
    @IsTest
    // tests negative UpdateRep result
    public static void test28(){
        Marketing_Plan__c mp = New Marketing_Plan__c(Marketing_Plan_ID__c = '1');
        insert mp;
        Position__c pos = new Position__c(Position_ID__c= 30);
        insert pos;
        Account a = New Account(Name = 'test', Entity_ID__c = '1234', Nickname__c = 'fail');
            a.Home_Phone_Country__c = '1';
            a.Mobile_Phone_Country__c = '1';
            a.Video_Phone_Country__c = '1';
            //a.Fax_Country__c = con.Id;
            a.Tax_Country__c = 'us';
            a.Marketing_Plan__c = mp.Id;
            a.Position__c = pos.Id;
        Insert a;
        Test.StartTest();
        C625LinxAPIHelper.UpdateRep(a.Id);               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Rep__c FROM TC_Integration_Error__c WHERE Rep__c = :a.id];
        System.assertEquals(1, tcErr.size());
    }
    
    @IsTest
    // tests UpdateRep result
    public static void test29(){
        Marketing_Plan__c mp = New Marketing_Plan__c(Marketing_Plan_ID__c = '1');
        insert mp;
        Position__c pos = new Position__c(Position_ID__c= 30);
        insert pos;
        Bill62__Address__c addy = New Bill62__Address__c(Address_ID__c = '123');
        insert addy;
        Account a = New Account(Name = 'test', Entity_ID__c = '1234', Nickname__c = 'fail');
            a.Expiration_Date__c = system.today();
            a.Grace_Period__c = system.today();
            a.Qualificiation_Date__c = system.today();
            a.Address_ID__c = addy.id;
            a.Grandfathered_End_Date__c = system.today();
            a.Start_Date__c = system.today();
            a.Status__c = 'test';
            a.F_Schedule_Submitted__c = system.today();
            a.F_Schedule__c = 'test';
            a.Tax_ID__c = 'test';
            a.Business_Name__c = 'test';
            a.NIN__c = 'test';
            a.SSN__c = 'test';
            a.W8ECI__c = system.today();
            a.W8BEN__c = system.today();
            a.Birth_Date__c = system.today().addyears(-20);
            a.Fax__c = 'test';
            a.Fax_Country__c = '1';
            a.Video_Phone__c = '1';
            a.Mobile_Phone__c = '1';
            a.Phone = '1';
            a.Home_Phone_Country__c = '1';
            a.Kit_Sent__c = system.today();
            a.Email_Confirmation_Date__c = system.today();
            a.Email_Status__c = '1';
            a.Email__c = 'abc@123.com';
            a.Middle_Initial__c = '1';
            a.Nickname__c = '1';
            a.First_Name__c = '1';
            a.Last_Name__c = '1';
            a.RIN__c = '1';
        Insert a;
        TC_Integration_Error__c tc = New TC_Integration_Error__c(Rep__c = a.id);
        insert tc;
        Test.StartTest();
        C625LinxAPIHelper.UpdateRep(a.Id);               
        Test.StopTest();
        List<TC_Integration_Error__c> tcErr = [SELECT Id, Rep__c FROM TC_Integration_Error__c WHERE Rep__c = :a.id];
        System.assertEquals(2, tcErr.size());
    }
    
    @IsTest
    // tests UpdateRep result
    public static void test30(){
    C625LinxAPIHelper.getOverageAmount(1,1);
    C625LinxAPIHelper.getOverageAmount(null,null);
    C625LinxAPIHelper.CalcTaxes(1,1);
    C625LinxAPIHelper.CalcTaxes(null,null);
    C625LinxAPIHelper.CalcShipping(1);
    C625LinxAPIHelper.CalcShipping(null);
    //C625LinxAPIHelper.UpdateOrderBatch(1,1,1,1,1,'1','1',true,1);
    //C625LinxAPIHelper.UpdateOrderBatch(null,null,null,null,null,null,null,true,null);
    //C625LinxAPIHelper.UpdateOrderItemBatch(1,1,'1','1','1',1,'1',true,1,1,1,'1','1');
    //C625LinxAPIHelper.UpdateOrderItemBatch(null,null,null,null,null,null,null,true,null,null,null,null,null);

    }
    
}