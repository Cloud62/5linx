/*
@Name               : FiveLinxBatchProcessPayments
@Author             : customersuccess@cloud62.com
@Date               : September 24, 2014
@Description        : Process all of today's Payments
*/
global class FiveLinxBatchProcessPayments implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable,Database.Stateful {
    global List<String> logList;
    global User currentUser;

    /*
    @Name :FiveLinxBatchProcessPayments
    @Description :Constructor
    */
    global FiveLinxBatchProcessPayments() {

    }

    /*
    @Name :start
    @Description :Return the payment records where payment date is today and status is queued
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        C62Utilities.cSetExecuted.add('paymentBatch');
        logList = new List<String>();
        Date payDate = Date.Today();
        String query = '';
        query += 'SELECT Id,Name,Bill62__Customer__c,Bill62__Customer__r.FirstName, ';
        query += 'Bill62__Customer__r.Customer_ID__c,  Bill62__Subscription__c, TC_Integration_Error__c, ';
        query += 'Bill62__Order__r.Bill62__Account__r.RIN__c, Bill62__Order__r.Bill62__Account__r.Entity_ID__c, Bill62__Recurring_Payment__c,  ';
        query += 'Bill62__Customer__r.Address__r.Country__r.Country_ID__c,Bill62__Customer__r.Address__r.Bill62__City__c, Bill62__Customer__r.Address__r.Bill62__Street_1__c, ';
        query += 'Bill62__Customer__r.Address__r.Bill62__State_Province__c, Bill62__Customer__r.Address__r.Bill62__Zip_Postal_Code__c, Bill62__Customer__r.HomePhone, ';
        query += 'Bill62__Subscription__r.Order_ID__c, Bill62__Transaction_Id__c, Bill62__Approval_Code__c, ';
        query += 'Bill62__Subscription__r.Order__r.Order_IDtxt__c, Bill62__Subscription__r.Bill62__Product__c, ';
        
        query += 'Bill62__Order__r.Order_Id__c, Bill62__Subscription__r.Order_Line_ID__c, Bill62__Subscription__r.Bill62__Product__r.External_ID__c, ';
        query += 'Bill62__Amount__c,Bill62__Payment_Method__c,Bill62__Payment_Method__r.Bill62__Card_Holder_Name__c,';
        query += 'Bill62__Subscription__r.Bill62__Product__r.Has_Tax__c, Bill62__Subscription__r.Bill62__Product__r.Has_Shipping__c,';
        query += 'Bill62__Customer__r.LastName,Bill62__Payment_Method__r.Bill62__Card_Number__c,';
        query += 'Bill62__Payment_Method__r.Payment_Method_ID__c,Bill62__Payment_Method__r.Bill62__Account_Number__c,';
        query += 'Bill62__Payment_Method__r.Bill62__Card_Type__c,Bill62__Payment_Method__r.Bill62__Token__c,Bill62__Payment_Method__r.Bill62__CVV_Code__c,';
        query += 'Bill62__Payment_Method__r.Bill62__Expiration_Date__c,Bill62__Payment_Method__r.Bill62__Payment_Type__c,';
        query += 'Bill62__Payment_Method__r.Bill62__Routing_Number__c, Bill62__Payment_Method__r.Name,Bill62__Payment_Method__r.Bill62__Billing_Address__c,';
        query += 'Bill62__Number_Of_Authorization_Attempts__c, Bill62__Payment_Gateway__c, Bill62__Payment_Gateway__r.Name ';
        query += 'FROM Bill62__Payment__c WHERE Bill62__Payment_Date__c<=:payDate AND ';
        query += '(Bill62__Status__c=\'Queued\' OR Bill62__Status__c=\'Queued Retry\' OR Bill62__Status__c=\'Exception\')';
        return Database.getQueryLocator(query);
    }

    /*
    @Name :execute
    @Description :Only 2 payments can be processed per batch, because there's a limt of 10
    callouts per batch, and each payment requires 4
    */
    global void execute(SchedulableContext ctx) {
        FiveLinxBatchProcessPayments batch1 = new FiveLinxBatchProcessPayments();
        ID batchprocessid = Database.executeBatch(batch1,16);
    }

    /*
    @Name :execute
    @Description :do the transaction
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        User currentUser = [SELECT ID, TC_User_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
        Bill62__C62Billing_Config__c billSettings = Bill62__C62Billing_Config__c.getOrgDefaults();
        Decimal numOfAttempts = billSettings.Bill62__Number_of_Payment_Attempts__c;
        Bill62.PaymentUtilities utils = new Bill62.PaymentUtilities();
        
        //TODO: remove this hardcoded paymeth
        Bill62__Payment_Method__c payMeth;
        if(!Test.isRunningTest()){
            /*payMeth = [SELECT Payment_Method_ID__c,Bill62__Account_Number__c,Bill62__Card_Number__c, Bill62__Card_Type__c, Bill62__Token__c,
                Bill62__CVV_Code__c, Bill62__Expiration_Date__c, Bill62__Payment_Type__c, Bill62__Routing_Number__c,
                Name, ID, Bill62__Billing_Address__c FROM Bill62__Payment_Method__c WHERE Name = 'Cloud62 Test Card' LIMIT 1];*/
        } else {
            payMeth = [SELECT Payment_Method_ID__c,Bill62__Account_Number__c,Bill62__Card_Number__c, Bill62__Card_Type__c, Bill62__Token__c,
                Bill62__CVV_Code__c, Bill62__Expiration_Date__c, Bill62__Payment_Type__c, Bill62__Routing_Number__c,
                Name, ID, Bill62__Billing_Address__c FROM Bill62__Payment_Method__c LIMIT 1];
        }
        
        C62Utilities.cSetExecuted.add('paymentBatch');
        
        //for querying Payment Methods
        Set<String> paymentMethodIDSet = new Set<String>();
        
        //gotta get payment reasons for PostPaymentProcess
        Set<ID> prodIDs = new Set<ID>();
        Map<String, String> paymentToProductMap = new Map<String, String>();
        Map<String, Payment_Reason__c> paymentReasonMap = new Map<String, Payment_Reason__c>();
        for(Bill62__Payment__c newPay: (List<Bill62__Payment__c>) scope){
            if(newPay.Bill62__Subscription__r.Bill62__Product__c != null) {
                prodIDs.add(newPay.Bill62__Subscription__r.Bill62__Product__c);
                paymentToProductMap.put(newPay.ID, newPay.Bill62__Subscription__r.Bill62__Product__c);
            }
            if(newPay.Bill62__Payment_Method__c != null){
                paymentMethodIDSet.add(newPay.Bill62__Payment_Method__c);
            }
        }
        
        List<Bill62__Payment_Method__c> payMethList = [SELECT ID, Name, Bill62__Card_Holder_Name__c,
        Bill62__Card_Number__c,Payment_Method_ID__c,Bill62__Card_Type__c,Bill62__Expiration_Date__c,Bill62__Billing_Address__c
        FROM Bill62__Payment_Method__c WHERE ID IN :paymentMethodIDSet];
        
        Map<String, Bill62__Payment_Method__c> paymentMethodMap = new Map<String, Bill62__Payment_Method__c>();
        for(Bill62__Payment_Method__c pm: payMethList){
            paymentMethodMap.put(pm.Id, pm);
        }
        
        List<Payment_Reason__c> payReasonList = [SELECT ID, Product__c, Product__r.Name, External_ID__c FROM Payment_Reason__c WHERE Product__c IN :prodIDs];
        Map<String, String> productToPayReasonMap = new Map<String, String>();
        for(Payment_Reason__c pr: payReasonList){
            productToPayReasonMap.put(pr.Product__c, pr.External_ID__c);
            paymentReasonMap.put(pr.External_ID__c, pr);
        }
        
        Map<String, String> paymentToPRExtIDMap = new Map<String, String>();
        for(String s: paymentToProductMap.keySet()){
            paymentToPRExtIDMap.put(s, productToPayReasonMap.get(paymentToProductMap.get(s)));
        }
        
        List<Auto_Renew_Payment_Error__c> errorList = new List<Auto_Renew_Payment_Error__c>();
        
        //now do the rest of the proccessing
        for(Bill62__Payment__c newPay: (List<Bill62__Payment__c>) scope){
            
            //if no payment method, create error
            if(paymentMethodMap.containsKey(newPay.Bill62__Payment_Method__c)){
                payMeth = paymentMethodMap.get(newPay.Bill62__Payment_Method__c);
            } else {
                Auto_Renew_Payment_Error__c err = new Auto_Renew_Payment_Error__c();
                err.Payment__c = newPay.Id;
                err.Type__c = 'Salesforce';
                err.Error_Message__c = 'Payment has no Payment Method set.';
                errorList.add(err);
                continue;
            }
            
            try{
                String firstName, lastName;
                if(newPay.Bill62__Customer__c != null){
                    if(newPay.Bill62__Customer__r.FirstName != null) firstName = newPay.Bill62__Customer__r.FirstName;
                    if(newPay.Bill62__Customer__r.LastName != null) lastName = newPay.Bill62__Customer__r.LastName;
                }
                Decimal addition = 0.0;
                //calculate tax amount
                if(newPay.Bill62__Subscription__r.Bill62__Product__r.Has_Tax__c && !Test.isRunningTest()){
                    addition += C625LinxAPIHelper.CalcTaxes(integer.valueof(newPay.Bill62__Order__r.Order_Id__c),integer.valueof(newPay.Bill62__Subscription__r.Order_Line_ID__c));
                }
                // calulate shipping amount     
                if(newPay.Bill62__Subscription__r.Bill62__Product__r.Has_Shipping__c && !Test.isRunningTest()){   
                    addition += C625LinxAPIHelper.CalcShipping(integer.valueof(newPay.Bill62__Order__r.Order_Id__c));
                }
                if(!Test.isRunningTest()) addition += C625LinxAPIHelper.getOverageAmount(integer.valueof(newPay.Bill62__Order__r.Order_Id__c), Integer.valueOf(newPay.Bill62__Subscription__r.Bill62__Product__r.External_ID__c));
                newPay.Bill62__Amount__c += addition;
                newPay.Bill62__Amount__c = newPay.Bill62__Amount__c.setScale(2, System.RoundingMode.CEILING);
                //TODO: newPay.Bill62__Currency_Code
                
                if(paymentReasonMap.containsKey(paymentToPRExtIDMap.get(newPay.Id)))
                    newPay.Bill62__Description__c = paymentReasonMap.get(paymentToPRExtIDMap.get(newPay.Id)).Product__r.Name;
                
                
                newPay.Bill62__Status__c='Paid';
                newPay.Bill62__Last_Authorization_Attempt__c=DateTime.NOW();
                
                /* removing all this code for testing non-live payments
                
                Bill62.C62PaymentGatewayClasses.MakePaymentResponse result;
                if(!Test.isRunningTest()){
                    try{
                        //result = utils.MakePayment2(payMeth, newPay, firstName, lastName);
                    } catch(Exception e){
                        newPay.Bill62__Status__c = 'Exception';
                        Auto_Renew_Payment_Error__c err = new Auto_Renew_Payment_Error__c();
                        err.Payment__c = newPay.Id;
                        err.Type__c = 'Payment Gateway';
                        String error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                        err.Error_Message__c = error;
                        errorList.add(err);
                    }
                }
                
                if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                    newPay.Bill62__Status__c='Paid';
                    newPay.Bill62__Amount_Used__c = newPay.Bill62__Amount__c;
                    newPay.Bill62__Transaction_Id__c = result.TransactionID;
                    newPay.Bill62__Approval_Code__c = result.AuthorizationID;
                    newPay.Bill62__Last_Authorization_Attempt__c=DateTime.NOW();
                    
                    //send to PostPaymentProcess
                    if(!Test.isRunningTest()){
                        newPay = postPaymentProcess( newPay, payMeth, paymentToPRExtIDMap.get(newPay.Id));
                        newPay.Bill62__Processing_Message__c = null;
                        //this is only if it fails
                        if(newPay.External_ID__c == null){ //postPaymentProcess failed
                            Auto_Renew_Payment_Error__c err = new Auto_Renew_Payment_Error__c();
                            err.Payment__c = newPay.Id;
                            err.Type__c = 'TC Integration';
                            err.Error_Message__c = newPay.Bill62__Processing_Message__c;
                            errorList.add(err);
                            try{
                                Bill62.C62PaymentGatewayClasses.VoidAuthorizationResponse refundResult = utils.voidAuthorization(newPay);
                                if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                                    newPay.Bill62__Status__c = 'Refunded';
                                }
                            } catch(Exception e){
                                newPay.Bill62__Status__c = 'Exception';
                                newPay.Bill62__Processing_Message__c = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                                String error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                                err.Payment__c = newPay.Id;
                                err.Type__c = 'Payment Gateway';
                                err.Error_Message__c = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                                errorList.add(err);
                            }
                        } else { //postPaymentProcess succeeded
                            //it's all good, the payment was successful
                            /*try{
                                Bill62.C62PaymentGatewayClasses.CaptureAuthorizationResponse captureResult = utils.CaptureAuthorization(newPay);
                                if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                                    //excellent!  it worked.
                                } else {
                                    Auto_Renew_Payment_Error__c err = new Auto_Renew_Payment_Error__c();
                                    err.Payment__c = newPay.Id;
                                    err.Type__c = 'Payment Gateway';
                                    String error = captureResult.ErrorDescription;
                                    err.Error_Message__c = error;
                                    errorList.add(err);
                                }
                            } catch(Exception e){
                                Auto_Renew_Payment_Error__c err = new Auto_Renew_Payment_Error__c();
                                err.Payment__c = newPay.Id;
                                err.Type__c = 'Salesforce';
                                String error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                                err.Error_Message__c = error;
                                errorList.add(err);
                            }
                        }
                    }
                    
                    if(newPay.Bill62__Number_Of_Authorization_Attempts__c == null){
                        newPay.Bill62__Number_Of_Authorization_Attempts__c = 1;
                    }else{
                        newPay.Bill62__Number_Of_Authorization_Attempts__c++;
                    }
                } else { //not successful payment
                    
                    //add auto renew error
                    Auto_Renew_Payment_Error__c err = new Auto_Renew_Payment_Error__c();
                    err.Payment__c = newPay.Id;
                    err.Type__c = 'Payment Gateway';
                    String error = result.ErrorDescription;
                    err.Error_Message__c = error;
                    errorList.add(err);
                    
                    newPay.Bill62__Last_Authorization_Attempt__c=DateTime.NOW();
                    if(newPay.Bill62__Number_Of_Authorization_Attempts__c == null){
                        newPay.Bill62__Number_Of_Authorization_Attempts__c = 1;
                    }else{
                        newPay.Bill62__Number_Of_Authorization_Attempts__c++;
                    }
                    if(newPay.Bill62__Number_Of_Authorization_Attempts__c>=numOfAttempts){
                        newPay.Bill62__Status__c='Rejected';
                    }else{
                        newPay.Bill62__Status__c= 'Queued Retry';
                    }
                }
                */
            } catch(Exception e){
                newPay.Bill62__Status__c = 'Exception';
                newPay.Bill62__Processing_Message__c = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                //logList.add(JSON.serializePretty(newPay));
                String error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                logList.add(error);
                Auto_Renew_Payment_Error__c err = new Auto_Renew_Payment_Error__c();
                err.Payment__c = newPay.Id;
                err.Type__c = 'Salesforce';
                err.Error_Message__c = error;
                errorList.add(err);
            }
        }//end of giant for loop
        
        update (List<Bill62__Payment__c>) scope;
        insert errorList;
    }
    
    /*
    @Name :finish
    @Description :finish method
    */
    global void finish(Database.BatchableContext BC) {
        //don't do nothin!
        //send an email with the results
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'rich@cloud62.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Payment Batch Errors');
        mail.setPlainTextBody(String.join(logList, '\r\n\r\n'));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        String cronId = System.ScheduleBatch(new FiveLinxBatchRescheduleBilling(),'FiveLinxBatchRescheduleBilling'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,1000);
    }
    
    public Bill62__Payment__c postPaymentProcess(Bill62__Payment__c p, Bill62__Payment_Method__c payMeth, String payReasonID){
        
        Integer intPayMethodId;
        String FName, LName, cardNum, expDate, routingNum, bankNum;
        //intPayMethodId = integer.valueof(payMeth.Payment_Method_ID__c);
        intPayMethodId = findPayMethodId(payMeth.Bill62__Card_Type__c);
        if(p.Bill62__Customer__c != null){
            if(p.Bill62__Customer__r.FirstName != null) FName = p.Bill62__Customer__r.FirstName;
            if(p.Bill62__Customer__r.LastName != null) LName = p.Bill62__Customer__r.LastName;
        }
        cardNum = payMeth.Bill62__Card_Number__c.replace('-','');
        String expDateTemp = string.valueof(payMeth.Bill62__Expiration_Date__c);
        if(expDateTemp != null){
             expDate = expDateTemp.substring(5,7) + expDateTemp.substring(2,4);
        }else{
            expDate = null;
        }
        
        Decimal pmtAmount = p.Bill62__Amount__c;
        String AuthCode = p.Bill62__Transaction_Id__c;
        String refId = p.Bill62__Approval_Code__c;
        // Rep info
        String RIN = p.Bill62__Order__r.Bill62__Account__r.RIN__c;
        Integer entityID;
        if(p.Bill62__Order__r.Bill62__Account__r.Entity_ID__c != null){
            entityId = integer.valueof(p.Bill62__Order__r.Bill62__Account__r.Entity_ID__c);
        }else{
            entityId = 1;
        }
        // Customer and address info
        String address = p.Bill62__Customer__r.Address__r.Bill62__Street_1__c;
        String city = p.Bill62__Customer__r.Address__r.Bill62__City__c; 
        String state = p.Bill62__Customer__r.Address__r.Bill62__State_Province__c;
        String zipCode = p.Bill62__Customer__r.Address__r.Bill62__Zip_Postal_Code__c;
        String phoneNum = p.Bill62__Customer__r.HomePhone;
        Integer countryId;
        if(p.Bill62__Customer__r.Address__r.Country__r.Country_ID__c != null){
            countryId = integer.valueof(p.Bill62__Customer__r.Address__r.Country__r.Country_ID__c);
        }else{
            countryId = 1;
        }
        Integer custId = integer.valueof(p.Bill62__Customer__r.Customer_ID__c);
        // eventually an order will be tied to payment
        String orderId;
        if ( p.Bill62__Subscription__r.Order__r.Order_IDtxt__c != null ){
            orderId = p.Bill62__Subscription__r.Order__r.Order_IDtxt__c;
        } else {
            orderId = '1';
        }
        Integer userId;
        if(currentUser != null){
            if ( currentUser.TC_User_ID__c != null ){
                userId = integer.valueof(currentUser.TC_User_ID__c);
            } else {
                userId = 1;
            }
        } else {
            userId = 1;
        }
        Integer payReason;
        if(payReasonID != null) payReason = Integer.valueOf(payReasonID);
        if(!Test.isRunningTest()){
            p = C625LinxAPIHelper.postPaymentProcessImmediate(entityID,orderID,payReason,intPayMethodId,bankNum,routingNum,cardNum,expDate,
                FName,LName,address,city,state,zipCode,countryId,countryId,pmtAmount,phoneNum,refID,AuthCode,p.id, p);
        }   
        return p;
    }
    
    public Integer findPayMethodID(String s){
        Integer i;
        if(s == 'Visa'){
            i = 1;
        }else if(s == 'MasterCard'){
            i = 2;
        }else if(s == 'American Express'){
            i = 3;
        }else if(s == 'Discover'){
            i = 4;
        }
        return i;
    }

}