/*
@Name            : SearchPageController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Search page
*/


public class SearchPageController { 
    public static String message {get;set;}
    public static List<Account> accList {get;set;}
    public static List<Contact> custList {get;set;}
    public static PageReference getResults(){   
        String ANI = ApexPages.currentPage().getParameters().get('ANI'); 
        String Phone = '('+ANI.substring(0,3)+') '+ANI.substring(3,6)+'-'+ANI.substring(6);
        String RIN = ApexPages.currentPage().getParameters().get('repRIN'); 
        accList = [SELECT Id, Name, Home_Phone__c, RIN__c FROM Account WHERE Home_Phone__c = :ANI OR RIN__c = :RIN];
        custList = [SELECT Id, Name, Bill62__Home_Phone__c, Email, Bill62__Birthdate__c FROM Contact WHERE Bill62__Home_Phone__c = :ANI OR Bill62__Home_Phone__c = :Phone];
        
        if (accList.size() >= 1 && custList.size() >= 1){
            message = null;
        } else if (accList.size() == 1 && custList.size() == 0){
            return new PageReference('/' + accList.get(0).Id);
        } else if (custList.size() == 1 && accList.size() == 0){
            return new PageReference('/' + custList.get(0).Id);
        } else {
            message = 'No results found';
        }
        return null;
    }
    
    /**added by matt to test */
    public PageReference writeData(){
        return null;
    }
}