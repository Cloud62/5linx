/*
@Name            : AddTrainingController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Add Training page
*/


public with sharing class AddTrainingController {
    public Account theRep {get;set;}
    public List<SelectOption> posList {get;set;}
    public String PosId {get;set;}
    public Map<String,Position__c> posName {get;set;}
    public String errorMsg {get;set;}
    public Boolean showErr {get;set;}
    public String winMsg {get;set;}
    public Boolean showWin {get;set;}
    
    public AddTrainingController(ApexPages.StandardController controller){
        theRep = [SELECT Id, Entity_ID__c, Name FROM Account WHERE Id = :controller.getId() LIMIT 1];
        system.debug(theRep);        
        posList = new list<selectoption>();
        posName = New Map<String,Position__c>();
        for(Position__c p : [SELECT Id, Name, Position_ID__c FROM Position__c]){
            system.debug(string.valueof(p.Position_ID__c)); 
            posList.add(new SelectOption(string.valueof(p.Position_ID__c), p.Name));
            posName.put(string.valueof(p.Position_ID__c), p);
        }
        showErr = false;
        showWin = false;
    }
    
    public pageReference AddTraining(){
         // call AddRepPositionTraining
         String result;
         String xml_req;
            xml_req =  '{"intEntityId":"'+theRep.Entity_ID__c+'",';     
            xml_req +=  '"intTrainingPositionId":"'+PosId+'"}';
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();              
         String path = apiHelp.URL__c + 'AddRepPositionTraining';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         if(!Test.IsRunningTest()) {
            HttpResponse res = h.send(req);
            System.debug('xml_req: '+xml_req);
            system.debug('THIS IS THE BODY!!' + res.getBody());
            result = res.getBody();
         } else {
            result = 'This training already exisis for this rep position was not successful Endpoint not found Training position added successfully TCID": 339995, ...';
         }
         // process result of api call
         if (result.contains('This training already exisis for this rep')){
            showErr = true;
            errorMsg = 'This training already exisis for this rep';
         } else if (result.contains('position was not successful')){
            showErr = true;
            errorMsg = 'Adding training position was not successful';
         } else if (result.contains('Endpoint not found')){
            showErr = true;
            errorMsg = 'There was a problem reacing the TC API.  Please try again later.';
         }
         
         if (result.contains('Training position added successfully')){
            String tcId = result.substring(result.indexof('TCID')+7, result.indexof('TCID')+13);
            Rep_Training__c rt = New Rep_Training__c();
                rt.Date_Completed__c = system.today();
                rt.Name = posName.get(posId).Name;
                rt.Rep__c = theRep.Id;
                rt.Position__c = posName.get(posId).Id;
                rt.TC_ID__c = tcId;
            insert rt;
            
            showWin = true;
            winMsg = 'Training position added successfully';
         }
         return null;
    }
    
    public pageReference cancel(){
        return new PageReference('/'+theRep.Id);
    }
}