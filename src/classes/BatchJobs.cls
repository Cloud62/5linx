public class BatchJobs{

    public static final Integer numberofjobs = 20;
   
    public List<AsyncApexJob> batchList {
        get{
           
                batchList = [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, CreatedDate, 
                CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob 
                where JobType = 'BatchApex' and ApexClass.Name not in ('BatchUpload','LogUploader')
                order by CreatedDate desc limit :numberOfJobs];
                       
            
            return batchList;
        } set;}
    
   
    public List<BatchJob> getActiveBatchJobs(){
        List<BatchJob> bjList = new List<BatchJob>();
        for(BatchJob bj : getBatchJobs()){
            if(bj.job.Status == 'Processing' || bj.job.Status == 'Queued' || bj.job.Status == 'Preparing'){
                bjList.add(bj);
            }
        }
        
        return bjList;
    }
    public List<BatchJob> getCompletedBatchJobs(){
        List<BatchJob> bjList = new List<BatchJob>();
        for(BatchJob bj : getBatchJobs()){
            if(bj.job.Status == 'Completed' || bj.job.Status == 'Failed' || bj.job.Status == 'Aborted'){
                bjList.add(bj);
            }
        }
        
        return bjList;
    }
    public List<BatchJob> getBatchJobs(){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        List<BatchJob> batchJobs = new List<BatchJob>();
        
        
        //Query the Batch apex jobs
        for(AsyncApexJob a : batchList ){
            Double itemsProcessed = a.JobItemsProcessed;
            Double totalItems = a.TotalJobItems;

            BatchJob j = new BatchJob();
            j.job = a;
            
            //Determine the pecent complete based on the number of batches complete
            if(totalItems == 0){
                //A little check here as we don't want to divide by 0.
                j.percentComplete = 0;
            }else{
                j.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
            }
            
            batchJobs.add(j);    
        }
        return batchJobs;
    }
  
    //This is the wrapper class the includes the job itself and a value for the percent complete    
    public Class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
        public String getEstimatedCompletion(){
            if(job.JobItemsProcessed == 0 ) return null;
            Integer itemsRemaining = job.TotalJobItems - job.JobItemsProcessed;
            
            Long runningTime = DateTime.now().getTime() - job.CreatedDate.getTime();
            Double timePerBatch = runningTime / Double.valueOf(job.JobItemsProcessed);
            Long remainingTime = (Long)(itemsRemaining * timePerBatch);
            
            return DateTime.newInstance(DateTime.now().getTime() + remainingTime).format();
        }
    }
    
    public PageReference abort(){
        Id abortId = ApexPages.currentPage().getParameters().get('abortid');
        system.abortJob(abortId);
        return null;
    }
    
    /*--------------------TEST METHOD------------------------*/

    
}