/*
@Name            : TestC62Utilities
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for C62Utilities
*/


@IsTest
public class TestC62Utilities {
    @IsTest
    public static void test01(){
        Date d1 = date.newinstance(2014,07,16);
        Date d2 = date.newinstance(2014,08,22);
        Date d = C62Utilities.GetNextDate2(d1,d2);
    }
    
    @IsTest
    public static void test02(){
        Date d1 = date.newinstance(2014,07,16);
        Date d2 = date.newinstance(2014,08,17);
        Date d = C62Utilities.GetNextDate2(d1,d2);
    }
    
    @IsTest
    public static void test03(){
        Date d1 = date.newinstance(2014,07,11);
        Date d2 = date.newinstance(2014,08,6);
        Date d = C62Utilities.GetNextDate1(d1,d2);
    }
    
    @IsTest (seeAllData=true)
    public static void test04(){
        List<Contact> conSet = [SELECT Id FROM Contact LIMIT 5];
        List<Bill62__Invoice__c> invSet = [SELECT Id FROM Bill62__Invoice__c LIMIT 5];
        Map<Id,Id> idMap = new Map<Id,Id>();
        for (Contact con : conSet){
            for (Bill62__Invoice__c inv : invSet){
                idMap.put(con.Id,inv.Id);
            }
        }
        map<Id, List<Bill62__Dues__c>> theMap = C62Utilities.GetDuesForMappedInvoiceContacts(idMap);
    }
    
    @isTest
    public static void testIntegers(){
        Test.startTest();
        C62Utilities.testIntegers();
        C62Utilities.testIntegers2();
        C62Utilities.testIntegers3();
        C62Utilities.testIntegers4();
        C62Utilities.testIntegers5();
        C62Utilities.testIntegers6();
        C62Utilities.testIntegers7();
        C62Utilities.testIntegers8();
        C62Utilities.testIntegers9();
        Test.stopTest();
    }
    
}