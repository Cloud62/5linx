/*
@Name            : UtilityBatch
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Utility batch class for mass manipulation of data.  Repurposed to fill any requirement
*/


global class UtilityBatch implements Database.Batchable<sObject>,Schedulable,Database.Stateful,Database.AllowsCallouts {
    global String what;
    global Boolean chain;


    global UtilityBatch(String YEAH, Boolean OKAY) {
        what = YEAH;
        chain = OKAY;
        
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query;
        if(what == 'Rename Orders'){
            query = 'SELECT Id, Name, Carrier__r.Name, Bill62__Customer__r.Name, Bill62__Customer__r.Business_Name__c FROM Bill62__Order__c WHERE Name LIKE \'a0W%\' OR Name LIKE \'System.Linq%\'';
        }else if(what == 'Rename Addy'){
            query = 'SELECT Id, Name, Bill62__Street_1__c FROM Bill62__Address__c WHERE Name LIKE \'a07%\'';
        }else if(what == 'Reset Orders'){
            query = 'SELECT Id, Has_Current_Payment__c FROM Bill62__Order__c WHERE Has_Current_Payment__c = true';
        }else if(what == 'OrderLine Status'){
            query = 'SELECT ID, Status_ID__c, Status__c FROM Bill62__Subscription__c';
        }else if(what == 'Rename Reps'){
            query = 'SELECT ID, Status_ID__c, Status__c, RIN__c, First_Name__c, Last_Name__c, Name FROM Account WHERE Name LIKE \'____-__-__%\'';
        } else if(what == 'Email Status'){
            query = 'SELECT ID, email_status__c FROM Account WHERE email_status__c LIKE \'_\'';
        } else if(what == 'VOLogin'){
            query = 'SELECT ID, RIN__c, Login_GUID__c FROM Account WHERE Login_GUID__c = null OR Login_GUID__c = \'Rep GUID not found\'';
        } else if(what == 'Rename Note'){
            query = 'SELECT ID, Name FROM Rep_Notes__c WHERE Name LIKE \'a15%\'';
        } else if(what == 'Dummy Account'){
            query = 'SELECT ID, AccountID FROM Contact WHERE AccountID = null';
        } else if(what == 'Push Addy'){
        	query = 'SELECT ID,Bill62__Street_1__c,Bill62__Street_2__c,Bill62__City__c,State__r.State_ID__c,'+
			'Bill62__State_Province__c,Bill62__Zip_Postal_Code__c FROM Bill62__Address__c WHERE Address_ID__c = null';
        } else if(what == 'Pay Method'){
        	query = 'SELECT ID, Is_Direct_Deposit__c, Pay_Method__c FROM Account';
        }
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext ctx) {
        UtilityBatch batch1 = new UtilityBatch('string', false);
        ID batchprocessid = Database.executeBatch(batch1,1000);
    }


    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(what == 'Rename Orders'){
            List<Bill62__Order__c> oList = (List<Bill62__Order__c>) scope;
            Map<Id, String> OLmap = New Map<Id, String>();
            
            for (Bill62__Subscription__c ol : [SELECT Bill62__Product__r.Name, Order__c 
                                               FROM Bill62__Subscription__c 
                                               WHERE Order__c IN :oList]){
                if (!OLMap.containsKey(ol.Order__c)){
                    OLMap.put(ol.Order__c, ol.Bill62__Product__r.Name);
                }
            }
            
            String newName; 
            for (Bill62__Order__c o : oList){
                
                if(o.Bill62__Customer__r.Business_Name__c != null){
                    if(o.Bill62__Customer__r.Business_Name__c.Length() > 23){
                        newName = o.Bill62__Customer__r.Business_Name__c.Substring(0,23) + ' - ';
                    } else {
                        newName = o.Bill62__Customer__r.Business_Name__c + ' - ';
                    }
                } else if (o.Bill62__Customer__r.Name != null){
                    if(o.Bill62__Customer__r.Name.Length() > 23){
                        newName = o.Bill62__Customer__r.Name.Substring(0,23) + ' - ';
                    } else {
                        newName = o.Bill62__Customer__r.Name + ' - ';
                    }
                } else {
                    newName = ' - ';
                }
                
                if(o.Carrier__r.Name != null){
                    if (o.Carrier__r.Name.Length() > 23){
                        newName += o.Carrier__r.Name.Substring(0,23) + ' - ';
                    } else {
                        newName += o.Carrier__r.Name + ' - ';
                    }
                } else {
                    newName += ' - ';
                }
                
                if(OLMap.get(o.Id) != null){    
                    if (OLMap.get(o.Id).Length() > 23){
                        newName += OLMap.get(o.Id).Substring(0,23);
                    } else {
                        newName += OLMap.get(o.Id);
                    }
                }
                
            o.Name = newName;
            }
            update oList;
            
        } else if(what == 'Rename Addy'){
            List<Bill62__Address__c> oList = (List<Bill62__Address__c>) scope;
            for (Bill62__Address__c okay : oList){
                if (okay.Bill62__Street_1__c != null){
                    if (okay.Bill62__Street_1__c.length() > 79){
                        okay.Name = okay.Bill62__Street_1__c.substring(0,79);
                    } else {
                        okay.Name = okay.Bill62__Street_1__c;
                    }
                } else {
                    okay.Name = okay.Id;
                }
            }
            update oList;
            
        } else if(what == 'Reset Orders'){
            List<Bill62__Order__c> oList = (List<Bill62__Order__c>) scope;
            for (Bill62__Order__c okay : oList){
                okay.Has_Current_Payment__c = false;
            }
            update oList;
            
        } else if(what == 'OrderLine Status'){
            for(Bill62__Subscription__c ol : (List<Bill62__Subscription__c>) scope){
                if (ol.Status__c == null && ol.Status_ID__c != null){
                    if (ol.Status_ID__c == '-6') ol.Status__c = 'Pending Payment Verification';
                    if (ol.Status_ID__c == '-5') ol.Status__c = 'Pending Payment Approval';
                    if (ol.Status_ID__c == '-4') ol.Status__c = 'VERIFIED';
                    if (ol.Status_ID__c == '-3') ol.Status__c = 'VERIFYING';
                    if (ol.Status_ID__c == '-2') ol.Status__c = 'RECORDED';
                    if (ol.Status_ID__c == '-1') ol.Status__c = 'SUBMITTED';
                    if (ol.Status_ID__c == '0') ol.Status__c = 'PENDING';
                    if (ol.Status_ID__c == '10') ol.Status__c = 'ACTIVE';
                    if (ol.Status_ID__c == '20') ol.Status__c = 'REWORK';
                    if (ol.Status_ID__c == '40') ol.Status__c = 'RESUBMIT';
                    if (ol.Status_ID__c == '50') ol.Status__c = 'SHIPPED';
                    if (ol.Status_ID__c == '60') ol.Status__c = 'CLOSED';
                    if (ol.Status_ID__c == '70') ol.Status__c = 'SUSPENDED';
                    if (ol.Status_ID__c == '80') ol.Status__c = 'PROVISIONING';
                    if (ol.Status_ID__c == '90') ol.Status__c = 'BACKORDERED';
                    if (ol.Status_ID__c == '91') ol.Status__c = 'UNDELIVERABLE';
                    if (ol.Status_ID__c == '95') ol.Status__c = 'RMA HOLD';
                    if (ol.Status_ID__c == '96') ol.Status__c = 'RMA TESTED GOOD';
                    if (ol.Status_ID__c == '99') ol.Status__c = 'PURGED';
                    if (ol.Status_ID__c == '100') ol.Status__c = 'FAST PURGE';
                    if (ol.Status_ID__c == '101') ol.Status__c = 'MANUAL PURGE';
                    if (ol.Status_ID__c == '103') ol.Status__c = 'CHARGEBACK PURGE';
                }
                if (ol.Status_ID__c == null && ol.Status__c != null){
                    if (ol.Status__c == 'Pending Payment Verification') ol.Status_ID__c = '-6';
                    if (ol.Status__c == 'Pending Payment Approval') ol.Status_ID__c = '-5';
                    if (ol.Status__c == 'VERIFIED') ol.Status_ID__c = '-4';
                    if (ol.Status__c == 'VERIFYING') ol.Status_ID__c = '-3';
                    if (ol.Status__c == 'RECORDED') ol.Status_ID__c = '-2';
                    if (ol.Status__c == 'SUBMITTED') ol.Status_ID__c = '-1';
                    if (ol.Status__c == 'PENDING') ol.Status_ID__c = '0';
                    if (ol.Status__c == 'ACTIVE') ol.Status_ID__c = '10';
                    if (ol.Status__c == 'REWORK') ol.Status_ID__c = '20';
                    if (ol.Status__c == 'RESUBMIT') ol.Status_ID__c = '40';
                    if (ol.Status__c == 'SHIPPED') ol.Status_ID__c = '50';
                    if (ol.Status__c == 'CLOSED') ol.Status_ID__c = '60';
                    if (ol.Status__c == 'SUSPENDED') ol.Status_ID__c = '70';
                    if (ol.Status__c == 'PROVISIONING') ol.Status_ID__c = '80';
                    if (ol.Status__c == 'BACKORDERED') ol.Status_ID__c = '90';
                    if (ol.Status__c == 'UNDELIVERABLE') ol.Status_ID__c = '91';
                    if (ol.Status__c == 'RMA HOLD') ol.Status_ID__c = '95';
                    if (ol.Status__c == 'RMA TESTED GOOD') ol.Status_ID__c = '96';
                    if (ol.Status__c == 'PURGED') ol.Status_ID__c = '99';
                    if (ol.Status__c == 'FAST PURGE') ol.Status_ID__c = '100';
                    if (ol.Status__c == 'MANUAL PURGE') ol.Status_ID__c = '101';
                    if (ol.Status__c == 'CHARGEBACK PURGE') ol.Status_ID__c = '103';
                }
            }
            update (List<Bill62__Subscription__c>) scope;
            
        } else if(what == 'Rename Reps'){
        
            for(Account a: (List<Account>) scope){
            
                String newName;
                if(a.RIN__c != null){
                    newName = a.RIN__c+' - ';
                } else {
                    newName = '-';
                }
                
                if(a.First_Name__c != null){
                    if(a.First_Name__c.length() > 24){
                        newName += a.First_Name__c.subString(0,23)+' ';
                    }else{
                        newName += a.First_Name__c +' ';
                    }
                }
                
                if(a.Last_Name__c != null){
                    if(a.Last_Name__c.length() > 24){
                        newName += a.Last_Name__c.subString(0,23);
                    }else{
                        newName += a.Last_Name__c;
                    }
                }
                a.Name = newName;
                
                if (a.Status_ID__c != null){
                    if (a.Status_ID__c == '1') a.Status__c = 'Active';
                    if (a.Status_ID__c == '2') a.Status__c = 'Inactive';
                    if (a.Status_ID__c == '3') a.Status__c = 'Suspended';
                    if (a.Status_ID__c == '4') a.Status__c = 'Hold No Cab';
                    if (a.Status_ID__c == '5') a.Status__c = 'Newly Expired';
                    if (a.Status_ID__c == '6') a.Status__c = 'Expired';
                    if (a.Status_ID__c == '7') a.Status__c = 'Deactivated';
                    if (a.Status_ID__c == '8') a.Status__c = 'Cancelled';
                    if (a.Status_ID__c == '9') a.Status__c = 'Deleted';
                    if (a.Status_ID__c == '10') a.Status__c = 'Chargeback';
                    if (a.Status_ID__c == '11') a.Status__c = 'Hold Pay Cab';
                }
                if (a.Status__c != null){
                    if (a.Status__c == 'Active') a.Status_ID__c = '1';
                    if (a.Status__c == 'Inactive') a.Status_ID__c = '2';
                    if (a.Status__c == 'Suspended') a.Status_ID__c = '3';
                    if (a.Status__c == 'Hold No Cab') a.Status_ID__c = '4';
                    if (a.Status__c == 'Newly Expired') a.Status_ID__c = '5';
                    if (a.Status__c == 'Expired') a.Status_ID__c = '6';
                    if (a.Status__c == 'Deactivated') a.Status_ID__c = '7';
                    if (a.Status__c == 'Cancelled') a.Status_ID__c = '8';
                    if (a.Status__c == 'Deleted') a.Status_ID__c = '9';
                    if (a.Status__c == 'Chargeback') a.Status_ID__c = '10';
                    if (a.Status__c == 'Hold Pay Cab') a.Status_ID__c = '11';
                }
            }
            update (List<Account>) scope;
            
        } else if(what == 'Email Status'){
            for(Account LETSGO:(List<Account>) scope){
                if(LETSGO.email_status__c != null){
                    if (LETSGO.email_status__c == '0') LETSGO.email_status__c = 'Validated Email';
                    if (LETSGO.email_status__c == '1') LETSGO.email_status__c = 'Unvalidated Email (New)';
                    if (LETSGO.email_status__c == '2') LETSGO.email_status__c = 'Unvalidated Email (Mod)';
                    if (LETSGO.email_status__c == '3') LETSGO.email_status__c = 'Invalid Email (Bounced)';
                    if (LETSGO.email_status__c == '4') LETSGO.email_status__c = 'Missing Email';
                }
            }
            update (List<Account>) scope;
            
        } else if(what == 'VOLogin'){
            for(Account YEAH: (List<Account>) scope){
                if(YEAH.RIN__c != null) YEAH.Login_GUID__c = C625LinxAPIHelper.getVOLoginGUID(YEAH.RIN__c);
            }
            update (List<Account>) scope;
            
        } else if(what == 'Rename Note'){
            for(Rep_Notes__c YEAH: (List<Rep_Notes__c>) scope){
                YEAH.Name = 'Imported Note';
            }
            update (List<Rep_Notes__c>) scope;
            
        } else if(what == 'Dummy Account'){
            Account a = [SELECT ID FROM Account WHERE Name = 'Default Rep' LIMIT 1];
            for(Contact c: (List<Contact>) scope){
                c.AccountId = a.Id;
            }
            update (List<Contact>) scope;
            
        } else if(what == 'Push Addy'){
        	for(Bill62__Address__c a: (List<Bill62__Address__c>)scope){
        		APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
		         String xml_req = '';           
		                if (a.Bill62__Street_1__c != null){
		                    xml_req +=   '{"strAddress1":"'+a.Bill62__Street_1__c+'",';
		                } else {
		                    xml_req +=   '"strAddress1":null,';
		                }
		                if (a.Bill62__Street_2__c != null){
		                    xml_req +=  '"strAddress2":"'+a.Bill62__Street_2__c+'",';
		                } else {
		                    xml_req +=  '"strAddress2":null,';
		                }
		                if (a.Bill62__City__c != null){
		                    xml_req +=  '"strCity":"'+a.Bill62__City__c+'",';
		                } else {
		                    xml_req +=  '"strCity":null';
		                }    
		                if (a.State__r.State_ID__c != null){
		                    xml_req +=  '"intStateID":"'+a.State__r.State_ID__c+'",';
		                } else {
		                    xml_req +=  '"intStateID":null,';
		                }
		                if (a.Bill62__State_Province__c != null){
		                    xml_req +=  '"strStateProvince":"'+a.Bill62__State_Province__c+'",';
		                } else {
		                    xml_req +=  '"strStateProvince":null,';
		                }   
		                if (a.Bill62__Zip_Postal_Code__c != null){
		                    xml_req +=  '"strZip":"'+a.Bill62__Zip_Postal_Code__c+'",'; 
		                } else {
		                    xml_req +=  '"strZip":null,';
		                }   
		                xml_req +=  '"intCountryID":"1"}';
		                      
		         String path = apiHelp.URL__c + 'CreateAddress';
		         Http h = new Http();
		         HttpRequest req = new HttpRequest();
		         req.setBody(xml_req);
		         req.setEndpoint(path);
		         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
		         req.setHeader('Content-Type','application/json');
		         req.setMethod('POST');
		         req.setTimeout(120000);
		         String result;
		         if(!Test.IsRunningTest()){
		             HttpResponse res = h.send(req);
		             system.debug('xml_req: '+xml_req);   
		             system.debug('THIS IS THE BODY!!' + res.getBody());
		             result = res.getBody(); 
		             if(result.contains('intAddressId')){
		             	a.Address_ID__c = result.substring(result.indexOf('intAddressId')+15, result.indexOf('intAddressId')+22);
		             }
		         }
        	}
        	update (List<Bill62__Address__c>) scope;
        } else if(what == 'Pay Method'){
        	for(Account a: (List<Account>) scope){
        		if(a.Is_Direct_Deposit__c) a.Pay_Method__c = 'ACH';
        		else a.Pay_Method__c = 'Pay Card';
        	}
        	update (List<Account>)scope;
        }
        
       
    }
    
    global void finish(Database.BatchableContext bc){
        if(chain){
            if(what == 'Rename Orders'){
                String cronId = System.ScheduleBatch(new UtilityBatch('OrderLine Status', true),'UtilityBatch OrderLine Status'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,1000);
            }else if(what == 'OrderLine Status'){
                String cronId = System.ScheduleBatch(new UtilityBatch('Rename Reps', true),'UtilityBatch Rename Reps'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,1000);
            }else if(what == 'Rename Reps'){
                String cronId = System.ScheduleBatch(new UtilityBatch('Rename Addresses', true),'UtilityBatch Rename Addresses'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,1000);
            } else if(what == 'Rename Addresses'){
                String cronId = System.ScheduleBatch(new UtilityBatch('VOLogin', true),'UtilityBatch VOLogin'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,100);
            } else if(what == 'VOLogin'){
                String cronId = System.ScheduleBatch(new UtilityBatch('Dummy Account', true),'UtilityBatch Dummy Account'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,1000);
            }
        }
    }
    
}