/*
@Name            : OrderLineLinksController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Order Line Links page
*/


public with sharing class OrderLineLinksController {
    public Bill62__Subscription__c thisOL {get;set;}
    
    public OrderLineLinksController(ApexPages.StandardController controller){
        thisOL = [SELECT Id, Order_Line_ID__c, Order_Number__c FROM Bill62__Subscription__c WHERE Id=:controller.getId()];
    }
}