/*
@Name            : TestCalculatePaymentDate
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for payment date utility class
*/


@isTest
public class TestCalculatePaymentDate{


    //setup utility methods
    public static Contact getCon(){
        Bill62__Address__c a = new Bill62__Address__c(Address_ID__c = '12345');
        insert a;
        Contact c = new Contact();  
            c.LastName = 'Weatherby';          
            c.Bill62__Last_Name__c = 'Weatherby';
            c.Bill62__First_Name__c = 'Percy';
            c.Middle_Initial__c = 'M';
            c.Bill62__Birthdate__c = system.today();
            c.Bill62__SSN__c = '111111111';
            c.Bill62__Home_Phone__c = '1111111111';
            c.Email = 'pweatherby@mom.org';
            c.Address__c = a.id;
        insert c;
        return c;
    } 
    
    public static Bill62__Subscription__c getSub(){
        Bill62__Subscription__c s = new Bill62__Subscription__c();
        s.Bill62__Customer__c = getCon().Id;
        s.Bill62__Product__c = getProd().Id;
        s.Order__c = getOrder().Id;
        s.Bill62__Start_Date__c = system.date.today();
        insert s;
        return s;
    }
    
    public static Product2 getProd(){
        Product2 p = new Product2();
        p.Name = 'test Prod';
        Product_Category__c pc = new Product_Category__c();
        pc.Name = 'test pc';
        insert PC;
        p.Product_Category__c = pc.Id;
        Product2 p2 = new Product2();
        p2.Name = 'Changer';
        insert p2;
        p.Changes_To__c = p2.Id;
        insert p;
        Payment_Reason__c pr = new Payment_Reason__c();
        pr.Months__c = 12;
        pr.Product__c = p.Id;
        insert pr;
        return p;
    }
    
    public static Bill62__Order__c getOrder(){
        Bill62__Order__c o = new Bill62__Order__c();
        o.Bill62__End_Date__c = system.date.today().addDays(-15);
        o.Name = 'test Order';
        insert o;
        return o;
    }
    
    @isTest
    public static void testPaymentDateCalculation(){
        C62Utilities.cSetExecuted.add('paymentBatch');
        Bill62__Subscription__c s = getSub();
        Bill62__Payment__c p = new Bill62__Payment__c();
        Product_Category__c pc = [SELECT Id, Name FROM Product_Category__c LIMIT 1];
        Bill62__Order__c o = [Select ID, Bill62__End_Date__c FROM Bill62__Order__c LIMIT 1];
        s = [SELECT Order__r.Bill62__End_Date__c, Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Name FROM Bill62__Subscription__c LIMIT 1];
        test.startTest();
        
        //just throw this here;
        FiveLinxCalculatePaymentDate asdf = new FiveLinxCalculatePaymentDate();
        
        s.Bill62__Product__r.Product_Category__r.Name = 'BES Renew Schedule';
        for(Integer i = 0; i<30;i++){
            system.debug('1');
            s.Order__r.Bill62__End_Date__c = system.date.TODAY().addDays(i-15);
            Date testDate = FiveLinxCalculatePaymentDate.calculatePaymentDate(p,s,0);
        }
        s.Bill62__Product__r.Product_Category__r.Name = '5LINX Velocity ERP';
        for(Integer i = 0; i<30;i++){
            system.debug('2');
            s.Order__r.Bill62__End_Date__c = system.date.TODAY().addDays(i-15);
            Date testDate = FiveLinxCalculatePaymentDate.calculatePaymentDate(p,s,0);
        }
        s.Bill62__Product__r.Product_Category__r.Name = 'Wellness Reship';
        for(Integer i = 0; i<30;i++){
            system.debug('3');
            s.Order__r.Bill62__End_Date__c = system.date.TODAY().addDays(i-15);
            Date testDate = FiveLinxCalculatePaymentDate.calculatePaymentDate(p,s,0);
        }
        s.Bill62__Product__r.Product_Category__r.Name = 'Membership Reactivations';
        for(Integer i = 0; i<30;i++){
            system.debug('4');
            s.Order__r.Bill62__End_Date__c = system.date.TODAY().addDays(i-15);
            Date testDate = FiveLinxCalculatePaymentDate.calculatePaymentDate(p,s,0);
        }
        s.Bill62__Product__r.Product_Category__r.Name = 'Renew Energy Certification';
        for(Integer i = 0; i<30;i++){
            system.debug('5');
            s.Order__r.Bill62__End_Date__c = system.date.TODAY().addDays(i-15);
            Date testDate = FiveLinxCalculatePaymentDate.calculatePaymentDate(p,s,0);
        }
        s.Bill62__Product__r.Product_Category__r.Name = 'Email';
        for(Integer i = 0; i<30;i++){
            system.debug('6');
            s.Order__r.Bill62__End_Date__c = system.date.TODAY().addDays(i-15);
            Date testDate = FiveLinxCalculatePaymentDate.calculatePaymentDate(p,s,0);
        }
        
        test.stopTest();
    }
    
    @isTest
    public static void testMutation(){
        C62Utilities.cSetExecuted.add('paymentBatch');
        Bill62__Subscription__c s = getSub();
        s = [SELECT ID, Bill62__Product__r.Changes_To__c FROM Bill62__Subscription__c LIMIT 1];
        List<Bill62__Subscription__c> sl = new List<Bill62__Subscription__C>();
        sl.add(s);
        Test.startTest();
        sl = FiveLinxCalculatePaymentDate.mutateProducts(sl);
        Test.stopTest();
    }
    
    @isTest
    public static void testEndDateMovement(){
        C62Utilities.cSetExecuted.add('paymentBatch');
        Bill62__Subscription__c s = getSub();
        Bill62__Order__c o = [SELECT ID, Bill62__End_Date__c FROM Bill62__Order__c LIMIT 1];
        List<Bill62__Order__c> ol = new List<Bill62__Order__c>();
        ol.add(o);
        Test.StartTest();
        ol = FiveLinxCalculatePaymentDate.advanceEndDate(ol);
        ol = FiveLinxCalculatePaymentDate.rollbackEndDate(ol);
        Test.stopTest();
    }
}