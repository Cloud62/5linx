/*
@Name            : CancelCardController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Cancel Card page
*/


public with sharing class CancelCardController {
    public Account theRep {get;set;}
    public String errorMsg {get;set;}
    public String winMsg {get;set;}
    
    public CancelCardController(ApexPages.StandardController controller){
        theRep = [SELECT Id, Entity_ID__c FROM Account WHERE Id=:controller.getId()];
        errorMsg = null;
        winMsg = null;
    }
    
    public PageReference submit(){
         
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"intEntityId":"'+theRep.Entity_ID__c+'"}';     
                       
         String path = apiHelp.URL__c + 'CancelCard';
         String result;
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);
             System.debug('xml_req: '+xml_req);
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody();  
         } else {
             result = 'Cancelled current paycard';
         }
         if(result.contains('Cancelled current paycard')){
            winMsg = 'Card Cancelled Successfully.';
            errorMsg = null;
            C62Utilities.addAgentNote('Cancelled Pay Card', theRep.Id, 'Successfully cancelled pay card');
         } else {
            errorMsg = 'Card not cancelled';
            C62Utilities.addAgentNote('Failed Canceling Pay Card', theRep.Id, 'Attempted to cancel pay card, but was rejected by TC.');
            winMsg = null;
         }
         
         return null;
         
    }
    
    public PageReference cancel(){
         return new PageReference('/'+theRep.Id);
    }
}