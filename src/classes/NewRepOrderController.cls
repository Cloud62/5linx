/*
@Name            : NewRepOrderController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for New Rep Order page.  Takes Payments and creates Orders
*/



public class NewRepOrderController {
    public static Account orderRep {get;set;}
    public String repID {get;set;}
    //public static String productID {get;set;}
    public String payReasonID {get;set;}
    //public String myProductID {get;set;}
    public String page {get;set;}
    public List<SelectOption> expMonth{get;set;}
    public List<SelectOption> expYear{get;set;}
    public String selectedExpMonth{get;set;}
    public String selectedExpYear{get;set;}
    public Bill62__Payment_Method__c cPayMethod{get;set;}
    public Bill62__Address__c cBillAddr {get;set;}
    public Boolean existingAddress {get;set;}
    public Boolean existingPayMeth {get;set;}
    public Boolean isOther {get;set;}
    public Decimal paymentAmount {get;set;}
    public Decimal taxAmt {get;set;}
    public Decimal shipAmt {get;set;}
    public Bill62__Order__c o {get;set;}
    public Bill62__Subscription__c ol {get;set;}
    public Bill62__Payment__c newPay {get;set;}
    public String customerID {get;set;}
    public String repFirst {get;set;}
    public String repLast {get;set;}
    public boolean saved {get;set;}
    public boolean complete {get;set;}
    public List<SelectOption> payReasons{get;set;}
    public Map<String, Payment_Reason__c> paymentReasonMap{get;set;}
    public List<SelectOption> payMethOptions{get;set;}
    public Map<String, Bill62__Payment_Method__c> payMethMap{get;set;}
    public String payMethId{get;set;}
    public Contact theCustomer{get;set;}
    public User currentUser{get;set;}
    public String entityId{get;set;}
    public boolean error{get;set;}
    public boolean paymentMade{get;set;}
    
    public NewRepOrderController(ApexPages.StandardController controller){
        currentUser = [SELECT ID, TC_User_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
    
        //initialize booleans
        error = false;
        page = '1';
        saved = false;
        complete=false;
        existingAddress = false;
        existingPayMeth = false;
        paymentMade = false;
        
        orderRep = [SELECT Id, RIN__c, Entity_ID__c, Customer_ID__c, Customer_ID__r.Customer_ID__c, Name, First_Name__C, Last_Name__c, 
            Address_Id__r.Id FROM Account WHERE ID = :controller.getId()];
        entityId = orderRep.Entity_ID__c;
        system.debug('entityID: '+entityId);
        customerId = orderRep.Customer_ID__c;
        List<Contact> cList = [SELECT Customer_ID__c, FirstName, LastName, HomePhone
            FROM Contact WHERE Id = :orderRep.Customer_ID__c];
        if(cList.size() > 0){
            theCustomer = cList[0];
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Customer found for this Rep!  Please set the Customer field on the Rep.'));
            error = true;
            return;
        }
        
        system.debug(customerID);
        repFirst = orderRep.First_Name__c;
        repLast = orderRep.Last_Name__c;
        repID = orderRep.Id;
        paymentAmount = 0.1;

        //find existing payment methods so you don't make a new one every time
        List<Bill62__Payment_Method__c> payMethList = [SELECT ID, Bill62__Card_Type__c,Bill62__Card_Number__c,
         Bill62__CVV_Code__c, Bill62__Expiration_Date__c, Bill62__Billing_Address__r.Bill62__Street_1__c,
         Bill62__Billing_Address__r.Bill62__Street_2__c, Bill62__Billing_Address__r.Bill62__Street_3__c, 
         Bill62__Billing_Address__r.Bill62__City__c, Bill62__Billing_Address__r.Bill62__State_Province__c, 
         Bill62__Billing_Address__r.Bill62__Zip_Postal_Code__c, Bill62__Billing_Address__r.Bill62__Country__c,
         Bill62__Payment_Type__c, Name, Default__c, Payment_Method_ID__c, Bill62__Card_Holder_Name__c, Bill62__Billing_Address__c
         FROM Bill62__Payment_Method__c WHERE Bill62__Customer__c = :customerId];// AND Default__c = TRUE LIMIT 1];
         system.debug('paymethlists size: '+payMethList.size());
         
        payMethOptions = new List<SelectOption>();
        payMethMap = new Map<String, Bill62__Payment_Method__c>();
        if(payMethList.size() > 0){
            for(Bill62__Payment_Method__c pm: payMethList){
                payMethOptions.add(new Selectoption(pm.Id, pm.Name));
                payMethMap.put(pm.Id, pm);
                if(pm.Default__c) {cPayMethod = pm;payMethID = pm.Id;}
            }
            if(cPayMethod == null){ //catch in case there are Payment Methods but none are marked as Default
                for(Bill62__Payment_Method__c pm: payMethList){
                    cPayMethod = pm;payMethID = pm.Id;
                }
            }
            existingPayMeth = true;
        }else{
            cPayMethod = new Bill62__Payment_Method__c();
            cPayMethod.Bill62__Account__c = orderRep.Id;
            existingPayMeth = false;
        }
        if(existingPayMeth) saved = true;
        
        system.debug('existingPayMeth: '+existingPayMeth);
        system.debug('saved: '+saved);
        
        if(existingPayMeth){
            if(cPayMethod.Bill62__Billing_Address__c != null){
                cBillAddr = [SELECT ID, Bill62__Street_1__c, Bill62__Street_2__c, Bill62__Street_3__c,
                        Bill62__City__c, Bill62__State_Province__c, Bill62__Zip_Postal_Code__c, Bill62__Country__c,
                        Country__r.Country_ID__c FROM Bill62__Address__c WHERE ID = :cPayMethod.Bill62__Billing_Address__c];
            }else{
                if(orderRep.Address_Id__r.Id != null){
                    cBillAddr = [SELECT ID, Bill62__Street_1__c, Bill62__Street_2__c, Bill62__Street_3__c,
                        Bill62__City__c, Bill62__State_Province__c, Bill62__Zip_Postal_Code__c, Bill62__Country__c,
                        Country__r.Country_ID__c FROM Bill62__Address__c WHERE ID = :orderRep.Address_Id__r.Id];
                    cPayMethod.Bill62__Billing_Address__c = cBillAddr.Id;
                    existingAddress = true;
                }else{
                    cBillAddr = new Bill62__Address__c();
                    existingAddress = false;
                }
            }
        } else {
            cBillAddr = new Bill62__Address__c();
            existingAddress = false;
        }
        
        expMonth = new List<SelectOption>();
        expYear = new List<SelectOption>();
        for(Integer i=1;i<=12;i++){
            String iString;
            if(i<10){
                iString='0'+i;
            }else{
                iString=i+'';
            }
            expMonth.add(new SelectOption(iString,iString));
        }
        for(Integer i=Date.Today().year();i<Date.Today().year()+10;i++){
            expYear.add(new SelectOption(i+'',i+''));
        }
        selectedExpYear=Date.Today().year()+'';
        selectedExpMonth=Date.Today().month()+'';
        
        //get payment reasons
        String userId = userInfo.getUserId();
        List<User> userlist = [SELECT TC_User_ID__c FROM User WHERE Id = :userId];
        String TCUserId;
        if(userlist.size() > 0) TCUserId = userlist[0].TC_User_ID__c;
        APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
        String xml_req =   '{"intCustomerId":"'+theCustomer.Customer_ID__c+'",';
                if(cBillAddr.Country__r.Country_ID__c != null){
                    xml_req +=  '"intCountryId":"'+cBillAddr.Country__r.Country_ID__c+'",';
                } else {
                    xml_req +=  '"intCountryId":"1",';
                }
                xml_req +=  '"intProductsID":null,';   
                xml_req +=  '"intUserId":"411",';//'+TCUserId+'",';
                xml_req +=  '"intOrderId":null}';        
         system.debug('getpaymentReason request: '+xml_req);
         String path = apiHelp.URL__c + 'GetPaymentReasonList';
         String result;
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         HttpResponse res = h.send(req);
         system.debug('THIS IS THE BODY!!' + res.getBody());
         
        //now to parse it
        List<paymentReason> jsonList = new List<paymentReason>();
        system.debug('jsonlists size:'+jsonList.size());
        Map<String,Object> myMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        for(String key : myMap.keyset()){
            system.debug(key+':'+mymap.get(key));
            if(key.isNumeric()){
                Map<String, Object> something = (Map<String, Object>)mymap.get(key);
                paymentReason payR = new paymentReason();
                payR.intPaymentReasonId = (Integer)something.get('intPaymentReasonId');
                payR.strPaymentReasonDs = (String)something.get('strPaymentReasonDs');
                jsonList.add(payR);
                system.debug('jsonish object:'+payR);
            }
        }

        Set<String> payIDSet = new Set<String>();
        for(paymentReason r: jsonList){
            payIDSet.add(String.valueOf(r.intPaymentReasonId));
        }
        List<Payment_Reason__c> payReasonList = [SELECT Id, Product__r.External_ID__c, Amount__c, Months__c, Name, Currency_ISO_Code__c, 
            External_ID__c, Product__r.Bill62__Payment_Gateway__c, Product__r.Name, Product__r.Product_Type__c
            FROM Payment_Reason__c WHERE External_ID__c IN :payIDSet];
            
        payReasons = new List<SelectOption>();
        paymentReasonMap = new Map<String, Payment_Reason__c>();
        for(Payment_Reason__c pr: payReasonList){
            if(pr.Product__r.External_ID__c != null){
                if(pr.Name != null){
                    payReasons.add(new SelectOption(pr.Id, pr.Name));
                } else {
                    payReasons.add(new SelectOption(pr.Id, 'No name for Payment Reason record!'));
                }
                paymentReasonMap.put(pr.Id, pr);
            }
        }
        
        if(payReasons.size() == 0){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Payment Reasons found! Please check the status of the Rep to ensure Payments are possible.'));
        }
    }
    
    public pageReference changePayMeth(){
        system.debug('changing payMeth: '+payMethMap.get(payMethID));
        cPayMethod = payMethMap.get(payMethID);
        return null;
    }
    
    public pageReference back(){
        page = '1';
        return null;
    }
    
    public pageReference toRep(){
        return new PageReference('/'+repId);
    }
    
    public pageReference toOrder(){
        return new PageReference('/'+o.Id);
    }
    
    public pageReference makeNew(){
        existingPayMeth = false;
        saved = false;
        cPayMethod = new Bill62__Payment_Method__c();
        cPayMethod.Name = repFirst+' '+repLast+'-Default';
        return null;
    }
    
    public pageReference editExisting(){
        existingPayMeth = false;
        saved = false;
        return null;
    }
    
    public PageReference createOrder(){
        // get the Rep first
        List<Account> repList = [SELECT Id, Name, Phone, Entity_ID__c, Customer_ID__c, Customer_ID__r.Name  FROM Account WHERE Id = :repID LIMIT 1];
        if(repList.size() > 0){
            orderRep = repList[0];
        } else {
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Could not find Rep! Please contact your support Organization.') );
            return null;
        }
        //myProductID = productId;
        Payment_Reason__c theR;
        if(paymentReasonMap.containsKey(payReasonID)){
            theR = paymentReasonMap.get(payReasonID);
        } else {
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Could not find selected Payment Reason record! Please contact your support Organization.') );
            return null;
        }
        
        // retrieve the product and product type selected
        List<Product2> prodList = [SELECT Id, External_ID__c, Carrier__c, Name, Carrier__r.Carrier_ID__c, Carrier__r.Name, Amount__c 
            FROM Product2 WHERE External_Id__c = :theR.Product__r.External_ID__c];
        Product2 orderProduct;
        if(prodList.size() > 0){
            orderProduct = prodList[0];
        } else {
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Could not find the Product for the selected Payment Reason! Please contact your support Organization.') );
            return null;
        } 
        List<Product_Type__c> ptList = [SELECT Id, External_ID__c FROM Product_Type__c WHERE ID = :theR.Product__r.Product_Type__c];
        Product_Type__c orderPT;
        if(ptList.size() > 0) orderPT = ptList[0];
        
        system.debug('product id: >'+orderProduct.External_ID__c+'<');
        isOther = false;
        /*if(productID == '1001' || productID == '1005' || productID == '1000' || productID == '1021'){
            isOther = true;
        } else */ 
        if (theR.Amount__c != null){
            paymentAmount = theR.Amount__c;
            if(paymentAmount == 0) isOther = true;
        } else {
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'No Amount found on Payment Reason record!') );
            return null;
        }
        system.debug('isOther: '+isOther);
        // create a new order
        o = new Bill62__Order__c();
        o.Name = orderRep.Customer_ID__r.Name + ' - ' + orderProduct.Carrier__r.Name + ' - ' + orderProduct.Name;
        o.Bill62__Account__c = orderRep.Id;
        o.Order_Date__c = system.today();
        o.Carrier__c = orderProduct.Carrier__c;
        o.Bill62__Customer__c = orderRep.Customer_ID__c;
        if(orderPT != null) o.Product_Type__c = orderPT.Id;
        o.Payment_Method__c = cPayMethod.Id;
        
        // create a new order line
        ol = new Bill62__Subscription__c();
        ol.Bill62__Customer__c = orderRep.Customer_ID__c;
        ol.Bill62__Start_Date__c = system.today();
        ol.Bill62__Monthly_Billing_Start_Day__c = string.valueof(system.today()).substring(8);
        ol.Bill62__Product__c = orderProduct.Id;
        

        // call CalcTaxes to get tax
        Decimal tax = CalcTaxesNOW(theCustomer.Customer_ID__c,theR.Product__r.External_ID__c);
        if(tax != null) paymentAmount += tax;
        
        
        /*C62Utilities.cSetExecuted.add('dontFireTrigger');
        insert o;
        ol.Order__c = o.Id;
        insert ol;*/
            
        //return new PageReference('/'+ o.Id);
        page = '2';
        return null;
    }


    // calls CalcTaxes to get Taxes
    public static Decimal CalcTaxesNOW(String customerId, String theProductId){

         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"customerId":"'+customerId+'",'+
                            '"productId":"'+theProductId+'"}';        
                       
         String path = apiHelp.URL__c + 'CalcTaxes';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         HttpResponse res = h.send(req);
         String tax = res.getBody();
         Decimal taxAmt = 0.0;
         
         if (tax.contains('"Success"')){
            tax = tax.substring(tax.indexOf('TaxAmount') + 12, tax.indexOf('TaxAmount') + 17);
            if (tax.contains(',')){
                tax = tax.substring(0, tax.indexof(','));
                system.debug(tax);
            }
            if(tax == '0'){
                system.debug('Tax: '+tax);
                taxAmt = 0;
            } else {
                system.debug('Tax: '+tax);
                taxAmt = decimal.valueof(tax);
            }
        }
        return taxAmt;
    }
    
    /* calls CalcShipping to get Shipping Amount
    //You cannot call calcShipping without an orderID, so this is useless
    public static String CalcShippingNOW(Integer intOrderId){

         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"orderId":"'+intOrderId+'"}';        
                       
         String path = apiHelp.URL__c + 'CalcShipping';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         HttpResponse res = h.send(req);
         return res.getBody();
    }*/
    
    public pageReference savePayMeth(){
        if(!existingAddress){
            try{
                upsert cBillAddr;
           }catch(Exception e){
               ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage() ) ); 
           }
        }
        if(!existingPayMeth){
            try{
                cPayMethod.Bill62__Billing_Address__c = cBillAddr.Id;
                cPayMethod.Bill62__Customer__c = customerID;
                cPayMethod.Bill62__Expiration_Date__c = system.Date.NewInstance(integer.valueof(selectedExpYear), integer.valueof(selectedExpMonth),1);
                cPayMethod.Default__c = true;
                cPayMethod.Bill62__Payment_Type__c = 'Credit Card';
                system.debug(cPayMethod.Bill62__Expiration_Date__c);
                upsert cPayMethod;   
                saved = true; 
                payMethOptions.add(new Selectoption(cPayMethod.Id, cPayMethod.Name));
                payMethMap.put(cPayMethod.Id, cPayMethod);
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, 'Card Saved!' ) );
            }catch(Exception e){
               ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage() ) ); 
            }
        } else {
            try{
                cPayMethod.Bill62__Billing_Address__c = cBillAddr.Id;
                cPayMethod.Bill62__Expiration_Date__c = system.Date.NewInstance(integer.valueof(selectedExpYear), integer.valueof(selectedExpMonth),1);
                upsert cPayMethod;
                saved = true;
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, 'Card Saved!' ) );
            } catch(Exception e){
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage() ) );
            }
        }
        existingPayMeth = true;
        return null;
    }
    
    public pageReference makePayment(){
        Bill62.PaymentUtilities utils = new Bill62.PaymentUtilities();

        
        if(newPay == null){
            newPay = new Bill62__Payment__c();
            // paymentAmount = paymentAmount + taxAmt + shipAmt;
            system.debug('paymentAmount: ' +paymentAmount );
            newPay.Bill62__Amount__c = paymentAmount;
            newPay.Bill62__Payment_Date__c = DateTime.NOW().date();
            if(paymentReasonMap.containsKey(payReasonId)){
                system.debug('got a PR in makePayment');
                newPay.Bill62__Currency_Code__c = paymentReasonMap.get(payReasonId).Currency_ISO_Code__c;
                newPay.Bill62__Description__c = paymentReasonMap.get(payReasonID).Product__r.Name;
                //Use the dynamic Payment Gateway, depending
                if(paymentReasonMap.get(payReasonId).Product__r.Bill62__Payment_Gateway__c != null){
                    newPay.Bill62__Payment_Gateway__c = 
                        paymentReasonMap.get(payReasonId).Product__r.Bill62__Payment_Gateway__c;
                }
            } else {
                newPay.Bill62__Currency_Code__c = 'USD';
            }
            system.debug(customerID);
            newPay.Bill62__Customer__c = customerID;
            newPay.Bill62__Status__c = 'Queued';
            newPay.Bill62__Payment_Method__c = cPayMethod.Id;
            //newPay.Bill62__Order__c = o.Id;
        }
        try{
            Bill62.C62PaymentGatewayClasses.MakePaymentResponse result = utils.MakePayment2(cPayMethod, newPay, repFirst, repLast);
            system.debug('gateway response object: '+result);
            if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Payment Charged Successfully!'));
                paymentMade = true;
                newPay.Bill62__Status__c='Paid';
                newPay.Bill62__Amount_Used__c = newPay.Bill62__Amount__c;
                newPay.Bill62__Transaction_Id__c = result.TransactionID;
                newPay.Bill62__Approval_Code__c = result.AuthorizationID;
                newPay.Bill62__Last_Authorization_Attempt__c=DateTime.NOW();
                if(newPay.Bill62__Number_Of_Authorization_Attempts__c == null){
                    newPay.Bill62__Number_Of_Authorization_Attempts__c = 1;
                }else{
                    newPay.Bill62__Number_Of_Authorization_Attempts__c++;
                }
                try{
                	newPay = postPaymentProcess(newPay);
                } catch(Exception e){
                	newPay.TC_Integration_Retry__c = system.dateTime.NOW();
                }
                if(newPay.TC_Integration_Retry__c != null){
                    try{
                        Bill62.C62PaymentGatewayClasses.VoidAuthorizationResponse refundResult = utils.voidAuthorization(newPay);
                        if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                            newPay.Bill62__Status__c = 'Refunded';
                            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Error in Post Payment Process. No Order ID assigned and Payment refunded.' ) );
                        } else {
                            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Error in Post Payment Process. No Order ID assigned and Payment refund failed.' ) );
                        }
                    } catch(Exception e){
                        newPay.Bill62__Status__c = 'Exception';
                        newPay.Bill62__Processing_Message__c = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                        String error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                    }
                }/* else {
                    try{
                        Bill62.C62PaymentGatewayClasses.CaptureAuthorizationResponse captureResult = utils.CaptureAuthorization(newPay);
                        if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                            //excellent!  it worked.
                        } else {
                            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Payment Authorization succeeded, but Capture failed.  Please contact your supervisor.' ) );
                        }
                    } catch(Exception e){
                        ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Payment Authorization succeeded, but Capture failed.  Please contact your supervisor.' ) );
                    }
                }*/
                system.debug('final Transaction ID: '+newPay.Bill62__Transaction_ID__c);
                system.debug('final authcode: '+newPay.Bill62__Approval_Code__c);
                newPay.Bill62__Order__c = o.Id;
                newPay.Bill62__Subscription__c = ol.Id; //TODO:set this to the new Order Line
                upsert newPay;
            } else {
                newPay.Bill62__Status__c='Rejected';
                newPay.Bill62__Last_Authorization_Attempt__c=DateTime.NOW();
                if(newPay.Bill62__Number_Of_Authorization_Attempts__c == null){
                    newPay.Bill62__Number_Of_Authorization_Attempts__c = 1;
                }else{
                    newPay.Bill62__Number_Of_Authorization_Attempts__c++;
                }
                
                if (result.ErrorDescription != null){
                    ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, result.ErrorDescription ) );
                } else {            
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'Problem Charging Payment'));
                } 
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }
        return null;
    }
    
    public Bill62__Payment__c postPaymentProcess(Bill62__Payment__c p){
        
        Integer intPayMethodId;
        String FName, LName, cardNum, expDate, routingNum, bankNum;
        intPayMethodId = findPayMethodId(cPayMethod.Bill62__Card_Type__c);
        if(theCustomer.FirstName != null) FName = theCustomer.FirstName;
        if(theCustomer.LastName != null) LName = theCustomer.LastName;
        cardNum = cPayMethod.Bill62__Card_Number__c.replace('-','');
        String expDateTemp = string.valueof(cPayMethod.Bill62__Expiration_Date__c);
        if(expDateTemp != null){
             expDate = expDateTemp.substring(5,7) + expDateTemp.substring(2,4);
        }else{
            expDate = null;
        }
        
        Decimal pmtAmount = p.Bill62__Amount__c;
        String AuthCode = p.Bill62__Approval_Code__c;
        String refId = p.Bill62__Transaction_Id__c;
        // Rep info
        String RIN = p.Bill62__Order__r.Bill62__Account__r.RIN__c;
        Integer entityID2;
        if(entityId != null){
            entityId2 = integer.valueof(entityId);
        }else{
            entityId2 = 1;
        }
        // Customer and address info
        String address = cBillAddr.Bill62__Street_1__c;
        String city = cBillAddr.Bill62__City__c; 
        String state = cBillAddr.Bill62__State_Province__c;
        String zipCode = cBillAddr.Bill62__Zip_Postal_Code__c;
        String phoneNum = theCustomer.HomePhone;
        Integer countryId;
        if(cBillAddr.Country__r.Country_ID__c != null){
            countryId = integer.valueof(cBillAddr.Country__r.Country_ID__c);
        }else{
            countryId = 1;
        }
        Integer custId = integer.valueof(theCustomer.Customer_ID__c);
        // eventually an order will be tied to payment
        String orderId;
        if ( o.Order_IDtxt__c != null ){
            orderId = o.Order_IDtxt__c;
        } else {
            orderId = null;
        }
        Integer userId;
        if ( currentUser.TC_User_ID__c != null ){
            userId = integer.valueof(currentUser.TC_User_ID__c);
        } else {
            userId = 1;
        }
        Integer payReason;
        if(paymentReasonMap.get(payReasonId).External_ID__c != null) 
            payReason = Integer.valueOf(paymentReasonMap.get(payReasonId).External_ID__c);
        if(!Test.isRunningTest()){
            p = C625LinxAPIHelper.postPaymentProcessImmediate(entityID2,orderID,payReason,intPayMethodId,bankNum,routingNum,cardNum,expDate,
                FName,LName,address,city,state,zipCode,countryId,countryId,pmtAmount,phoneNum,refID,AuthCode,p.id, p);
        }   
        if(p.Bill62__Processing_Message__c != null){
            system.debug('Processing Message not null:');
            system.debug(p.Bill62__Processing_Message__c);
            //ghetto parser to get the Order and Order Item ID
            if(p.Bill62__Processing_Message__c.contains('intOrderId')){
                o.Order_ID__c = integer.valueOf(p.Bill62__Processing_Message__c.substring(p.Bill62__Processing_Message__c.indexOf('intOrderId') + 13, p.Bill62__Processing_Message__c.indexOf('intOrderId') + 20 )); 
                system.debug('order ID returned: '+o.Order_ID__c);
                ol.Order_Line_ID__c = p.Bill62__Processing_Message__c.substring(p.Bill62__Processing_Message__c.indexOf('intOrderItemId') + 17, p.Bill62__Processing_Message__c.indexOf('intOrderItemId') + 25);
                system.debug('Order Item ID returned: '+ol.Order_Line_ID__c);
                C62Utilities.cSetExecuted.add('dontFireTrigger');
                try{
                    insert o;
                    ol.Order__c = o.Id;
                    insert ol;
                } catch(Exception e){
                    if(e.getMessage().contains('duplicate value found')){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Duplicate Order ID found.  Data will be updated to reflect payment within 30 minutes.'));
                        insert ol;
                    }else{
                        ApexPages.addMessages(e);
                    }
                }
                complete=true;
            } else {
                system.debug('Problem with postpaymentprocess: no contains');
            }
        } else {
            system.debug('Problem with postpaymentprocess: null');
        }    
        p.Bill62__Processing_Message__c = null;
        return p;
    }
    
    public class paymentReason{
        public Integer intPaymentReasonId{get;set;}
        public String strPaymentReasonDs {get;set;}
        
        public paymentReason(){}
    }
    
    public Integer findPayMethodID(String s){
        Integer i;
        if(s == 'Visa'){
            i = 1;
        }else if(s == 'MasterCard'){
            i = 2;
        }else if(s == 'American Express'){
            i = 3;
        }else if(s == 'Discover'){
            i = 4;
        }
        return i;
    }
}