/*
@Name            : FiveLinxGeneratePayments
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Generates Payments for today, to be processed by the process Payments batch
*/


global class FiveLinxGeneratePayments implements Database.Batchable<sObject>,Schedulable,Database.Stateful,Database.AllowsCallouts {
    global String error {get;set;}

    global FiveLinxGeneratePayments() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        error = '';
        String query = 
            'SELECT Id, Bill62__Customer__c, Order__r.Bill62__End_Date__c, Order__c, ' +
            'Order__r.Has_Current_Payment__c, Bill62__Product__c, Order__r.Bill62__Account__r.status_id__c,' +
            'Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Bill62__Payment_Gateway__c, '+
            'Bill62__Product__r.Name, Bill62__Product__r.Changes_To__c '+
            'FROM Bill62__Subscription__c WHERE  Order__r.Bill62__End_Date__c != null ' +
            'AND Status_Id__c IN (\'10\', \'70\') AND (Order__r.Has_Current_Payment__c = false ' +
            'OR Order__r.Has_Current_Payment__c = null) AND Bill62__Product__r.Bill62__Auto_Renew__c = TRUE ' +
            //exclude Wellness Orders
            'AND Bill62__Product__r.Product_Category__r.Name NOT IN (\'Wellness Reship\', \'Coffee Recurring\', '+
            ' \'Nutrition\', \'Nutrition Recurring\', \'Premium Bundle\')';
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext ctx) {
        FiveLinxGeneratePayments batch1 = new FiveLinxGeneratePayments();
        ID batchprocessid = Database.executeBatch(batch1,50);
    }


    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            C62Utilities.cSetExecuted.add('paymentBatch'); //don't fire triggers
            List<Bill62__Subscription__c> firstsubList = (List<Bill62__Subscription__c>)scope;
            
            Set<ID> subIds2 = new Set<ID>();
            for(Bill62__Subscription__c s:firstsubList){
                subIds2.add(s.Id);
            }
            
            List<Bill62__Subscription__c> secondSublist = [SELECT Id, Bill62__Customer__c, Order__r.Bill62__End_Date__c, Order__c,
                Order__r.Has_Current_Payment__c, Bill62__Product__c, Order__r.Bill62__Account__r.status_id__c,
                Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Name, Bill62__Product__r.Changes_To__c,
                Bill62__Product__r.Bill62__Payment_Gateway__c
                FROM Bill62__Subscription__c WHERE ID IN :subIds2];
            
            //First, mutate products that mutate
            secondSublist = FiveLinxCalculatePaymentDate.mutateProducts(secondSublist);
            update secondSubList;
            
            Set<ID> subIds = new Set<ID>();
            for(Bill62__Subscription__c s:secondSubList){
                subIds.add(s.Id);
            }
            
            List<Bill62__Subscription__c> sublist = [SELECT Id, Bill62__Customer__c, Order__r.Bill62__End_Date__c, Order__c,
                Order__r.Has_Current_Payment__c, Bill62__Product__c, Order__r.Bill62__Account__r.status_id__c,
                Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Name, Bill62__Product__r.Changes_To__c,
                Bill62__Product__r.Bill62__Payment_Gateway__c, Order__r.Payment_Method__c
                FROM Bill62__Subscription__c WHERE ID IN :subIds];
            
            List<Bill62__Payment__c> insertPayList = new List<Bill62__Payment__c>();
            List<Bill62__Order__c> updateOrders = new List<Bill62__Order__c>();
            
            //this block: need to get the payment reason so we can set the Amount
            Set<String> prodIds = new Set<String>();
            Set<String> orderIds = new Set<String>();
            Set<String> updateOrdersSet = new Set<String>();
            for(Bill62__Subscription__c sub: subList){
                if(sub.Bill62__Product__c != null) prodIds.add(sub.Bill62__Product__c);
                if(sub.Order__c != null) orderIds.add(sub.Order__c);
            }
            Map<Id, Bill62__Order__c> orderMap = new Map<Id, Bill62__Order__c>([SELECT Id, 
                Has_Current_Payment__c FROM Bill62__Order__c WHERE ID IN :orderIds]);
            List<Payment_Reason__c> payReasonList = [SELECT ID, Amount__c, Months__c, Product__c, Currency_ISO_Code__c FROM 
                Payment_Reason__c WHERE Product__c IN :prodIds];
                
            Map<String, Payment_Reason__c> productToPayReasonMap = new Map<String, Payment_Reason__c>();
            for(Payment_Reason__c pr: payReasonList){
                if(pr.Product__c != null) productToPayReasonMap.put(pr.Product__c, pr);
            }
            
            Map<String, Product2> productMap = new Map<String, Product2>([SELECT Id, Product_Category__r.Name, Name FROM Product2 WHERE ID IN :prodIds]);
            
            Set<String> goodStatuses = new Set<String>{'1', '2', '4', '5', '6', '11'};
            Set<String> memberships = new Set<String>{'Membership Reactivations', 'Monthly Membership',
                'Platinum Services', 'Standard Services', 'No Services'};
                        
            for(Bill62__Subscription__c sl : subList){
                if(sl.Order__r.Has_Current_Payment__c) continue;
                
                    //this block: only create payments if the Order's Product has a Payment Reason
                    Payment_Reason__c payReason;
                    if(sl.Bill62__Product__c != null){
                        if(productToPayReasonMap.containsKey(sl.Bill62__Product__c)){
                            payReason = productToPayReasonMap.get(sl.Bill62__Product__c);
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                    //error = sl.ID+' '+sl.Bill62__Product__r.Product_Category__r.Name; //for testing ONLY!
                    
                    //don't do yearly memberships
                    if(productMap.get(sl.Bill62__Product__c).Product_Category__r.Name == 'Yearly Membership') continue;
                    
                    //don't mess up if there's no product category
                    if(productMap.get(sl.Bill62__Product__c).Product_Category__r.Name == null) continue;
                    
                    //this block: check the Rep's status for all Services Orders
                    if(memberships.contains(productMap.get(sl.Bill62__Product__c).Product_Category__r.Name)){
                        if(!goodStatuses.contains(sl.Order__r.Bill62__Account__r.status_id__c)) continue;
                    }
                    
                    
                    //make the actual Payment object
                    Bill62__Payment__c newPay = new Bill62__Payment__c();
                    if(sl.Bill62__Product__r.Bill62__Payment_Gateway__c != null)
                        newPay.Bill62__Payment_Gateway__c = sl.Bill62__Product__r.Bill62__Payment_Gateway__c;
                    
                    //remove after testing:
                    newPay.Fake_Payment__c = true;
                    
                    newPay.Bill62__Amount__c = payReason.Amount__c;
                    newPay.Bill62__Subscription__c = sl.Id;
                    newPay.Bill62__Customer__c = sl.Bill62__Customer__c;
                    newPay.Bill62__Status__c = 'Queued';
                    newPay.Bill62__Order__c = sl.Order__c;
                    newPay.Bill62__Currency_Code__c = payReason.Currency_ISO_Code__c;
                    newPay.Bill62__Payment_Method__c = sl.Order__r.Payment_Method__c;
                    newPay.Bill62__Payment_Date__c = FiveLinxCalculatePaymentDate.calculatePaymentDate(newPay, sl, 0);
                    if(newPay.Bill62__Payment_Date__c == System.Date.today()) {
                        insertPayList.add(newPay);
                        Bill62__Order__c theOrder = orderMap.get(sl.Order__c);
                        theOrder.Has_Current_Payment__c = true;
                        updateOrdersSet.add(theOrder.Id);
                    }
                
                }
                for(String s: updateOrdersSet){
                    updateOrders.add(orderMap.get(s));
                }
                insert insertPayList;
                update updateOrders;
            } catch(Exception e) {
                error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {'rich@cloud62.com'};
                mail.setToAddresses(toAddresses);
                mail.setSubject('GeneratePayments Exception!');
                mail.setPlainTextBody(error);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    

    global void finish(Database.BatchableContext BC) {
        /*if(error != null){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'rich@cloud62.com'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('GeneratePayments Exception!');
            mail.setPlainTextBody(error);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }*/
        String cronId = System.ScheduleBatch(new FiveLinxGenerateWellnessPayments(),'FiveLinxGenerateWellnessPayments'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,200);
    }

}