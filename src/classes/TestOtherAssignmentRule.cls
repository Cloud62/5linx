/*
@Name            : TestOtherAssignmentRule
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for case assignment
*/


@IsTest
public with sharing class TestOtherAssignmentRule {
    @IsTest
    public static void test01(){
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.GetUserId() LIMIT 1];
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Satellite' LIMIT 1];
        Case c = New Case(RecordTypeID=rt.Id, OwnerId = u.ID,AssignmentRule__c = true);
        insert c;
    }
}