/*
@Name            : StatusBoldController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for inline Status page on Account
*/


public class StatusBoldController {

public String color{get;set;}
public string txtColor{get;set;}
public String status{get;set;}
public String statusTxt {get;set;}
public Account a {get;set;}
public String last4 {get;set;}
public String DOB {get;set;}
public String last4Tax{get;set;}

    public StatusBoldController(ApexPages.StandardController controller) {
        Id accountId = controller.getRecord().Id;
        a = [SELECT Status_ID__c, Name, Business_Name__c,Birth_Date__c,Home_Phone_Country__c,Home_Phone__c, SSN__c,
            EMail__c, RIN__c, Mobile_Phone_Country__c, Mobile_Phone__c, Tax_ID__c FROM Account WHERE ID = :accountID LIMIT 1];
            
        status = 'no value'; //default in case of null
        last4='no value'; //default in case of null
        DOB = 'no value'; //default in case of null
        last4Tax = 'no value'; //default in case of null
        
        if(a.SSN__c != null && a.SSN__c.length() > 4){
             last4='XXX-XX-'+a.SSN__c.substring(5);
        } else if (a.SSN__c != null && a.SSN__c.length() == 4){
             last4='XXX-XX-'+a.SSN__c;
        }
        
        if(a.Tax_ID__c != null){
            last4Tax = '';
            if(a.Tax_ID__c.length() > 3){
                Integer numbers = a.Tax_ID__c.length();
                for(Integer i = 0; i<numbers-4; i++){
                    last4Tax += 'X';
                }
                last4Tax += a.Tax_ID__c.RIGHT(4);
            } else {
                last4Tax = a.Tax_ID__c;
            }
        }
        
        if(a.Status_ID__c != null) status = a.Status_ID__c;
        if(a.Birth_Date__c != null) DOB = a.Birth_Date__c.format();
        if(DOB.contains('M')) DOB = DOB.substring(0, DOB.indexof(' '));
        
        color = 'black'; //default value;
        txtColor = 'white'; //default value
        
        //set the right words and right colors
        if(status == '1'){
            status = 'Active';
            color = 'LimeGreen';
            txtColor = 'black';
        } else if(status == '2'){
            status = 'Inactive';
            color = 'saddleBrown';
        }else if(status == '3'){
            status = 'Suspended';
            color = 'red';
        }else if(status == '4'){
            status = 'Hold No Cab';
            color = 'gold';
            txtColor = 'black';
        }else if(status == '5'){
            status = 'Newly Expired';
            color = 'LightGray';
            txtColor = 'black';
        }else if(status == '6'){
            status = 'Expired';
            color = 'DimGray';
            txtColor = 'black';
        }else if(status == '7'){
            status = 'Deactivated';
            color = 'maroon';
        }else if(status == '8'){
            status = 'Cancelled';
            color = 'darkorange';
        }else if(status == '9'){
            status = 'Deleted';
            color = 'blue';
        }else if(status == '10'){
            status = 'Chargeback';
            color = 'red';
        }else if(status == '11'){
            status = 'Hold Pay Cab';
            color = 'gold';
        }
    }

}