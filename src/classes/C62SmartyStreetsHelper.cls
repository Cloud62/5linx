global class C62SmartyStreetsHelper {
    
    
    public static Boolean validateAddress(String street1, String street2, String city, String state, String zip){
        Smarty_Streets__c creds = Smarty_Streets__c.getOrgDefaults();
        if (creds == null){
            system.debug(LoggingLevel.ERROR,'***MISSING SMARTY STREET CREDS');
            return false;
        }
        String authID = creds.ID__c;
        String authToken = creds.Token__c;
        System.debug('***creds: ' + authID + ' --- ' + authToken);
        
        String thisBody = '[{';
        if (street1 != null){
            thisBody += '"street":"' + street1 + '",';
        }
        if (street2 != null){
            thisBody += '"street2":"' + street2 + '",';
        }
        if (city != null){
            thisBody += '"city":"' + city + '",';
        }
        if (state != null){
            thisBody += '"state":"' + state + '",';
        }
        if (zip != null){
            thisBody += '"zipcode":"' + zip + '",';
        }
        thisBody = thisBody.substring(0, thisBody.length() - 1);
        thisBody += '}]';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        system.debug('value of body before 3000000:--------------------------> '+thisBody);
        if(thisBody.length()>3000000){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Exceeded max size limit of 3000000. Please try to attach a small attachment.'));
            //return null;
        }
        req.setMethod('POST');
        req.setEndpoint('https://api.smartystreets.com/street-address?auth-id=' + authID + '&auth-token=' + authToken);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(thisBody);
        String response;

        if (!System.Test.isRunningTest()){
            HttpResponse res = h.send(req);
            system.debug(res.getStatusCode());
            system.debug(res.getBody());
            system.debug(thisBody);
            response = res.getBody();   
        }
        if (response != '[]'){
            return true;
        } else {
            return false;
        }
    }

}