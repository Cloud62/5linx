/*
@Name            : RequalifyController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Rep Requalify page
*/


public with sharing class RequalifyController {
    public Account theRep {get;set;}
    public String errorMsg {get;set;}
    public String winMsg {get;set;}
    
    public RequalifyController (ApexPages.StandardController controller){
        theRep = [SELECT Id, Entity_ID__c FROM Account WHERE Id=:controller.getId()];
        errorMsg = null;
        winMsg = null;
    }
    
    public PageReference submit(){
         
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"intEntityId":"'+theRep.Entity_ID__c+'"}';     
                       
         String path = apiHelp.URL__c + 'requalifyRep';
         String result;
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);
             System.debug('xml_req: '+xml_req);
             system.debug('THIS IS THE BODY!!' + res.getBody());
             try{
                 result = res.getBody();  
             } catch(System.CalloutException e) {
                 result = res.getBody();  
             }
         } else {
             result = 'Rep requalified successfully';
         }
         if(result.contains('Rep requalified successfully')){
            winMsg = 'Rep Requalified Successfully.';
            errorMsg = null;
            C62Utilities.addAgentNote('Rep Requalified', theRep.Id, 'Successfully Requalified Rep');
         } else {
            errorMsg = 'Rep not requalified.';
            C62Utilities.addAgentNote('Failed Requalified Rep', theRep.Id, 'Attempted to Requalify Rep, but was rejected by TC.');
            winMsg = null;
         }
         
         return null;
         
    }
    
    public PageReference cancel(){
         return new PageReference('/'+theRep.Id);
    }
}