/*
@Name            : ConsoleListenerController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Who knows.
*/


global class ConsoleListenerController {
    
    global ConsoleListenerController(){}
    
    @RemoteAction
    global static String doCheck(){
        return 'beep beep';
    }
    
}