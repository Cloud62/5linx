/*
@Name            : CABProofController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for CAB Proof page
*/


public with sharing class CABProofController {
    public Account theRep {get;set;}
    public Boolean showLink {get;set;}
    public String linkPrefix {get;set;}
    
    public CABProofController(ApexPages.StandardController controller){
        showLink=false;
        theRep = [SELECT Id, CAB_Date__c, Entity_ID__c FROM Account WHERE Id=:controller.getId() LIMIT 1];
        if( theRep.CAB_Date__c != null ){
            showLink=true;
        }
        if(!Test.isRunningTest()) linkPrefix = TC_URL__c.getValues('Prefix').URL__c;
    }
    
    public PageReference CABChargeBack(){
         
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"intEntityId":"'+theRep.Entity_ID__c+'"}';     
                       
         String path = apiHelp.URL__c + 'CABChargeBack';
         String result;
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);
             System.debug('xml_req: '+xml_req);
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody();  
         }
         return new PageReference('/'+theRep.Id);
    }
}