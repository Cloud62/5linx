@istest
private class oppNoteTriggers_TestMethod // Name of the test class, Can be any name
{         
    static testMethod void oppNoteTriggers_TestMethod()   // Name of the test method. Must be the same as the class name 
    {
      // Perform our data preparation.
      Opportunity[] pID = [select id from Opportunity where Name='Opp Test'];
      List<Note> notes = new List<Note>{};
      Note note = new Note(Title = 'Unit Test Note for Opportunity', Body = 'Unit Test Note for Opportunity', ParentId = pID[0].ID); 
      notes.add(note);
        
      // Start the test, this changes governor limit context to
      // that of trigger rather than test.
      test.startTest();
           
      // Insert the note that causes the trigger to execute.
      insert notes;                        // Call Function of Original class
      
      // Stop the test, this changes limit context back to test from trigger.
      test.stopTest();
      
    }    
}