/*
@Name            : RecentlyViewedRepsController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Recently Viewed Reps page
*/


public class RecentlyViewedRepsController {
    public List<RecentlyViewed> accList {get;set;}
    public RecentlyViewedRepsController(){
        accList = [SELECT Id, Name FROM RecentlyViewed 
                   WHERE LastViewedDate !=null AND Type = 'Account' 
                   ORDER BY LastViewedDate DESC LIMIT 10];    
    }
}