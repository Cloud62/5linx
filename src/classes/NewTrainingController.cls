public class NewTrainingController {
    public University_Training__c theTraining{get;set;}

    public NewTrainingController(ApexPages.StandardController controller) {
        theTraining = new University_Training__c();
        Map<String, String> params = ApexPages.currentPage().getParameters();
        if(params.containsKey('Rep')){
            theTraining.Rep__c = params.get('Rep');
        }
    }
    
    public pageReference doIt(){
        if(theTraining.Rep__c == null || theTraining.Position__c == null){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Rep and Position are required!'));
            return null;
        }
        //String result = C625LinxAPIHelper.AddRepPositionTraining('1190891','6');
        //TODO: finish this.  gotta get external ids to pass along, make request, parse it, add message
        //Map<String, Object> myMap = (Map<string, object>)JSON.deserializeUntyped(result);
        return null;
    }

}