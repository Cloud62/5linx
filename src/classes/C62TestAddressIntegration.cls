/*
@Name            : C62TestAddressIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for Address Integration
*/


@IsTest
public class C62TestAddressIntegration {
    
    @IsTest
    public static void test01(){
        State__c s = new State__c(State_ID__c = 32, Name = 'New York');
        Country__C c = new Country__c(Country_ID__c = 1, Name = 'United States');
        insert s;
        insert c;
        Bill62__Address__c a = new Bill62__Address__c();
                a.Address_ID__c = '1234';
                a.Bill62__Street_1__c = '1576 Sweet Home Rd.';
                a.Bill62__Street_2__c = 'Suite 209';
                a.Bill62__City__c = 'Testlandia'; 
                a.Bill62__State_Province__c = 'New York';
                a.State__c = s.Id;
                a.Country__c = c.Id;
                a.Bill62__Zip_Postal_Code__c = '14226';
        test.startTest();
        insert a;
        test.stopTest();
    }
    
    @IsTest
    public static void test02(){
        State__c s = new State__c(State_ID__c = 32, Name = 'New York');
        Country__C c = new Country__c(Country_ID__c = 1, Name = 'United States');
        insert s;
        insert c;
        Bill62__Address__c a = new Bill62__Address__c();
                a.Address_ID__c = '1234';
                a.Bill62__Street_1__c = '1576 Sweet Home Rd.';
                a.Bill62__Street_2__c = 'Suite 209';
                a.Bill62__City__c = 'Testlandia'; 
                a.Bill62__State_Province__c = 'New York';
                a.State__c = s.Id;
                a.Country__c = c.Id;
                a.Bill62__Zip_Postal_Code__c = '14226';
        insert a;
        Bill62__Address__c newAddy = [SELECT Id FROM Bill62__Address__c WHERE Id =:a.Id]; 
        newAddy.Bill62__City__c = 'Buffalo';
        test.startTest();
        update newAddy;
        system.assertEquals('Buffalo', newAddy.Bill62__City__c);
        test.stopTest();
    }
    
    @IsTest
    public static void test03(){
        State__c s = new State__c(State_ID__c = 32, Name = 'New York');
        Country__C c = new Country__c(Country_ID__c = 1, Name = 'United States');
        insert s;
        insert c;
        Bill62__Address__c a = new Bill62__Address__c();
                a.Address_ID__c = '1234';
                a.Bill62__Street_1__c = '1576 Sweet Home Rd.';
                a.Bill62__Street_2__c = 'Suite 209';
                a.Bill62__City__c = 'Testlandia'; 
                a.Bill62__State_Province__c = 'New York';
                a.State__c = s.Id;
                a.Country__c = c.Id;
                a.Bill62__Zip_Postal_Code__c = '14226';
        insert a;    
        Boolean b = C62SmartyStreetsHelper.validateAddress(a.Bill62__Street_1__c, a.Bill62__Street_2__c, a.Bill62__City__c, a.Bill62__State_Province__c,a.Bill62__Zip_Postal_Code__c); 
    }   
}