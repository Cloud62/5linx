/*
@Name            : FiveLinxBatchRescheduleBilling
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Batch to resechdule failed Payments to their next billing date
*/


global class FiveLinxBatchRescheduleBilling implements Database.Batchable<sObject>,Schedulable,Database.Stateful,Database.AllowsCallouts {
    global String error {get;set;}
    global List<String> logList;

    global FiveLinxBatchRescheduleBilling() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        error = '';
        logList = new List<String>();
        String query = 
            'SELECT ID, Bill62__Payment_Date__c, Bill62__Status__c, Bill62__Order__c, '+
                                                             'Bill62__Subscription__r.Order__r.Bill62__End_Date__c, '+
                                                             'Bill62__Subscription__r.Bill62__Product__r.Product_Category__r.Name, '+
                                                             'Bill62__Subscription__r.Bill62__Product__r.Name, '+
                                                             'Bill62__Order__r.NOP_ID__c '+
                                                      'FROM   Bill62__Payment__c '+
                                                      'WHERE Bill62__Payment_Date__c = TODAY AND '+
                                                      '(Bill62__Status__c = \'Rejected\' OR Bill62__Status__c LIKE \'exception\' OR Bill62__Status__c = \'Queued Retry\' OR Bill62__Status__c = \'Paid\')';
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext ctx) {
        FiveLinxBatchRescheduleBilling batch1 = new FiveLinxBatchRescheduleBilling();
        ID batchprocessid = Database.executeBatch(batch1,100);
    }


    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            C62Utilities.cSetExecuted.add('paymentBatch'); //don't fire triggers
            List<Bill62__Payment__c> payList = (List<Bill62__Payment__c>) scope;
            Set<ID> payIDs = new Set<ID>();
            Set<Id> orderIDs = new Set<ID>();
            Set<ID> orderLineIDs = new Set<ID>();
            Set<String> NOPIDs = new Set<String>();
            
            for(Bill62__Payment__c payInd : payList){
                if(payInd.Bill62__Status__c != 'Paid'){
                
                    //check whether we should give up:
                    String dateString;
                    if(payInd.Bill62__Subscription__r.Order__r.Bill62__End_Date__c != null){
                        dateString = String.valueof(payInd.Bill62__Subscription__r.Order__r.Bill62__End_Date__c);
                    } else {
                        continue;
                    }
                    Date endDate = Date.valueOf(dateString);
                    String fam = payInd.Bill62__Subscription__r.Bill62__Product__r.Product_Category__r.Name;
                    String prod = payInd.Bill62__Subscription__r.Bill62__Product__r.Name;
                    Date myToday = system.date.TODAY().addDays(1);  //tomorrow is the earliest we'd be rescheduling to
                    
                    if(fam.Equals('BES Renew Schedule') || fam.Equals('TextAlertz') || fam.Equals('Business Elite')){
                        if(myToday > endDate.addDays(15)){
                            payInd.Bill62__Status__c = 'Cancelled';
                            continue;
                        }
                    }else if(fam.Equals('5LINX Velocity ERP')){
                        if(myToday > endDate.addDays(15)){
                            payInd.Bill62__Status__c = 'Cancelled';
                            continue;
                        }
                    }else if(fam.Equals('Wellness Reship') || fam.Equals('Coffee Recurring') || fam.Equals('Nutrition')
                        || fam.Equals('Nutrition Recurring') || fam.Equals('Premium Bundle')){
                        if(myToday > endDate){
                            payInd.Bill62__Status__c = 'Cancelled';
                            continue;
                        }
                    }else if(fam.Equals('Membership Reactivations') || fam.Equals('Monthly Membership') 
                        || fam.Equals('Yearly Membership')){
                        if(myToday > endDate.addDays(60)){
                            payInd.Bill62__Status__c = 'Cancelled';
                            continue;
                        }
                    }else if(fam.Equals('Renew Energy Certification') || prod.Equals('Renewable Energy Certificate')){
                        if(myToday > endDate.addDays(15)){
                            payInd.Bill62__Status__c = 'Cancelled';
                            continue;
                        }
                    }else if(fam.Equals('Email') || fam.Equals('OptiMYz') || fam.Equals('VBC') 
                        || fam.Equals('Platinum Services') || fam.Equals('Standard Services')
                        || fam.Equals('No Services')){
                        if(myToday > endDate.addDays(15)){
                            payInd.Bill62__Status__c = 'Cancelled';
                            continue;
                        }
                    }
                    
                    //if we haven't given up yet, reset payment date
                    Bill62__Subscription__c sub = payInd.Bill62__Subscription__r;
                    payInd.Bill62__Payment_Date__c = FiveLinxCalculatePaymentDate.calculatePaymentDate(payInd, sub, 1);
                } else {
                    //if a Payment is successful, set up the Order to generate the next one tomorrow
                    if(payInd.Bill62__Order__r.NOP_ID__c == null){
                        orderIDs.add(payInd.Bill62__Order__c);
                        orderLineIDs.add(payInd.Bill62__Subscription__c);
                    } else {
                        NOPIDs.add(payInd.Bill62__Order__r.NOP_ID__c);
                    }
                }
            }
            
            List<Bill62__Order__c> nopOrders = [SELECT ID FROM Bill62__Order__c WHERE NOP_ID__c IN :NOPIDs];
            for(Bill62__Order__c ord : nopOrders){
                orderIDs.add(ord.Id);
            }
            
            List<Bill62__Order__c> orderList = [SELECT ID, Has_Current_Payment__c, Push_To_TC__c, Bill62__End_Date__c 
                FROM Bill62__Order__c WHERE ID IN :orderIDs];
            for(Bill62__Order__c ord: orderList){
                ord.Has_Current_Payment__c = false;
                //ord.Push_To_TC__c = true;
            }
            //orderList = FiveLinxCalculatePaymentDate.advanceEndDate(orderList); TC does this on its own apparently
            
            update payList;
            update orderList;
            
        } catch(Exception e) {
            error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
            logList.add(error);
        }
    }
    

    global void finish(Database.BatchableContext BC) {
        if(error != null){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'rich@cloud62.com'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Reschedule Billing Exception!');
            mail.setPlainTextBody(String.join(logList, '\r\n\r\n'));
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        //String cronId = System.ScheduleBatch(new FiveLinxGeneratePayments(),'FiveLinxGeneratePayments'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),1,500);
    }

}