/*
@Name            : C62TestCustomerIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class to test Customer Integration
*/


@IsTest
public class C62TestCustomerIntegration {
    @IsTest
    public static void test01(){
        State__c s = new State__c(State_ID__c = 32, Name = 'New York');
        Country__c co = new Country__c(Country_ID__c = 1, Name = 'United States');
        insert s;
        insert co;
        Bill62__Address__c a = new Bill62__Address__c(Address_ID__c = '123');
        insert a;
        Contact c = new Contact();  
            c.LastName = 'Weatherby';   
            c.FirstName = 'Weatherby';        
            c.Middle_Initial__c = 'M';
            c.Business_Name__c = 'MoM';
            c.Bill62__Birthdate__c = system.today().addyears(-25);
            c.Bill62__SSN__c = '465738485';
            c.Bill62__Home_Phone__c = '7168675309';
            c.Email = 'pweatherby@mom.org';
            c.Address__c = a.id;
            c.Customer_ID__c = '12345';
        test.starttest();
        insert c;
        c.Email = 'pweasley@mom.org';
        update c;
        test.stoptest();
    }
}