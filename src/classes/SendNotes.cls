/*
@Name            : SendNotes
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Utility class for Notes integration to 5Linx TC
*/


public with sharing class SendNotes {

    @future (callout=true)
    public static void sendNotes(List<String> Ids, List<String> Notes, List<Id> Sfids) {
        
        String noteRequest = SendNotes.buildNoteRequest(Ids, Notes, Sfids);
        
        // do request....
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://api.5linx.com/SalesForceNotesNotifications.aspx');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setBody(noteRequest);
        HttpResponse res = h.send(req);
        
    }
    
    public static String buildNoteRequest(List<String> Ids, List<String> Notes, List<Id> Sfids) {
        String noteRequest = 'NotesListener2=<?xml version="1.0" encoding="utf-16"?>';
        noteRequest += '<ArrayOfNotes xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        for(Integer x = 0; x < Ids.size(); x++) {
            noteRequest += '<notes>';
            noteRequest += '<leadID>' + Ids[x] + '</leadID>';
            noteRequest += '<note>' + Notes[x].Replace('&','and') + '</note>';         
            noteRequest += '<sfID>' + Sfids[x] + '</sfID>';                                 
            noteRequest += '</notes>';  
        }
        noteRequest += '</ArrayOfNotes>';
        system.debug(noteRequest);
        return noteRequest;
    }

}