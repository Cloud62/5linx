/*
@Name            : RepNotesController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Rep Notes page
*/


public with sharing class RepNotesController {
  public Account theRep {get;set;}
  
  public RepNotesController (ApexPages.StandardController controller){
    theRep = [SELECT Id, Entity_ID__c, Name FROM Account WHERE Id=:controller.getId()];
  }
  
  public ApexPages.StandardSetController setCon {
        get {
            
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [SELECT Name, Note_Date__c, Priority__c, Note__c, CreatedDate FROM Rep_Notes__c WHERE Rep_Name__c = :theRep.Id ORDER BY Priority__c DESC, Note_Date__c DESC]));
            }
            return setCon;
        }
        set;
   }
    
   public List<Rep_Notes__c> getList() {
         setCon.setpagesize(5);
         return (List<Rep_Notes__c>) setCon.getRecords();
   }
}