/*
@Name            : TCLinksController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for inline TC Links page
*/


public with sharing class TCLinksController {
    public Boolean showCELink {get;set;}
    public Boolean notCSR {get;set;}
    public Boolean isFinance {get;set;}
    public Boolean isFieldOps {get;set;}
    public TCLinksController(){
        Id pId = userinfo.getProfileId();
        Profile pr = [select Name from profile where id = :pId];

        if (pr.Name == 'Customer Experience Supervisor' || pr.Name == 'Customer Experience Manager' || 
            pr.Name == 'Finance Department' || pr.Name == 'System Administrator' || 
            pr.Name == 'Offline Team' || pr.Name == '5Linx Admin' || pr.Name == 'Manager Field Operations' ){
            showCELink = true;
        } else {
            showCELink = false;
        }
        if (pr.Name != '5Linx CSR' && pr.Name != 'Offline Team'){
            notCSR = true;
        }
        if (pr.Name == 'Finance Department' || pr.Name == '5Linx Admin' || pr.Name == 'System Administrator'){
            isFinance = true;
        }
        if (pr.Name == 'Finance Department' || pr.Name == '5Linx Admin' || pr.Name == 'Customer Experience Manager' ||
            pr.Name == 'Manager Field Operations' || pr.Name == 'System Administrator'){
            isFieldOps = true;
        }
    }
}