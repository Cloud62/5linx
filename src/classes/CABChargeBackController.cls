/*
@Name            : CABChargeBackController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for CAB Charge Back page
*/


public with sharing class CABChargeBackController {
    public Account thisRep {get;set;}
    public CABChargeBackController(ApexPages.StandardController controller){
        thisRep = [SELECT Id, Entity_ID__c FROM Account WHERE Id = :controller.getId() LIMIT 1];
    }
    
    public PageReference CABChargeBack(){
         
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"intEntityId":"'+thisRep.Entity_ID__c+'"}';     
                       
         String path = apiHelp.URL__c + 'CABChargeBack';
         String result;
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);
             System.debug('xml_req: '+xml_req);
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody();  
         }
         return new PageReference('/'+thisRep.Id);
    }
}