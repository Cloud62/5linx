/*
@Name            : VOPasswordResetController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for VO Password Reset page
*/


public with sharing class VOPasswordResetController {
    public Account theRep {get;set;}
    public VOPasswordResetController(ApexPages.StandardController controller){
        theRep = [SELECT Id, Entity_ID__c FROM Account WHERE Id=:controller.getId()];
    }
    
    public PageReference VOPasswordReset(){
         
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"intEntityId":"'+theRep.Entity_ID__c+'"}';     
                       
         String path = apiHelp.URL__c + 'VOPasswordReset';
         String result;
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);
             System.debug('xml_req: '+xml_req);
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody();  
         }
         //make a rep note
         C62Utilities.addAgentNote('VO Password Reset', theRep.Id, 'Reset VO Password');
         
         return new PageReference('/'+theRep.Id);
    }
}