/*
@Name            : TestControllers
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Test class for all VisualForce controllers
*/


@IsTest
public class TestControllers {

//setup utility methods
    public static Contact getCon(){
        Bill62__Address__c a = new Bill62__Address__c(Address_ID__c = '12345');
        insert a;
        Contact c = new Contact();  
            c.LastName = 'Weatherby';          
            c.Bill62__Last_Name__c = 'Weatherby';
            c.Bill62__First_Name__c = 'Percy';
            c.Middle_Initial__c = 'M';
            c.Bill62__Birthdate__c = system.today();
            c.Bill62__SSN__c = '111111111';
            c.Bill62__Home_Phone__c = '1111111111';
            c.Email = 'pweatherby@mom.org';
            c.Address__c = a.id;
            c.Customer_ID__c = '1';
        insert c;
        return c;
    } 
    
    public static Bill62__Subscription__c getSub(){
        Bill62__Subscription__c s = new Bill62__Subscription__c();
        s.Bill62__Customer__c = getCon().Id;
        s.Bill62__Product__c = getProd().Id;
        s.Order__c = getOrder().Id;
        s.Bill62__Start_Date__c = system.date.today();
        s.Status_Id__c = '10';
        insert s;
        return s;
    }
    
    public static Product2 getProd(){
        Product2 p = new Product2();
        p.Name = 'test Prod';
        Product_Category__c pc = new Product_Category__c();
        pc.Name = 'test pc';
        insert PC;
        p.Product_Category__c = pc.Id;
        p.Bill62__Auto_Renew__c = true;
        Product2 p2 = new Product2();
        p2.Name = 'Changer';
        p2.Product_Category__c = pc.Id;
        p2.Bill62__Auto_Renew__c = true;
        insert p2;
        p.Changes_To__c = p2.Id;
        insert p;
        Payment_Reason__c pr = new Payment_Reason__c();
        pr.Months__c = 12;
        pr.Product__c = p.Id;
        pr.External_ID__c = '1';
        Payment_Reason__c pr2 = new Payment_Reason__c();
        pr2.Months__c = 12;
        pr2.Product__c = p2.Id;
        pr2.External_ID__c = '2';
        insert pr;
        insert pr2;
        return p;
    }
    
    public static Bill62__Order__c getOrder(){
        Bill62__Order__c o = new Bill62__Order__c();
        o.Bill62__End_Date__c = system.date.today().addDays(-15);
        o.Name = 'test Order';
        o.Has_Current_Payment__c = false;
        o.NOP_ID__c = '12345';
        insert o;
        return o;
    }
    
    public static Bill62__Payment_Method__c getPayMeth(){
        Bill62__Payment_Method__c pm = new Bill62__Payment_Method__c();
        pm.Bill62__Card_Holder_Name__c = 'Tom Smith'; 
        pm.Bill62__Card_Number__c = '11111111111111';
        pm.Bill62__Expiration_Date__c = system.date.newInstance(2100, 10, 10);
        pm.Bill62__Routing_Number__c = '11111111111111';
        pm.Bill62__Account_Number__c = '11111111111111';
        pm.Bill62__Payment_Type__c ='Credit Card';
        pm.Bill62__Card_Type__c = 'Visa';
        insert pm;
        return pm;
    }
    
    public static Bill62__Payment_Gateway__c getPayGate(){
        Bill62__Payment_Gateway__c theGate = new Bill62__Payment_Gateway__c(Name='Authorize.net',Bill62__Default__c=TRUE,Bill62__Login__c='63kF4pjcgZ2Z',Bill62__Password__c='7Yk49MRq86wU29j9',
            Bill62__Production_URL__c='https://secure.authorize.net/gateway/transact.dll',
            Bill62__Test_Code_Response__c='1|1|1|This transaction has been approved.|LDV9JR|Y|2160295577||0|600.00|CC|'
            +'auth_capture||aslam|bari||dfsdf||wa|5555||||||||||||||||||71AFA2003D02930E47E84F23E14E07C3|P|2||'
            +'|||||||||XXXX1111|Visa|||||||||||||||||false|1',Bill62__Test_URL__c='https://test.authorize.net/gateway/transact.dll');
        theGate.Bill62__Default__c = true;
        insert theGate;
        return theGate;
    }
    
    public static Account getRep(){
        Account a = new Account();
        a.Entity_ID__c = '1234';
        a.Name = 'Snoop Dogg';
        a.CAB_Date__c = system.date.newInstance(2010, 10, 10);
        a.RIN__c = '1234';
        insert a;
        return a;
    }


    @isTest
    public static void testAddressIntegrationTrigger(){
        Bill62__Address__c a = new Bill62__Address__c();
        a.Address_ID__c = '1';
        a.Bill62__Street_1__c = '10236 Charing Cross Road';
        a.Bill62__City__c = 'Los Angeles';
        a.Bill62__State_Province__c = 'California';
        a.Bill62__Zip_Postal_Code__c = '90210';
        insert a;
        Test.StartTest();
        a.Address_ID__c = '2';
        update a;
        Test.StopTest();
    }

    @IsTest
    public static void C62AddPMController() {
        C62Utilities.cSetExecuted.add('dontFireTrigger');
       PageReference pageRef = Page.C62AddPM;
       Test.setCurrentPage(pageRef);
       
       /*Contact c = getCon(); 
       Date startDt = date.newInstance(2006,05,01);
       Bill62__Subscription__c s = new Bill62__Subscription__c();
           s.Bill62__Billing_Frequency__c = 'Monthly';
           s.Bill62__Monthly_Billing_Start_Day__c = '1';
           s.Bill62__Customer__c = c.Id;
           s.Bill62__Start_Date__c = startDt;
       insert s;
       Bill62__Payment_Method__c pm = new Bill62__Payment_Method__c(Bill62__Payment_Type__c ='Credit Card', Bill62__Card_Type__c = 'Visa', 
           Bill62__Card_Holder_Name__c = 'John Smith', Bill62__Customer__c = c.Id, Bill62__Card_Number__c='1111111111111111', 
           Bill62__Expiration_Date__c=system.Date.newInstance(2111, 10, 09));
       insert pm;*/
       Bill62__Subscription__c s = getSub();
       Bill62__Order__c o = [SELECT ID FROM Bill62__Order__c LIMIT 1];
       Contact c = [SELECT ID FROM Contact LIMIT 1];
       Bill62__Payment_Method__c pm = getPayMeth();
       pm.Bill62__Customer__c = c.Id;
       update pm;
       ApexPages.StandardController sc = new ApexPages.StandardController(s);
       C62AddPMController controller = new C62AddPMController(sc);       
       controller.thisSub.Bill62__Payment_Method__c = pm.id;
       
       Test.StartTest();
       controller.submit();
       controller.cancel();
       pm.Bill62__Card_Type__c = 'MasterCard';
       update pm;
       controller.submit();
       pm.Bill62__Card_Type__c = 'American Express';
       pm.Bill62__CVV_Code__c = '1234';
       update pm;
       controller.submit();
       pm.Bill62__Card_Type__c = 'Discover';
       update pm;
       controller.submit();
       pm.Bill62__Payment_Type__c = 'ACH/echeck';
       update pm;
       controller.submit();
       pm.Bill62__Payment_Type__c = '';
       update pm;
       controller.submit();
       controller.submit();
       controller.submit();
       Test.StopTest();
   }
   
   @IsTest
    public static void C62AddRepCertController() {
       C62Utilities.cSetExecuted.add('dontFireTrigger');
       PageReference pageRef = Page.C62AddRepCertification;
       Test.setCurrentPage(pageRef);
       /*Contact c = getCon(); 
       Date startDt = date.newInstance(2006,05,01);
       Bill62__Subscription__c s = getSub();*/
       Bill62__Subscription__c s = getSub();
       Bill62__Order__c o = [SELECT ID FROM Bill62__Order__c LIMIT 1];
       Contact c = [SELECT ID FROM Contact LIMIT 1];
       Bill62__Payment_Method__c pm = getPayMeth();
       pm.Bill62__Customer__c = c.Id;
       ApexPages.StandardController sc = new ApexPages.StandardController(o);
       C62AddRepCertController controller = new C62AddRepCertController(sc);   
           
       Test.StartTest();
       controller.AddCert();
       controller.cancel();
       Test.StopTest();
   }
   
   @IsTest
    public static void C62EditSSNController() {
        C62Utilities.cSetExecuted.add('dontFireTrigger');
       PageReference pageRef = Page.C62EditSSN;
       Test.setCurrentPage(pageRef);
       Account a = new Account(Name = 'Test');
       insert a;
       ApexPages.StandardController sc = new ApexPages.StandardController(a);
       C62EditSSNController controller = new C62EditSSNController(sc);   
           
       Test.StartTest();
       controller.newSSN = '123654987';
       controller.editSSN();
       controller.newSSN = '123456789';
       controller.editSSN();
       Test.StopTest();
   }
   
    public static void makeMaps() {
        C62Utilities.cSetExecuted.add('dontFireTrigger');
       Country__c theCountry = New Country__c(Name = 'United States', Country_ID__c = 1);
       insert theCountry;
       Country__c theOtherCountry = New Country__c(Name = 'Canada', Country_ID__c = 2);
       insert theOtherCountry;
       State__c theState = New State__c(Name = 'New York', Abbreviation__c = 'NY', State_ID__c = 32);
       insert theState;
    }
    
    @IsTest
    public static void testCreateNewAddress01(){
        makeMaps();
        PageReference pageRef = Page.C62CreateNewAddress;
        Test.setCurrentPage(pageRef);
        Bill62__Address__c addr = New Bill62__Address__c();
            addr.Bill62__Street_1__c = '123 Test Ave.';
            addr.Bill62__Street_1__c = 'Suite 300';
            addr.Bill62__City__c = 'Buffalo';
            addr.Bill62__State_Province__c = 'NY';
            addr.Bill62__Country__c = 'United States';
        insert addr;       
        Contact c = New Contact(LastName = 'Jones', Bill62__Birthdate__c = system.today(), Address__c = addr.Id );
        insert c; 
        ApexPages.currentPage().getParameters().put('objectType','contact');
        ApexPages.currentPage().getParameters().put('saveURL','xyz.com/'+c.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(addr);
        C62CreateNewAddressController controller = new C62CreateNewAddressController(sc);   
        Test.StartTest();
        controller.submit();
        Test.StopTest();
    }
    
    @IsTest
    public static void testCreateNewAddress02(){
        makeMaps();
        PageReference pageRef = Page.C62CreateNewAddress;
        Test.setCurrentPage(pageRef);
        Bill62__Address__c addr = New Bill62__Address__c();
            addr.Bill62__Street_1__c = '123 Test Ave.';
            addr.Bill62__Street_1__c = 'Suite 300';
            addr.Bill62__City__c = 'Buffalo';
            addr.Bill62__State_Province__c = 'NY';
            addr.Bill62__Country__c = 'Canada';
        insert addr;       
        Account r = New Account(Name = 'Jones', Address_ID__c = addr.Id );
        insert r; 
        ApexPages.currentPage().getParameters().put('objectType','rep');
        ApexPages.currentPage().getParameters().put('saveURL','xyz.com/'+r.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(addr);
        C62CreateNewAddressController controller = new C62CreateNewAddressController(sc);   
        Test.StartTest();
        controller.submit();
        Test.StopTest();
    }
    
    @IsTest
    public static void testCreateNewAddress03(){
        makeMaps();
        PageReference pageRef = Page.C62CreateNewAddress;
        Test.setCurrentPage(pageRef);
        Bill62__Address__c addr = New Bill62__Address__c();
            addr.Bill62__Street_1__c = '123 Test Ave.';
            addr.Bill62__Street_1__c = 'Suite 300';
            addr.Bill62__City__c = 'Buffalo';
            addr.Bill62__State_Province__c = 'NY';
            addr.Bill62__Country__c = 'United States';
        insert addr;       
        Bill62__Order__c o = New Bill62__Order__c(Name = 'Jones', Shipping_Address__c = addr.Id );
        insert o; 
        ApexPages.currentPage().getParameters().put('objectType','order');
        ApexPages.currentPage().getParameters().put('saveURL','xyz.com/'+o.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(addr);
        C62CreateNewAddressController controller = new C62CreateNewAddressController(sc);   
        Test.StartTest();
        controller.submit();
        controller.cancel();
        controller.submit();
        Test.StopTest();
    }
    
    @IsTest
    public static void testCreateNewAddress04(){
        makeMaps();
        PageReference pageRef = Page.C62CreateNewAddress;
        Test.setCurrentPage(pageRef);
        Bill62__Address__c addr = New Bill62__Address__c();
            addr.Bill62__State_Province__c = 'NY';  
            addr.Bill62__Street_1__c = '123 Main St';
            addr.Bill62__City__c = 'Buffalo';
            addr.Bill62__State_Province__c = 'NY';
            addr.Bill62__Zip_Postal_Code__c = '14226';
        insert addr;       
        Bill62__Order__c o = New Bill62__Order__c(Name = 'Jones', Shipping_Address__c = addr.Id );
        insert o; 
        ApexPages.currentPage().getParameters().put('saveURL',null);
        ApexPages.StandardController sc = new ApexPages.StandardController(addr);
        C62CreateNewAddressController controller = new C62CreateNewAddressController(sc);   
        Test.StartTest();
        controller.submit();
        controller.cancel();
        controller.submit();
        Test.StopTest();
    }
    
    @isTest
    public static void testCABChargeBackController(){
        Account a = getRep();
        PageReference pageRef = Page.CABChargeBack;
        Test.setCurrentPage(pageRef);
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        CABChargeBackController controller = new CABChargeBackController(sc);
        controller.CABChargeBack();
        Test.StopTest();
    }
    
    @isTest
    public static void testCABProofController(){
        Account a = getRep();
        PageReference pageRef = Page.CABProofReport;
        Test.setCurrentPage(pageRef);
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        CABProofController controller = new CABProofController(sc);
        controller.CABChargeBack();
        Test.StopTest();
    }
    
    @isTest
    public static void testCancelCardController(){
        Account a = getRep();
        PageReference pageRef = Page.CancelCard;
        Test.setCurrentPage(pageRef);
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        CancelCardController controller = new CancelCardController(sc);
        controller.submit();
        Test.StopTest();
    }
    
    @isTest
    public static void testChangeEnrollerSponsorController(){
        Account a = getRep();
        Account b = new Account();
        b.Entity_ID__c = '5678';
        b.Name = 'Snoop Doggy Dogg';
        b.CAB_Date__c = system.date.newInstance(2010, 10, 10);
        b.RIN__c = '234';
        b.ParentId = a.Id;
        b.Enroller__c = a.Id;
        insert b;
        Account c = new Account();
        c.Entity_ID__c = '9101';
        c.Name = 'Mr T';
        c.CAB_Date__c = system.date.newInstance(2010, 10, 10);
        c.RIN__c = '567';
        insert c;
        a.Enroller__c = c.Id;
        a.ParentId = c.Id;
        update a;
        PageReference pageRef = Page.ChangeEnrollerSponsor;
        Test.setCurrentPage(pageRef);
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ChangeEnrollerSponsorController controller = new ChangeEnrollerSponsorController(sc);
        controller.ChangeEnrollerAndSponsor();
        controller.ChangeSponsor();
        controller.ChangeEnroller();
        controller.BackToRep();
        Test.StopTest();
    }
    
}