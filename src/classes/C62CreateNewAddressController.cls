/*
@Name            : C62CreateNewAddressController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Address Edit override page
*/


public class C62CreateNewAddressController {
    public Bill62__Address__c theAddy {get; set;}
    public String saveURL {get; set;}
    public String addID {get;set;}
    public String objType {get;set;}
    public Map<String,Country__c> CountryMap {get;set;}
    public Map<String,State__c> StateMap {get;set;}
    
    public C62CreateNewAddressController(ApexPages.StandardController controller) {
        theAddy = (Bill62__Address__c)controller.getRecord();
        if (theAddy.Id != null){
            theAddy = [SELECT Bill62__Name__c, Bill62__Type__c, Bill62__Street_1__c, Name, Address_ID__c,
                              Bill62__Customer__c, Bill62__Street_2__c, Bill62__Legacy_Id__c, State__c, Country__c,
                              Bill62__Street_3__c, Bill62__From_Date__c, Bill62__City__c, Bill62__To_Date__c, Bill62__State_Province__c, Bill62__Zip_Postal_Code__c, Bill62__Country__c
                       FROM Bill62__Address__c 
                       WHERE ID =: theAddy.Id];
         }
         if (ApexPages.currentPage().getParameters().get('saveURL') != null){
             saveURL = ApexPages.currentPage().getParameters().get('saveURL');
             saveURL = saveURL.substring(saveURL.Indexof('.com')+5);
             system.debug(saveURL);
         }
         if (ApexPages.currentPage().getParameters().get('objectType') != null){
             objType = ApexPages.currentPage().getParameters().get('objectType');
         }
         
         // populate state and country maps
         CountryMap = New Map<String,Country__c>();
         for (Country__c c : [SELECT ID, Name, Country_ID__c FROM Country__c]){
             CountryMap.put(c.Name, c);
         }
         StateMap = New Map<String,State__c>();
         for (State__c s : [SELECT ID, Name, Abbreviation__c, State_ID__c FROM State__c]){
             StateMap.put(s.Abbreviation__c , s);
         }
         if (theAddy.Bill62__State_Province__c != null && StateMap.containsKey(theAddy.Bill62__State_Province__c)){
             theAddy.State__c = StateMap.get(theAddy.Bill62__State_Province__c).Id;
         }        
         if (theAddy.Bill62__Country__c != null && CountryMap.containsKey(theAddy.Bill62__Country__c)){
             theAddy.Country__c = CountryMap.get(theAddy.Bill62__Country__c).Id;
         }         
    }

    public PageReference Submit(){
        // verify address via API
        system.debug('***theAddy: ' + theAddy);
        if (!C62SmartyStreetsHelper.validateAddress(theAddy.Bill62__Street_1__c,theAddy.Bill62__Street_2__c,theAddy.Bill62__City__c,theAddy.Bill62__State_Province__c,theAddy.Bill62__Zip_Postal_Code__c)){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'This is not a valid address.'));
            return null;
        }
        
        String str1 = theAddy.Bill62__Street_1__c;
        String str2 = theAddy.Bill62__Street_2__c;
        String city = theAddy.Bill62__City__c;
        String state = theAddy.Bill62__State_Province__c;
        Decimal stateCode = StateMap.get(state).State_ID__c;
        String zip = theAddy.Bill62__Zip_Postal_Code__c;
        String country = theAddy.Bill62__Country__c;
        Decimal countryCode;            
        if (theAddy.Bill62__Country__c == 'United States'){ 
            countryCode = 1;
        } else {    
            countryCode = CountryMap.get(country).Country_ID__c;
        }        
        // add or update record to 5linx API library
        string result;
        if (theAddy.Address_ID__c == null){
            if(!Test.isRunningTest()){
                result = CreateAddress(str1,str2,city,stateCode,state,zip,countryCode);        
            } else{
                result = 'Address Created   intAddressId": 1234567   ';
            } 
            if (result.contains('Address Created')) addID = result.substring(result.indexOf('intAddressId') + 15, result.indexOf('intAddressId') + 22);
            theAddy.Address_ID__c = addID;
            if(theAddy.Bill62__Street_1__c != null){
                if(theAddy.Bill62__Street_1__c.length() < 80){
                    theAddy.Name = theAddy.Bill62__Street_1__c;
                }else{
                    theAddy.Name = theAddy.Bill62__Street_1__c.substring(0,79);
                }
            }
        } else {
            system.debug('calling update address');
            UpdateAddress(theAddy.Address_ID__c,str1,str2,city,stateCode,state,zip,countryCode);
            if(theAddy.Bill62__Street_1__c != null){
                if(theAddy.Bill62__Street_1__c.length() < 80){
                    theAddy.Name = theAddy.Bill62__Street_1__c;
                }else{
                    theAddy.Name = theAddy.Bill62__Street_1__c.substring(0,79);
                }
            }
        }         
        
        if (saveURL != null){
            Bill62__Order__c o;
            Contact c;
            Account a;
            if (objType == 'order') o = [SELECT Shipping_Address__c FROM Bill62__Order__c Where Id = :saveURL];
            if (objType == 'contact') c = [SELECT Address__c FROM Contact Where Id = :saveURL];
            if (objType == 'rep') a = [SELECT Address_ID__c FROM Account Where Id = :saveURL];
            // C62Utilities.cSetExecuted.add('dontFireTrigger');
            upsert theAddy;
            if (objType == 'order'){
                o.Shipping_Address__c = theAddy.Id;
                update o;
            } else if (objType == 'contact'){
                c.Address__c = theAddy.Id;
                update c;
            } else if (objType == 'rep'){
                a.Address_ID__c = theAddy.Id;
                update a;
            }
            
            return new PageReference('/' + saveURL); 
        } else {
            // C62Utilities.cSetExecuted.add('dontFireTrigger');
            upsert theAddy;
            return new PageReference('/' + theAddy.id); 
        }
    }

    public PageReference Cancel(){
        if (saveURL != null){
            return new PageReference('/' + saveURL); 
        } else {
            return new PageReference('/' + theAddy.Id);
        }
    }
    
    public static string CreateAddress(String strAddress1, String strAddress2, String strCity, Decimal intStateID, 
                                       String strStateProvince, String strZip, Decimal intCountryId){
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req;
                if (strAddress1 != null){
                    xml_req =   '{"strAddress1":"'+strAddress1+'",';
                } else {
                    xml_req =   '{"strAddress1":null,';
                }
                if (strAddress2 != null){
                    xml_req +=  '"strAddress2":"'+strAddress2+'",';
                } else {
                    xml_req +=  '"strAddress2":null,';
                }
                if (strCity != null){
                    xml_req +=  '"strCity":"'+strCity+'",';
                } else {
                    xml_req +=  '"strCity":null';
                }    
                if (intStateID != null){
                    xml_req +=  '"intStateID":"'+intStateID+'",';
                } else {
                    xml_req +=  '"intStateID":null,';
                }
                if (strStateProvince != null){
                    xml_req +=  '"strStateProvince":"'+strStateProvince+'",';
                } else {
                    xml_req +=  '"strStateProvince":null,';
                }   
                if (strZip != null){
                    xml_req +=  '"strZip":"'+strZip+'",'; 
                } else {
                    xml_req +=  '"strZip":null,';
                }   
                if (intCountryId != null){
                    xml_req +=  '"intCountryID":"'+intCountryId+'"}';
                } else {
                    xml_req +=  '"intCountryID":null}';
                }  
                                          
         String path = apiHelp.URL__c + 'CreateAddress';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         String result;
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);    
             system.debug('xml_req: '+xml_req);        
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody();
         } else {
             result = 'Address Created, "intAddressId": 3696996 }';
         }
         return result;
     }     
         
     public static void UpdateAddress(String intAddressID, String strAddress1, String strAddress2, String strCity, 
                                      Decimal intStateID, String strStateProvince, String strZip, Decimal intCountryId){         
         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req;
                if (intAddressID != null){
                    xml_req = '{"intAddressID":"'+intAddressID+'",';
                } else {
                    xml_req = '{"intAddressID":null,';
                }               
                if (strAddress1 != null){
                    xml_req +=   '"strAddress1":"'+strAddress1+'",';
                } else {
                    xml_req +=   '"strAddress1":null,';
                }
                if (strAddress2 != null){
                    xml_req +=  '"strAddress2":"'+strAddress2+'",';
                } else {
                    xml_req +=  '"strAddress2":null,';
                }
                if (strCity != null){
                    xml_req +=  '"strCity":"'+strCity+'",';
                } else {
                    xml_req +=  '"strCity":null';
                }    
                if (intStateID != null){
                    xml_req +=  '"intStateID":"'+intStateID+'",';
                } else {
                    xml_req +=  '"intStateID":null,';
                }
                if (strStateProvince != null){
                    xml_req +=  '"strStateProvince":"'+strStateProvince+'",';
                } else {
                    xml_req +=  '"strStateProvince":null,';
                }   
                if (strZip != null){
                    xml_req +=  '"strZip":"'+strZip+'",'; 
                } else {
                    xml_req +=  '"strZip":null,';
                }   
                if (intCountryId != null){
                    xml_req +=  '"intCountryID":"'+intCountryId+'"}';
                } else {
                    xml_req +=  '"intCountryID":null}';
                }  
                      
         String path = apiHelp.URL__c + 'UpdateAddress';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         String result;
         if(!Test.IsRunningTest()){
             HttpResponse res = h.send(req);
             system.debug('xml_req: '+xml_req);   
             system.debug('THIS IS THE BODY!!' + res.getBody());
             result = res.getBody(); 
         }
    }
}