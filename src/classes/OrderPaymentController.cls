/*
@Name            : OrderPaymentController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Order Manual Payment page.  Takes payments, pushes to 5Linx TC
*/


public class OrderPaymentController {
    public boolean error {get;set;}
    public boolean saved {get;set;}
    public Bill62__Payment__c newPay {get;set;}
    public String page {get;set;}
    public List<SelectOption> expMonth{get;set;}
    public List<SelectOption> expYear{get;set;}
    public String selectedExpMonth{get;set;}
    public String selectedExpYear{get;set;}
    public Bill62__Payment_Method__c cPayMethod{get;set;}
    public Bill62__Address__c cBillAddr {get;set;}
    public Boolean existingAddress {get;set;}
    public Boolean existingPayMeth {get;set;}
    public Decimal paymentAmount {get;set;}
    public Bill62__Order__c theOrder {get;set;}
    public Contact theCustomer {get;set;}
    public Boolean complete {get;set;}
    public Boolean manualPmt {get;set;}
    public List<SelectOption> paymentReasonList {get;set;}
    public String paymentReasonID {get;set;}
    public String theProductID {get;set;}
    public String theOrderItemID {get;set;}
    public String theOrderID{get;set;}
    public Map<String, Payment_Reason__c> paymentReasonMap {get;set;}
    public User currentUser{get;set;}
    public List<SelectOption> payMethOptions{get;set;}
    public Map<String, Bill62__Payment_Method__c> payMethMap{get;set;}
    public String payMethId{get;set;}
    public String entityId{get;set;}
    public Boolean isWellness{get;set;}
    public String theNOPID{get;set;}
    public List<paymentWrapper> wrapperList{get;set;} //Snoop Dogg, Dr. Dre, 2Chainz, Jay-Z, Kanye West, Eminem
    public Decimal taxAmount{get;set;}
    public Decimal shipAmount{get;set;}

    public OrderPaymentController(ApexPages.StandardController controller) {
    
        //initialize booleans
        error = false;
        isWellness = false;
        complete = false;
        saved = false;
        existingAddress = false;
        existingPayMeth = false;
        manualPmt = false;
        page = '0.5';
        
        String orderID = controller.getID();
        paymentReasonMap = new Map<String, Payment_Reason__c>();
        
        currentUser = [SELECT ID, TC_User_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        theOrder = [SELECT ID, Order_Idtxt__c, Bill62__Customer__c, Carrier__r.Product_Type_ID__c, Shipping_Address__c,
            Bill62__End_Date__c, Bill62__Account__r.RIN__c, Bill62__Account__r.Entity_ID__c, Payment_Method__c, NOP_ID__c 
            FROM Bill62__Order__c WHERE ID = :orderId LIMIT 1];
        
        theOrderID = theOrder.Order_IDtxt__c;
        system.debug('theOrderID: '+theOrderID);
        theNOPID = theOrder.NOP_ID__c;
        List<Bill62__Subscription__c> quantityList = [SELECT ID FROM Bill62__Subscription__c WHERE Order__c = :theOrder.ID];
        system.debug('quantityList size: '+quantityList.size());
        //get wrapping
        wrapperList = new List<paymentWrapper>();
        paymentWrapper firstWrapper = new paymentWrapper();
        firstWrapper.theOrder = theOrder;
        //firstWrapper.hasOverage = false; //set default
        if(quantityList.size() > 0){
            firstWrapper.quantity = quantityList.size();
            system.debug('setting firstWrapper quantity: '+firstWrapper.quantity);
        }else{
            firstWrapper.quantity = 0;
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Order Item found for this Order! Please check the Order Items.'));
            return;
        }
        wrapperList.add(firstWrapper);
        if(theNOPID != null) {
            isWellness = true;
            List<Bill62__Order__c> tempList = [SELECT ID, Order_Idtxt__c, Bill62__Customer__c, Carrier__r.Product_Type_ID__c, Shipping_Address__c,
            Bill62__End_Date__c, Bill62__Account__r.RIN__c, Bill62__Account__r.Entity_ID__c, Payment_Method__c, NOP_ID__c 
            FROM Bill62__Order__c WHERE NOP_ID__c = :theNOPID];
            
            for(Bill62__Order__c ord: tempList){
                if(wrapperList[0].theOrder.Id != ord.Id){
                    paymentWrapper secondWrapper = new paymentWrapper();
                    //secondWrapper.hasOverage = false; //set default
                    secondWrapper.theOrder = ord;
                    wrapperList.add(secondWrapper);
                }
            }
        }
            
        List<Contact> cList = [SELECT ID, Customer_ID__c, FirstName, LastName, Address__c, Name,
            Address__r.Bill62__Street_1__c,  Address__r.Bill62__City__c, Address__r.Bill62__State_Province__c,
            Address__r.Bill62__Zip_Postal_Code__c, HomePhone, Address__r.Country__r.Country_ID__c 
            FROM Contact WHERE ID = :theOrder.Bill62__Customer__c LIMIT 1];
        if(cList.size() > 0){
            theCustomer = cList[0];
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Customer found for this Rep! Please set the Customer field and try again.'));
            error = true;
            return;
        }
            
        List<Bill62__Subscription__c> orderLineList = [SELECT ID, Order_Line_ID__c, Bill62__Product__r.Name,
            Bill62__Product__r.External_ID__c, Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Product_Category__r.Has_Overage__c 
            FROM Bill62__Subscription__c WHERE Order__c = :theOrder.Id];
            
        if(orderLineList.size() > 0){
            theProductID = orderLineList[0].Bill62__Product__r.External_ID__c;
            theOrderItemID = orderLineList[0].Order_Line_ID__c;  
            if(wrapperList.size() > 0) wrapperList[0].productName = orderLineList[0].Bill62__Product__r.Name;
            
            /*remove when NOPs work
            String fam = orderLineList[0].Bill62__Product__r.Product_Category__r.Name;
            if(fam.Equals('Wellness Reship') || fam.Equals('Coffee Recurring') || fam.Equals('Nutrition')
            || fam.Equals('Nutrition Recurring') || fam.Equals('Premium Bundle')){
                error = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Wellness Orders must be processed in TC at this time'));
                return;
            }*/
        }
        system.debug('ProductID: '+theProductID);
        
        saved = false;
        paymentAmount = 0.11;//TODO:set this from the Product + tax + shipping later
        manualPmt = true;
        
        // checks profile and product type
        Integer pType = integer.valueof(theOrder.Carrier__r.Product_Type_ID__c);
        system.debug('ptype: '+ptype);
        if ( pType != 85 && pType != 77 && pType != 45 && pType != 62 ){
            manualPmt = false;
        }

        //find existing payment methods so you don't make a new one every time
        List<Bill62__Payment_Method__c> payMethList = [SELECT ID, Bill62__Card_Type__c,Bill62__Card_Number__c,
         Bill62__CVV_Code__c, Bill62__Expiration_Date__c, Bill62__Billing_Address__r.Bill62__Street_1__c,
         Bill62__Billing_Address__r.Bill62__Street_2__c, Bill62__Billing_Address__r.Bill62__Street_3__c, 
         Bill62__Billing_Address__r.Bill62__City__c, Bill62__Billing_Address__r.Bill62__State_Province__c, 
         Bill62__Billing_Address__r.Bill62__Zip_Postal_Code__c, Bill62__Billing_Address__r.Bill62__Country__c,
         Bill62__Payment_Type__c, Name, Default__c, Payment_Method_ID__c, Bill62__Card_Holder_Name__c, Bill62__Billing_Address__c 
         FROM Bill62__Payment_Method__c WHERE ID = :theOrder.Payment_Method__c LIMIT 1];//Bill62__Customer__c = :theCustomer.Id AND Default__c = TRUE LIMIT 1];
         system.debug('paymethlists size: '+payMethList.size());
        payMethOptions = new List<SelectOption>();
        payMethMap = new Map<String, Bill62__Payment_Method__c>();
        if(payMethList.size() > 0){
            for(Bill62__Payment_Method__c pm: payMethList){
                payMethOptions.add(new Selectoption(pm.Id, pm.Name));
                payMethMap.put(pm.Id, pm);
                if(pm.Default__c) {cPayMethod = pm;payMethID = pm.Id;}
            }
            existingPayMeth = true;
        }else{
            cPayMethod = new Bill62__Payment_Method__c();
            cPayMethod.Bill62__Customer__c = theCustomer.Id;
            cPayMethod.Name = theCustomer.FirstName+' '+theCustomer.LastName+'-Default';
            existingPayMeth = false;
        }
        if(existingPayMeth) saved = true;
        
        
        
        system.debug('existingPayMeth: '+existingPayMeth);
        system.debug('saved: '+saved);
        
        if(cPayMethod.Bill62__Billing_Address__c != null){
            cBillAddr = [SELECT ID, Bill62__Street_1__c, Bill62__Street_2__c, Bill62__Street_3__c, Country__r.Country_ID__c,
                Bill62__City__c, Bill62__State_Province__c, Bill62__Zip_Postal_Code__c, Bill62__Country__c, Country__c
                FROM Bill62__Address__c WHERE ID = :cPayMethod.Bill62__Billing_Address__c];
            existingAddress = true;
        }else{
            if(theOrder.Shipping_Address__c != null && cPayMethod.Bill62__Billing_Address__c == null){
                cBillAddr = [SELECT ID, Bill62__Street_1__c, Bill62__Street_2__c, Bill62__Street_3__c, Country__r.Country_ID__c,
                    Bill62__City__c, Bill62__State_Province__c, Bill62__Zip_Postal_Code__c, Bill62__Country__c, Country__c
                    FROM Bill62__Address__c WHERE ID = :theOrder.Shipping_Address__c];
                if(!existingPayMeth) cPayMethod.Bill62__Billing_Address__c = cBillAddr.Id;
                existingAddress = true;
            }else{
                cBillAddr = new Bill62__Address__c();
                existingAddress = false;
            }
        }
        
        expMonth = new List<SelectOption>();
        expYear = new List<SelectOption>();
        for(Integer i=1;i<=12;i++){
            String iString;
            if(i<10){
                iString='0'+i;
            }else{
                iString=i+'';
            }
            expMonth.add(new SelectOption(iString,iString));
        }
        for(Integer i=Date.Today().year();i<Date.Today().year()+10;i++){
            expYear.add(new SelectOption(i+'',i+''));
        }
        selectedExpYear=Date.Today().year()+'';
        selectedExpMonth=Date.Today().month()+'';
        
        paymentReasonList = getPaymentReasons();
        
        if(paymentReasonList != null){
            if(paymentReasonList.size() == 0){
                error = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Payment Reasons found! Please check the status of the Order to ensure Payment is possible.'));
            }
        }
    }


    public pageReference editExisting(){
        existingPayMeth = false;
        saved = false;
        return null;
    }

    public pageReference changePayMeth(){
        system.debug('changing payMeth: '+payMethMap.get(payMethID));
        cPayMethod = payMethMap.get(payMethID);
        return null;
    }


    public pageReference next(){
        page = '1';
        paymentAmount = paymentReasonMap.get(paymentReasonID).Amount__c;
        if(wrapperList != null){
            wrapperList[0].paymentReasonAmount = paymentAmount;
            system.debug('setting paymentReasonAmount on wrapper[0]: '+wrapperList[0].paymentReasonAmount);
        }else{
            system.debug('lost the wrappers!');
        }
        system.debug('payment reason value selected: '+paymentReasonID);
        system.debug('paymentreason Map: '+paymentReasonMap);
        system.debug('paymentreason object: '+paymentReasonMap.get(paymentReasonID));
        system.debug('payment reason ID in next: '+paymentReasonMap.get(paymentReasonID).External_ID__c);
        system.debug('Payment Amount: '+paymentAmount);
        Decimal taxAmt, shipAmt;
        //get taxes
        system.debug('OrderID: '+theOrderID+' and OrderItemID: '+theOrderItemID);
        if(theOrderID == null || theOrderItemID == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Order ID or Order Item ID are missing!  Please assign an Order and Order Item ID to this record before continuing.'));
            page = '0.5';
            return null;
        }
        
        //don't need to worry about wrappers for tax and shipping, TC figures it out if you just send it one
        String tax = CalcTaxesNOW(integer.valueof(theOrderID),integer.valueof(theOrderItemID));
        system.debug('Tax Response: '+tax);
        if (tax.contains('"Success"')){
            tax = tax.substring(tax.indexOf('TaxAmount') + 12, tax.indexOf('TaxAmount') + 17);
            if (tax.contains(',')){
                tax = tax.substring(0, tax.indexof(','));
                system.debug('Tax1: '+tax);
            }
            if(tax == '0'){
                system.debug('Tax2: '+tax);
                taxAmt = 0;
                taxAmount = 0; //class-level
            } else {
                system.debug('Tax3: '+tax);
                paymentAmount += decimal.valueof(tax);
                taxAmount = decimal.valueOf(tax);
            }
        }
        
        //get shipping
        String ship = CalcShippingNOW(integer.valueof(theOrder.Order_IDtxt__c));
        system.debug('Shipping Response: '+ship);
        //ghetto parser
        if (ship.contains('"Success"')){
            ship = ship.substring(ship.indexOf('ShippingAmount') + 17, ship.indexOf('ShippingAmount')+22);
            if (ship.contains(',')){
                ship = ship.substring(0, ship.indexof(','));
                system.debug('Shipping1: '+ship);
            }
            if(ship == '0'){
                system.debug('Shipping2: '+ship);
                shipAmt = 0;
                shipAmount = 0; //class-level
            } else {
                system.debug('Shipping3: '+ship);
                paymentAmount += decimal.valueof(ship);
                shipAmount = decimal.valueOf(ship);
                shipAmt = 0;
            }
        } else if (ship.contains('No shipping required')){
            shipAmt = 0;
            shipAmount = 0;
        }
        
        paymentAmount = paymentAmount.setScale(2);
        system.debug('final payment amount: '+paymentAmount);
        if(isWellness){
            updateWrappers(taxAmt, shipAmt);
        } else {
            wrapperList[0].amount = paymentAmount;
        }
        
        paymentAmount = 0;
        for(paymentWrapper pw: wrapperList){
            system.debug('current wrapper while totaling payment amount: '+pw);
            if(pw.quantity != null && pw.quantity != 0){
                paymentAmount += pw.paymentReasonAmount*pw.quantity;
            }else{
                paymentAmount += pw.paymentReasonAmount;
            }
            system.debug('Order ID '+pw.theOrder.Id+' changed paymentAmount to '+paymentAmount);
        }
        system.debug('adding tax: '+taxAmount);
        paymentAmount += taxAmount;
        system.debug('adding shipping: '+shipAmount);
        paymentAmount += shipAmount;
        return null;
    }
    
    public void updateWrappers(Decimal taxAmt, Decimal shipAmt){
        Integer intWrappers = wrapperList.size();
        Set<String> orderIDs = new Set<string>();
        for(paymentWrapper pw: wrapperList){
            orderIDs.add(pw.theOrder.Id);
        }
        List<Bill62__Subscription__c> orderLineList = [SELECT ID, Order_Line_ID__c, Bill62__Product__r.Name, Order__r.Order_IDtxt__c, 
            Bill62__Product__r.External_ID__c, Bill62__Product__r.Product_Category__r.Name, Order__c, Bill62__Product__c, 
            Bill62__Product__r.Product_Category__r.Has_Overage__c 
            FROM Bill62__Subscription__c WHERE Order__c IN :orderIDs];
            
        system.debug('updateWrappers OLList size: '+orderLineList.size());    
        Map<String, Bill62__Subscription__c> orderToLineMap = new Map<String, Bill62__Subscription__c>();
        Map<String, Integer> quantityMap = new Map<String,Integer>();
        Set<String> productIDs = new Set<String>();
        Map<String, Decimal> OrderOverageMap = new Map<String, Decimal>(); //an Order will only ever have 1 overage line
        
        for(Bill62__Subscription__c ol: orderLineList){
        
            //look for overages
            Decimal overage;
            if(ol.Order__r.Order_IDtxt__c != null && ol.Bill62__Product__r.External_ID__c != null && ol.Bill62__Product__r.Product_Category__r.Has_Overage__c)
            overage = C625LinxAPIHelper.getOverageAmount(Integer.valueOf(ol.Order__r.Order_IDtxt__c), Integer.valueOf(ol.Bill62__Product__r.External_ID__c));
            if(overage != null){
                OrderOverageMap.put(ol.Order__c, overage);
            }
            
            orderToLineMap.put(ol.Order__c, ol);
            productIDs.add(ol.Bill62__Product__c);
            if(quantityMap.containsKey(ol.Order__c)){
                quantityMap.put(ol.Order__c, quantityMap.get(ol.Order__c)+1);
                system.debug('incrementing a quantity');
            }else{
                quantityMap.put(ol.Order__c, 1);
                system.debug('adding new quantity');
            }
        }
        
        List<Payment_Reason__c> payReasonList = [SELECT ID, Product__c, Amount__c FROM Payment_Reason__c WHERE Product__c IN :productIDs];
        Map<String,Decimal> productToAmountMap = new Map<String, Decimal>();
        for(Payment_Reason__c pr: payReasonList){
            if(pr.Amount__c != null) productToAmountMap.put(pr.Product__c, pr.Amount__c);
        }
        
        //have to split the tax among the wellness orders
        Decimal splitTax;
        if(intWrappers != 0){
            splitTax = taxAmount/intWrappers;
        } else {
            splitTax = taxAmount;
        }
        
        for(paymentWrapper pw: wrapperList){
            system.debug('updateWrapper: '+pw);
            String thisOrderID = pw.theOrder.Id;
            if(orderToLineMap.containsKey(thisOrderID)){
                pw.productName = orderToLineMap.get(thisOrderID).Bill62__Product__r.Name;
                pw.quantity = quantityMap.get(thisOrderID);
                system.debug('setting name and quantity: '+pw);
                if(productToAmountMap.containsKey(orderToLineMap.get(thisOrderID).Bill62__Product__c)){
                    if(paymentReasonMap.get(paymentReasonID).External_ID__c != '435' && paymentReasonMap.get(paymentReasonID).External_ID__c != '436'){ //wellness reship is shipping only
                        pw.paymentReasonAmount = productToAmountMap.get(orderToLineMap.get(thisOrderID).Bill62__Product__c);
                        pw.amount = (pw.paymentReasonAmount*pw.Quantity) + splitTax;
                        if(OrderOverageMap.containsKey(thisOrderID)){
                            pw.amount += OrderOverageMap.get(thisOrderID);
                        }
                    } else {
                        pw.paymentReasonAmount = 0;
                        pw.amount = 0;
                        if(OrderOverageMap.containsKey(thisOrderID)){
                            pw.amount += OrderOverageMap.get(thisOrderID);
                        }
                    }
                    system.debug('setting amount and prAmount: '+pw);
                }
            }
        }

        wrapperList[0].amount += shipAmt;
        
        system.debug('wrapperList after updating: '+wrapperList);
        
    }
    
    public pageReference back(){
        page = '0.5';
        return null;
    }

    public pageReference toOrder(){
        return new PageReference('/'+theOrder.Id);
    }
    
    public pageReference toPayment(){
        return new PageReference('/'+newPay.Id);
    }
    
    public pageReference makeNew(){
        existingPayMeth = false;
        saved = false;
        cPayMethod = new Bill62__Payment_Method__c();
        cPayMethod.Name = theCustomer.FirstName+' '+theCustomer.LastName+'-Default';
        return null;
    }
    
    public pageReference savePayMeth(){
        if(!existingAddress){
            try{
                upsert cBillAddr;
           }catch(Exception e){
               ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage() ) ); 
           }
        }
        if(!existingPayMeth){
            try{
                cPayMethod.Bill62__Billing_Address__c = cBillAddr.Id;
                cPayMethod.Bill62__Customer__c = theCustomer.ID;
                cPayMethod.Bill62__Expiration_Date__c = system.Date.NewInstance(integer.valueof(selectedExpYear), integer.valueof(selectedExpMonth),1);
                cPayMethod.Default__c = true;
                cPayMethod.Bill62__Payment_Type__c = 'Credit Card';
                system.debug(cPayMethod.Bill62__Expiration_Date__c);
                upsert cPayMethod;   
                theOrder.Payment_Method__c = cPayMethod.Id;
                update theOrder;
                saved = true; 
                payMethOptions.add(new Selectoption(cPayMethod.Id, cPayMethod.Name));
                payMethMap.put(cPayMethod.Id, cPayMethod);
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, 'Card Saved!' ) );
            }catch(Exception e){
               ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage() ) ); 
            }
        } else {
            try{
                cPayMethod.Bill62__Billing_Address__c = cBillAddr.Id;
                cPayMethod.Bill62__Expiration_Date__c = system.Date.NewInstance(integer.valueof(selectedExpYear), integer.valueof(selectedExpMonth),1);
                upsert cPayMethod;
                saved = true;
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, 'Card Saved!' ) );
            } catch(Exception e){
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage() ) );
            }
        }
        existingPayMeth = true;
        return null;
    }
    
    public pageReference makePayment(){
        Bill62.PaymentUtilities utils = new Bill62.PaymentUtilities();
        system.debug('wrapperList: '+wrapperList);
        
        if(newPay == null){
            newPay = new Bill62__Payment__c();
            newPay.Bill62__Amount__c = paymentAmount.setScale(2);
            newPay.Bill62__Payment_Date__c = DateTime.NOW().date();
            //newPay.Bill62__Subscription_Line__c = sl.Id; TODO:set this to the new Order Line
            system.debug(theCustomer.Id);
            if(paymentReasonMap.get(paymentReasonID).Product__r.Bill62__Payment_Gateway__c != null){
                system.debug('setting the payment gateway: '+paymentReasonMap.get(paymentReasonID).Product__r.Bill62__Payment_Gateway__c);
                newPay.Bill62__Payment_Gateway__c = paymentReasonMap.get(paymentReasonID).Product__r.Bill62__Payment_Gateway__c;
            }
            newPay.Bill62__Customer__c = theCustomer.Id;
            newPay.Bill62__Status__c = 'Queued';
            newPay.Bill62__Payment_Method__c = cPayMethod.Id;
            newPay.Bill62__Order__c = theOrder.Id;
            newPay.Bill62__Description__c = paymentReasonMap.get(paymentReasonID).Product__r.Name;
        }
        system.debug('card type: '+cPayMethod.Bill62__Card_Type__c);
        
        Bill62.C62PaymentGatewayClasses.MakePaymentResponse result = utils.MakePayment2(cPayMethod, newPay, theCustomer.FirstName, theCustomer.LastName);
        
        system.debug('Passed Make Payment: '+result);
        if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
            system.debug('Starting success');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Payment Charged Successfully!'));
            newPay.Bill62__Status__c='Paid';
            newPay.Bill62__Amount_Used__c = newPay.Bill62__Amount__c;
            newPay.Bill62__Transaction_Id__c = result.TransactionID;
            newPay.Bill62__Approval_Code__c = result.AuthorizationID;
            newPay.Bill62__Last_Authorization_Attempt__c=DateTime.NOW();
            if(newPay.Bill62__Number_Of_Authorization_Attempts__c == null){
                newPay.Bill62__Number_Of_Authorization_Attempts__c = 1;
            }else{
                newPay.Bill62__Number_Of_Authorization_Attempts__c++;
            }
            complete=true;
            //theOrder.Bill62__End_Date__c = theOrder.Bill62__End_Date__c.addMonths(Integer.valueOf(paymentReasonMap.get(paymentReasonID).Months__c));
            theOrder.Has_Current_Payment__c = false;
            
            //call postPaymentProcess on the Order
            Boolean pppError = false;
            //for(paymentWrapper pw: wrapperList){
                paymentWrapper pw = wrapperList[0];
                system.debug('postpayment params: '+newPay+'\n\r'+cPayMethod+'\n\r'+paymentReasonMap.get(paymentReasonID).External_ID__c+'\n\r'+pw.theOrder+'\n\r'+pw.amount);
                newPay = postPaymentProcess(newPay, cPayMethod, paymentReasonMap.get(paymentReasonID).External_ID__c, pw.theOrder, paymentAmount);
                if(newPay.TC_Integration_Retry__c != null) pppError = true;
            //}
            if(pppError){
                try{
                    Bill62.C62PaymentGatewayClasses.VoidAuthorizationResponse refundResult = utils.voidAuthorization(newPay);
                    if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                        newPay.Bill62__Status__c = 'Refunded';
                        newPay.Bill62__Amount_Refunded__c = newPay.Bill62__Amount__c;
                        ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Error in Post Payment Process. Payment refunded.' ) );
                    } else {
                        ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Error in Post Payment Process. Payment refund failed. Please contact your supervisor.' ) );
                        newPay.Bill62__Processing_Message__c = result.ErrorDescription;
                    }
                } catch(Exception e){
                    newPay.Bill62__Status__c = 'Exception';
                    newPay.Bill62__Processing_Message__c = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                    String error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                    ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Error in Post Payment Process. Payment refund failed. Please contact your supervisor.' ) );
                }
            }/* else {
                try{
                    Bill62.C62PaymentGatewayClasses.CaptureAuthorizationResponse captureResult = utils.CaptureAuthorization(newPay);
                    if(result.Effect == Bill62.C62PaymentGatewayClasses.TransactionEffect.Success){
                        //excellent!  it worked.
                    } else {
                        ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Payment Authorization succeeded, but Capture failed.  Please contact your supervisor.' ) );
                    }
                } catch(Exception e){
                    ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, 'Payment Authorization succeeded, but Capture failed.  Please contact your supervisor.' ) );
                }
            }*/
                
            update theOrder;
            upsert newPay;
            system.debug('Ending Success');
        } else {
            system.debug('starting error');
            newPay.Bill62__Status__c='Rejected';
            newPay.Bill62__Last_Authorization_Attempt__c=DateTime.NOW();
            if(newPay.Bill62__Number_Of_Authorization_Attempts__c == null){
                newPay.Bill62__Number_Of_Authorization_Attempts__c = 1;
            }else{
                newPay.Bill62__Number_Of_Authorization_Attempts__c++;
            }
            system.debug('Error right before pagemessage');
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, result.ErrorDescription ) );
            system.debug('Ending Error');
        }
        return null;
    }
    
    
    public List<SelectOption> getPaymentReasons(){
        //get payment reasons
        String TCcustomerId = theCustomer.Customer_ID__c;
        String userId = userInfo.getUserId();
        List<User> userlist = [SELECT TC_User_ID__c FROM User WHERE Id = :userId];
        String TCUserId;
        if(userlist.size() > 0) TCUserId = userlist[0].TC_User_ID__c;
        APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
        String xml_req =   '{"intCustomerId":"'+TCcustomerId+'",';
            if(cBillAddr.Country__c != null){
                if(cBillAddr.Country__r.Country_ID__c != null){
                    xml_req +=  '"intCountryId":"'+cBillAddr.Country__r.Country_ID__c+'",';
                }
           } else {
               xml_req +=  '"intCountryId":"1",';
           }
           if(theProductID != null){
                xml_req +=  '"intProductsID":"'+theProductID+'",'; 
           } else {
                xml_req +=  '"intProductsID":null,';   
           }
                xml_req +=  '"intUserId":"411",';//'+TCUserId+'",';
                xml_req +=  '"intOrderID":"'+theOrder.Order_Idtxt__c+'"}';        
         system.debug('getpaymentReason request: '+xml_req);
         String path = apiHelp.URL__c + 'GetPaymentReasonList';
         String result;
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         HttpResponse res = h.send(req);
         system.debug('THIS IS THE BODY!!' + res.getBody());
         
        //now to parse it
        List<paymentReason> jsonList = new List<paymentReason>();
        system.debug('jsonlists size:'+jsonList.size());
        Map<String,Object> myMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        for(String key : myMap.keyset()){
            system.debug(key+':'+mymap.get(key));
            if(key.isNumeric()){
                Map<String, Object> something = (Map<String, Object>)mymap.get(key);
                paymentReason payR = new paymentReason();
                system.debug('JSON: '+JSON.deserializeUntyped(res.getBody())+'<<');
                system.debug('something.get(intPaymentReasonId): '+something.get('intPaymentReasonId'));
                if (something.get('intPaymentReasonId') != ''){
                    payR.intPaymentReasonId = (Integer)something.get('intPaymentReasonId');
                    payR.strPaymentReasonDs = (String)something.get('strPaymentReasonDs');
                }                
                jsonList.add(payR);
                system.debug('jsonish object:'+payR);
            }
        }

        Set<String> payIDSet = new Set<String>();
        for(paymentReason r: jsonList){
            payIDSet.add(String.valueOf(r.intPaymentReasonId));
        }
        List<Payment_Reason__c> payReasonList = [SELECT Id, External_ID__c, Product__r.External_ID__c, Amount__c, Months__c, Name, 
            Product__r.Bill62__Payment_Gateway__c, Product__r.Name
            FROM Payment_Reason__c WHERE External_ID__c IN :payIDSet];
            
        List<SelectOption> payReasons = new List<SelectOption>();
        for(Payment_Reason__c pr: payReasonList){
            if(pr.Product__r.External_ID__c != null){
                system.debug('adding to payReasonMap: '+pr);
                payReasons.add(new SelectOption(pr.Id, pr.Name));
                paymentReasonMap.put(pr.Id, pr);
            }
        }
        return payReasons;
    }
    
     // calls CalcTaxes to get Taxes
    public static String CalcTaxesNOW(Integer intOrderId, Integer intOrderItemId){

         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"orderId":"'+intOrderId+'",'+
                            '"orderItemId":"'+intOrderItemId+'"}';        
         system.debug('tax request: '+xml_req);
         String path = apiHelp.URL__c + 'CalcTaxes';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         HttpResponse res = h.send(req);
         return res.getBody();
    }
    
    // calls CalcShipping to get Shipping Amount
    public static String CalcShippingNOW(Integer intOrderId){

         APIHelper__c apiHelp = APIHelper__c.getOrgDefaults();
         String xml_req =  '{"orderId":"'+intOrderId+'"}';        
         system.debug('shipping request: '+xml_req);              
         String path = apiHelp.URL__c + 'CalcShipping';
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         req.setBody(xml_req);
         req.setEndpoint(path);
         req.setHeader('AuthInfo','51nX4p1Us3rsLf');
         req.setHeader('Content-Type','application/json');
         req.setMethod('POST');
         req.setTimeout(120000);
         
         HttpResponse res = h.send(req);
         return res.getBody();
    }
    
    public class paymentReason{
        public Integer intPaymentReasonId{get;set;}
        public String strPaymentReasonDs {get;set;}
        
        public paymentReason(){}
    }
    
    
    //payReasonID is the external ID
    public Bill62__Payment__c postPaymentProcess(Bill62__Payment__c p, Bill62__Payment_Method__c payMeth, String payReasonID, 
        Bill62__Order__c thisOrder, Decimal theAmount){
        
        Integer intPayMethodId;
        String FName, LName, cardNum, expDate, routingNum, bankNum;
        intPayMethodId = findPayMethodId(payMeth.Bill62__Card_Type__c);
        if(theCustomer.FirstName != null) FName = theCustomer.FirstName;
        if(theCustomer.LastName != null) LName = theCustomer.LastName;
        cardNum = payMeth.Bill62__Card_Number__c.replace('-','');
        String expDateTemp = string.valueof(payMeth.Bill62__Expiration_Date__c);
        if(expDateTemp != null){
             expDate = expDateTemp.substring(5,7) + expDateTemp.substring(2,4);
        }else{
            expDate = null;
        }
        
        Decimal pmtAmount;
        if(theAmount == null){
            pmtAmount = p.Bill62__Amount__c;
        } else {
            pmtAmount = theAmount;
        }
        String AuthCode = p.Bill62__Approval_Code__c;
        String refId = p.Bill62__Transaction_Id__c;
        // Rep info
        String RIN = theOrder.Bill62__Account__r.RIN__c;
        Integer entityID;
        if(thisOrder.Bill62__Account__r.Entity_ID__c != null){
            entityId = integer.valueof(thisOrder.Bill62__Account__r.Entity_ID__c);
        }else{
            entityId = 1;
        }
        // Customer and address info
        String address = theCustomer.Address__r.Bill62__Street_1__c;
        String city = theCustomer.Address__r.Bill62__City__c; 
        String state = theCustomer.Address__r.Bill62__State_Province__c;
        String zipCode = theCustomer.Address__r.Bill62__Zip_Postal_Code__c;
        String phoneNum = theCustomer.HomePhone;
        Integer countryId;
        if(theCustomer.Address__r.Country__r.Country_ID__c != null){
            countryId = integer.valueof(theCustomer.Address__r.Country__r.Country_ID__c);
        }else{
            countryId = 1;
        }
        Integer custId = integer.valueof(theCustomer.Customer_ID__c);
        // eventually an order will be tied to payment
        String orderId;
        if ( thisOrder.Order_IDtxt__c != null ){
            orderId = thisOrder.Order_IDtxt__c;
        } else {
            orderId = '1';
        }
        Integer userId;
        if ( currentUser.TC_User_ID__c != null ){
            userId = integer.valueof(currentUser.TC_User_ID__c);
        } else {
            userId = 1;
        }
        Integer payReason;
        if(payReasonID != null) payReason = Integer.valueOf(payReasonID);
        if(!Test.isRunningTest()){
            p = C625LinxAPIHelper.postPaymentProcessImmediate(entityID,orderID,payReason,intPayMethodId,bankNum,routingNum,cardNum,expDate,
                FName,LName,address,city,state,zipCode,countryId,countryId,pmtAmount,phoneNum,refID,AuthCode,p.id, p);
        }   
        return p;
    }
    
    public Integer findPayMethodID(String s){
        Integer i;
        if(s == 'Visa'){
            i = 1;
        }else if(s == 'MasterCard'){
            i = 2;
        }else if(s == 'American Express'){
            i = 3;
        }else if(s == 'Discover'){
            i = 4;
        }
        return i;
    }
    
    
    //wrapper for Wellness Orders
    public class paymentWrapper{
        public Bill62__Order__c theOrder{get;set;}
        public String productName{get;set;}
        public Decimal amount{get;set;}
        public Decimal paymentReasonAmount{get;set;}
        public Integer quantity{get;set;}
        //public boolean hasOverage{get;set;}
    }
}