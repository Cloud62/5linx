/*
@Name            : FiveLinxGenerateWellnessPayments
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Batch class to generate Wellness Payments, because multiple Wellness Orders go under the same Payment sometimes
*/



global class FiveLinxGenerateWellnessPayments implements Database.Batchable<sObject>,Schedulable,Database.Stateful,Database.AllowsCallouts {
    global Set<String> nopsProcessed{get;set;}
    global string error{get;set;}

    global FiveLinxGenerateWellnessPayments() {
        
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        nopsProcessed = new Set<String>();
        String query = 
            'SELECT Id, Bill62__Customer__c, Order__r.Bill62__End_Date__c, Order__c, ' +
            'Order__r.Has_Current_Payment__c, Bill62__Product__c, Order__r.Bill62__Account__r.status_id__c,' +
            'Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Name, '+
            'Order__r.NOP_ID__c, Bill62__Product__r.Changes_To__c, Bill62__Product__r.Bill62__Payment_Gateway__c '+
            'FROM Bill62__Subscription__c WHERE  Order__r.Bill62__End_Date__c != null ' +
            'AND Status_Id__c IN (\'10\', \'70\') AND (Order__r.Has_Current_Payment__c = false ' +
            'OR Order__r.Has_Current_Payment__c = null) AND Bill62__Product__r.Bill62__Auto_Renew__c = TRUE ' +
            //include ONLY Wellness Orders
            'AND Bill62__Product__r.Product_Category__r.Name IN (\'Wellness Reship\', \'Coffee Recurring\', '+
            ' \'Nutrition\', \'Nutrition Recurring\', \'Premium Bundle\') ORDER BY Order__r.NOP_ID__c';
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext ctx) {
        FiveLinxGenerateWellnessPayments batch1 = new FiveLinxGenerateWellnessPayments();
        ID batchprocessid = Database.executeBatch(batch1,50);
    }


    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            C62Utilities.cSetExecuted.add('paymentBatch'); //don't fire triggers
            List<Bill62__Subscription__c> subList = (List<Bill62__Subscription__c>)scope;
            List<Bill62__Payment__c> insertPayList = new List<Bill62__Payment__c>();
            List<Bill62__Order__c> updateOrders = new List<Bill62__Order__c>();
            
            //this block: need to get the payment reason so we can set the Amount
            Set<String> prodIds = new Set<String>();
            Set<String> orderIds = new Set<String>();
            Set<String> updateOrdersSet = new Set<String>();
            Set<String> NOPIDs = new Set<String>();
            Map<String, List<Bill62__Subscription__c>> nopMap = new Map<String, List<Bill62__Subscription__c>>();
            
            //gotta mutate the products first
            subList = FiveLinxCalculatePaymentDate.mutateProducts(subList);
            update subList;
            
            for(Bill62__Subscription__c sub: subList){

                if(sub.Order__r.NOP_ID__c != null && !nopsProcessed.contains(sub.Order__r.NOP_ID__c)){
                    NOPIDs.add(sub.Order__r.NOP_ID__c);
                    nopMap.put(sub.Order__r.NOP_ID__c, new List<Bill62__Subscription__c>());
                }
            }
            
            //this block: organize by NOP, to make sure that order lines under the same NOP
            //don't get split into multiple payments
            List<Bill62__Subscription__c> secondSubList = [SELECT Id, Bill62__Customer__c, 
                Order__r.Bill62__End_Date__c, Order__c, Order__r.Has_Current_Payment__c, 
                Bill62__Product__c, Order__r.Bill62__Account__r.status_id__c, Order__r.NOP_ID__c,
                Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Name, Bill62__Product__r.Changes_To__c,
                Bill62__Product__r.Bill62__Payment_Gateway__c
                FROM Bill62__Subscription__c WHERE Order__r.Has_Current_Payment__c = false AND
                Order__r.NOP_ID__c IN :NOPIDs];
                
            //mutate products that do:
            secondSubList = FiveLinxCalculatePaymentDate.mutateProducts(secondSubList);
            update secondSubList;
            
            secondSubList = [SELECT Id, Bill62__Customer__c, Order__r.Payment_Method__c, 
                Order__r.Bill62__End_Date__c, Order__c, Order__r.Has_Current_Payment__c, 
                Bill62__Product__c, Order__r.Bill62__Account__r.status_id__c, Order__r.NOP_ID__c,
                Bill62__Product__r.Product_Category__r.Name, Bill62__Product__r.Name, Bill62__Product__r.Changes_To__c,
                Bill62__Product__r.Bill62__Payment_Gateway__c
                FROM Bill62__Subscription__c WHERE Order__r.Has_Current_Payment__c = false AND
                Order__r.NOP_ID__c IN :NOPIDs];
            
            for(Bill62__Subscription__c sub: secondSubList){
                if(sub.Bill62__Product__c != null) prodIds.add(sub.Bill62__Product__c);
                if(sub.Order__c != null) orderIds.add(sub.Order__c);
            }
                
            Map<Id, Bill62__Order__c> orderMap = new Map<Id, Bill62__Order__c>([SELECT Id, 
                Has_Current_Payment__c FROM Bill62__Order__c WHERE ID IN :orderIds]);
            List<Payment_Reason__c> payReasonList = [SELECT ID, Amount__c, Months__c, Product__c, Currency_ISO_Code__c FROM 
                Payment_Reason__c WHERE Product__c IN :prodIds];
            Map<String, Payment_Reason__c> productToPayReasonMap = new Map<String, Payment_Reason__c>();
            for(Payment_Reason__c pr: payReasonList){
                if(pr.Product__c != null) productToPayReasonMap.put(pr.Product__c, pr);
            }
            
            for(Bill62__Subscription__c sl : secondSubList){
                List<Bill62__Subscription__c> temp = nopMap.get(sl.Order__r.NOP_ID__c);
                temp.add(sl);
                nopMap.put(sl.Order__r.NOP_ID__c, temp);
            }
            
            for(String nop:nopMap.keyset()){
                List<Bill62__Subscription__c> nopList = nopMap.get(nop);
                if(nopList.size() == 0 || nopsProcessed.contains(nop)) continue;
                //make the actual Payment object
                Bill62__Payment__c newPay = new Bill62__Payment__c();
                newPay.Bill62__Amount__c = 0;
                newPay.NOP_ID__c = nop;
                newPay.Bill62__Customer__c = nopList[0].Bill62__Customer__c;
                newPay.Bill62__Status__c = 'Queued';
                
                //remove after testing:
                newPay.Fake_Payment__c = true;

                for(Bill62__Subscription__c sl : nopList){
                    if(sl.Order__r.Has_Current_Payment__c) continue;
                    
                        //this block: only create payments if the Order's Product has a Payment Reason
                        Payment_Reason__c payReason;
                        if(sl.Bill62__Product__c != null){
                            if(productToPayReasonMap.containsKey(sl.Bill62__Product__c)){
                                payReason = productToPayReasonMap.get(sl.Bill62__Product__c);
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                        
                        //it doesn't matter what the order and order item are, TC will calulate tax and
                        //shipping for the entire NOP order if you just put any one in
                        if(sl.Bill62__Product__r.Bill62__Payment_Gateway__c != null)
                            newPay.Bill62__Payment_Gateway__c = sl.Bill62__Product__r.Bill62__Payment_Gateway__c;
                        newPay.Bill62__Order__c = sl.Order__c;
                        newPay.Bill62__Subscription__c = sl.Id;
                        newPay.Bill62__Amount__c += payReason.Amount__c;
                        newPay.Bill62__Currency_Code__c = payReason.Currency_ISO_Code__c;
                        newPay.Bill62__Payment_Date__c = FiveLinxCalculatePaymentDate.calculatePaymentDate(newPay, sl, 0);
                        newPay.Bill62__Payment_Method__c = sl.Order__r.Payment_Method__c;
                        
                        if(newPay.Bill62__Payment_Date__c == System.Date.today()) {
                            Bill62__Order__c theOrder = orderMap.get(sl.Order__c);
                            theOrder.Has_Current_Payment__c = true;
                            orderMap.put(theOrder.Id, theOrder);
                            updateOrdersSet.add(theOrder.Id);
                        }
                    
                    }//end of sl FOR
                    if(newPay.Bill62__Payment_Date__c == System.Date.today()) {
                            insertPayList.add(newPay);
                    }
                    nopsProcessed.add(nop);
                }//end of NOP FOR
                for(String s: updateOrdersSet){
                    updateOrders.add(orderMap.get(s));
                }
                insert insertPayList;
                update updateOrders;
            } catch(Exception e) {
                error = e.getMessage()+' line '+e.getLineNumber() +'    '+e.getStackTraceString();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {'rich@cloud62.com'};
                mail.setToAddresses(toAddresses);
                mail.setSubject('GenerateWellnessPayments Exception!');
                mail.setPlainTextBody(error);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    

    global void finish(Database.BatchableContext BC) {
        /*String a = '';
        for(String s: nopsProcessed){
            a += s+' ';
        }
        if(error != null){
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {'rich@cloud62.com'};
                mail.setToAddresses(toAddresses);
                mail.setSubject('GenerateWellnessPayments Exception!');
                mail.setPlainTextBody(error);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {'rich@cloud62.com'};
                mail.setToAddresses(toAddresses);
                mail.setSubject('Todays NOPs');
                mail.setPlainTextBody(a);
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
        String cronId = System.ScheduleBatch(new FiveLinxBatchProcessPayments(),'FiveLinxBatchProcessPayments'+Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day(),2,16);
    }

}