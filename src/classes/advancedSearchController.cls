/*
@Name            : advancedSearchController
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Controller for Advanced Search page
*/


public with sharing class advancedSearchController {
    public String page{get;set;}
    public String repRIN{get;set;}
    public String customerName {get;set;}
    public String businessName {get;set;}
    public String customerEmail {get;set;}
    public String customerHomePhone {get;set;}
    public String serviceNumber {get;set;}
    public String repHomePhone {get;set;}
    public String GUID {get;set;}
    public String orderId {get;set;}
    public String itemId {get;set;}
    public String customerId {get;set;}
    public String customerStreet {get;set;}
    public String customerCity {get;set;}
    public String customerState {get;set;}
    public String customerZip {get;set;}
    public String orderStatus {get;set;}
    public String paymentID {get;set;}
    public String NOPId {get;set;}
    public String productType {get;set;}
    public List<SelectOption> prodTypes{get;set;}
    public String carrier {get;set;}
    public List<SelectOption> carriers{get;set;}
    public String vendor {get;set;}
    
    //case search fields
    public string cNumber {get;set;}
    public string cCustName {get;set;}
    public string cRepRIN {get;set;}
    public string cUserSearch {get;set;}
    
    public Map<Id,RecordType> rtMap {get;set;}
    public Account dummyAccount{get;set;}
    public Bill62__Address__c dummyAddress{get;set;}
    public List<Bill62__Order__c> orderList{get;set;}
    public List<Account> repList{get;set;}
    public List<Case> caseList{get;set;}
    
    //rep search fields
    public String rRIN {get;set;}
    public String rFirstName {get;set;}
    public String rLastName {get;set;}
    public String rCompany {get;set;}
    public String rStreet {get;set;}
    public String rCity {get;set;}
    public String rState {get;set;}
    public String rZIP {get;set;}
    public String rEmail {get;set;}
    public String rHomePhone {get;set;}
    public String rMobilePhone {get;set;}
    public String rStartDate {get;set;}
    public String rStatus {get;set;}
    public String rPosition {get;set;}
    public String rSSN {get;set;}

    public advancedSearchController(){
        prodTypes = new List<SelectOption>();
        carriers = new List<SelectOption>();
        carriers.add(new SelectOption('', '-- None --'));
        List<Carrier__c> carrierList = [Select Id, Name FROM Carrier__c ORDER BY Name];
        for(Carrier__c c:carrierList){
            carriers.add(new SelectOption(c.Name, c.Name));
        }
        List<Product_Type__c> ptList = [SELECT Id, Name FROM Product_Type__c ORDER BY Name];
        prodTypes.add(new SelectOption('', '-- None --'));
        for(Product_Type__c pt: ptList){
            prodTypes.add(new SelectOption(pt.Name, pt.Name));
        }
        page = 'start';
        orderList = new List<Bill62__Order__c>();
        repList = new List<Account>();
        dummyAddress = new Bill62__Address__c();
        dummyAccount = new Account();
        rtMap = New Map<Id,RecordType>([SELECT Id, Name FROM RecordType]);
        
    }
    
    public User getUser(){
        return [SELECT Id FROM User LIMIT 1];
    }
    
    public pageReference startSearchReps(){
        page = 'searchReps';
        return null;
    }
    
    public pageReference startSearchOrders(){
        page = 'searchOrders';
        return null;
    }
    
    public pageReference startSearchCases(){
        page = 'searchCases';
        return null;
    }
    
    public pageReference goBack(){
        if(page == 'searchOrders') page = 'start';
        if(page == 'searchReps') page = 'start';
        if(page == 'orderResults') page = 'searchOrders';
        if(page == 'repResults') page = 'searchReps';
        if(page == 'caseResults') page = 'searchCases';
        if(page == 'searchCases') page = 'start';
        return null;
    }
    
    public pageReference getSearchOrdersResults(){
        String query = 'SELECT Id, Name, Order_IDtxt__c, Bill62__Account__r.RIN__c, Bill62__Customer__r.Business_Name__c, '+
        'Bill62__Customer__r.Name, Bill62__Customer__r.Email, Bill62__Customer__r.HomePhone, Bill62__Account__r.Home_Phone__c, '+
        'Bill62__Account__r.Login_GUID__c, Bill62__Customer__r.Customer_ID__c, Bill62__Customer__r.MailingStreet, Order_Date__c,' +
        'Bill62__Customer__r.MailingCity, Bill62__Customer__r.MailingState, Bill62__Customer__r.MailingPostalCode, ' +
        'Product_Type__c, Carrier__r.Name, Bill62__Account__r.Name, Bill62__Account__r.Id, Count_Factor__c, Bill62__Customer__r.Id '+
        'FROM Bill62__Order__c WHERE Order_ID__c != null ';
        if(orderID != null && orderID != ''){
            query += ' AND Order_IDtxt__c LIKE \'%'+orderID+'%\'';
        }
        if(repRIN != null && repRIN != ''){
            query += ' AND Bill62__Account__r.RIN__c LIKE \'%'+repRIN+'%\'';
        }
        if(businessName != null && businessName != ''){
            query += ' AND Bill62__Customer__r.Business_Name__c LIKE \'%'+businessName+'%\'';
        }
        if(customerName != null && customerName != ''){
            query += ' AND Bill62__Customer__r.Name LIKE \'%'+customerName+'%\'';
        }
        if(customerEmail != null && customerEmail != ''){
            query += ' AND Bill62__Customer__r.Email LIKE \'%'+customerEmail+'%\'';
        }
        if(customerHomePhone != null && customerHomePhone != ''){
            query += ' AND Bill62__Customer__r.Bill62__Home_Phone__c = \''+customerHomePhone+'\'';
        }
        if(repHomePhone != null && repHomePhone != ''){
            query += ' AND Bill62__Account__r.Home_Phone__c = \''+repHomePhone+'\'';
        }
        if(GUID != null && GUID != ''){
            query += ' AND Bill62__Account__r.Login_GUID__c LIKE \'%'+GUID+'%\'';
        }
        if(customerID != null && customerID != ''){
            query += ' AND Bill62__Customer__r.Customer_ID__c = \''+customerID+'\'';
        }
        if(customerStreet != null && customerStreet != ''){
            query += ' AND Bill62__Customer__r.MailingStreet LIKE \'%'+customerStreet+'%\'';
        }
        if(customerCity != null && customerCity != ''){
            query += ' AND Bill62__Customer__r.MailingCity LIKE \'%'+customerCity+'%\'';
        }
        if(customerState != null && customerState != ''){
            query += ' AND Bill62__Customer__r.MailingState LIKE \'%'+customerState+'%\'';
        }
        if(customerZip != null && customerZip != ''){
            query += ' AND Bill62__Customer__r.MailingPostalCode LIKE \'%'+customerZip+'%\'';
        }
        if(productType != null && productType != ''){
            query += ' AND Product_Type__r.Name LIKE \'%'+productType+'%\'';
        }
        if(carrier != null && carrier != ''){
            query += ' AND Carrier__r.Name LIKE \'%'+carrier+'%\'';
        }
        
        query += ' LIMIT 100';
        system.debug(query);
        orderList = Database.query(query);
        page = 'orderResults';
        return null;
    }
    
    
    public pageReference getSearchRepsResults(){
        String query = 'SELECT ID, Name, RIN__C, First_Name__c, Last_Name__c, Business_Name__c,'+
            ' Address_ID__r.Bill62__Street_1__c, Address_ID__r.Bill62__City__c, Address_ID__r.Id,'+
            ' Address_ID__r.Bill62__State_Province__c, Address_ID__r.Bill62__Zip_Postal_Code__c, Address_ID__r.Name,'+
            ' EMail__c, Home_Phone__c, Mobile_Phone__c, Start_Date__c, Status_ID__c, Status__c , Position__r.Position_Code__c'+
            ' FROM Account WHERE Entity_ID__c != null';

        if(rRIN != null && rRIN != ''){
            
            query += ' AND RIN__c LIKE \'%'+rRIN+'%\'';
        }
        if(rFirstName != null && rFirstName != ''){
            query += ' AND First_Name__c LIKE \'%'+rFirstName+'%\'';
        }
        if(rLastName != null && rLastName != ''){
            query += ' AND Last_Name__c LIKE \'%'+rLastName+'%\'';
        }
        if(rCompany != null && rCompany != ''){
            query += ' AND Business_Name__c LIKE \'%'+rCompany+'%\'';
        }
        if(rStreet != null && rStreet != ''){
            query += ' AND Address_ID__r.Bill62__Street_1__c LIKE \'%'+rStreet+'%\'';
        }
        if(rEmail != null && rEmail != ''){
            query += ' AND EMail__c LIKE \'%'+rEmail+'%\'';
        }
        if(rHomePhone != null && rHomePhone != ''){
            query += ' AND Home_Phone__c = \''+rHomePhone+'\'';
        }
        if(rCity != null && rCity != ''){
            query += ' AND Address_ID__r.Bill62__City__c LIKE \'%'+rCity+'%\'';
        }
        if(dummyAddress.Bill62__State_Province__c != null && dummyAddress.Bill62__State_Province__c != ''){
            query += ' AND Address_ID__r.Bill62__State_Province__c = \''+dummyAddress.Bill62__State_Province__c+'\'';
        }
        if(rZIP != null && rZIP != ''){
            query += ' AND Address_ID__r.Bill62__Zip_Postal_Code__c LIKE \'%'+rZIP+'%\'';
        }
        if(rMobilePhone != null && rMobilePhone != ''){
            query += ' AND Mobile_Phone__c = \''+rMobilePhone+'\'';
        }
        if(dummyAccount.Start_Date__c != null){
            query += ' AND Start_Date__c = '+string.valueof(dummyAccount.Start_Date__c).substring(0,10);
        }
        if(dummyAccount.Status__c != null && dummyAccount.Status__c != ''){
            query += ' AND Status__c LIKE \''+dummyAccount.Status__c+'\'';
        } 
        if(rPosition != null && rPosition != ''){
            query += ' AND Position__r.Position_Code__c LIKE \'%'+rPosition+'%\'';
        }
        if(rSSN != null && rSSN != ''){
            query += ' AND SSN__c LIKE \'%'+rSSN+'%\'';
        }

        query += ' LIMIT 100';
        system.debug(query);
        repList = Database.query(query);
        page = 'repResults';
        return null;
    }
    
     public pageReference getSearchCasesResults(){
        String query = 'SELECT ID, CaseNumber, RecordTypeId, RIN__C, Status, Contact.Id, Contact.Name, CreatedBy.Name'+
            ' FROM Case WHERE CaseNumber != null';

        if(cNumber != null && cNumber != ''){
            
            query += ' AND CaseNumber LIKE \'%'+cNumber+'%\'';
        }
        if(cCustName != null && cCustName != ''){
            query += ' AND Customer_Name__c LIKE \'%'+cCustName+'%\'';
        }
        if(cRepRIN != null && cRepRIN != ''){
            query += ' AND RIN__c LIKE \'%'+cRepRIN+'%\'';
        }
        if(cUserSearch != null && cUserSearch != ''){
            query += ' AND CreatedBy.name LIKE \'%'+cUserSearch+'%\'';
        }

        query += ' LIMIT 100';
        system.debug(query);
        caseList = Database.query(query);
        page = 'caseResults';
        return null;
    }
}