public with sharing class SendNotes_TestMethod{

    public static testmethod void SendNotes_TestMethod() {

        // Adding test methods for SendNote class
        Lead[] lead = [select id, Lead_ID__c from Lead where name='SFTester Tester'];

        List<String> Ids = new List<String>();
        Ids.add(lead[0].Lead_ID__c);
        
        List<String> Notes = new List<String>();
        Notes.add('NoteTest');
        
        List<String> Sfids = new List<String>();
        Sfids.add(lead[0].ID);
              
        SendNotes.sendNotes(Ids, Notes, Sfids); //  Call Function of Original class
        }
    }