/*
@Name            : C62PaymentMethodIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to handle Payment Method integration to 5Linx TC
*/


trigger C62PaymentMethodIntegration on Bill62__Payment_Method__c (after update) {
    if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
    
    Set<String> paymethIDSet = new Set<String>();
    for(Bill62__Payment_Method__c paymeth: trigger.new){
        paymethIDSet.add(paymeth.Id);
    }
    
    List<Bill62__Order__c> oList = [SELECT ID, Order_Idtxt__c, Payment_Method__r.Bill62__Card_Number__c, Payment_Method__r.Name,
        Payment_Method__r.Bill62__Billing_Address__r.Bill62__City__c, Payment_Method__r.Bill62__Billing_Address__r.Bill62__Street_1__c,
        Payment_Method__r.Bill62__Billing_Address__r.Bill62__State_Province__c, 
        Payment_Method__r.Bill62__Billing_Address__r.Bill62__Zip_Postal_Code__c,
        Payment_Method__r.Bill62__Expiration_Date__c
        FROM Bill62__Order__c WHERE Payment_Method__c IN :paymethIDSet];
        
    Set<ID> payMethIds = new Set<ID>();
    for(Bill62__Order__c o:oList){
        if(payMethIds.contains(o.Payment_Method__c)) continue;
        payMethIds.add(o.Payment_Method__c);
        String address = o.Payment_Method__r.Bill62__Billing_Address__r.Bill62__Street_1__c +' '+
        o.Payment_Method__r.Bill62__Billing_Address__r.Bill62__City__c+', '+
        o.Payment_Method__r.Bill62__Billing_Address__r.Bill62__State_Province__c;
        String expr;
        Datetime temp = system.datetime.newInstance(o.Payment_Method__r.Bill62__Expiration_Date__c.year(),
        o.Payment_Method__r.Bill62__Expiration_Date__c.month(),
        o.Payment_Method__r.Bill62__Expiration_Date__c.day(),
        0,0,0);
        expr = temp.format('MMYY');
        system.debug('expr: '+expr);
        
        if(!Test.isRunningTest())
            C625LinxAPIHelper.UpdateCCOnOrder(o.Order_Idtxt__c, o.Payment_Method__r.Name, address, 
                o.Payment_Method__r.Bill62__Billing_Address__r.Bill62__Zip_Postal_Code__c, o.Payment_Method__r.Bill62__Card_Number__c, 
                expr);
    }
}