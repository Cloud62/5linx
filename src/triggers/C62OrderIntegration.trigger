/*
@Name            : C62OrderIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to handle Order integration to 5Linx TC
*/


trigger C62OrderIntegration on Bill62__Order__c (after insert, after update) {
   if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
   if(C62Utilities.cSetExecuted.contains('dontFireTrigger') || C62Utilities.cSetExecuted.contains('paymentBatch')) return;
   Boolean updateAPI = false;
   Set<Id> orderSet = new Set<Id>();
    for (Bill62__Order__c s : Trigger.new){
        orderSet.add(s.Id);
        if (trigger.IsUpdate){    
        if (s.Order_ID__c != System.Trigger.oldMap.get(s.Id).Order_ID__c){
            updateAPI = true;
        } else if (s.Bill62__Account__c != System.Trigger.oldMap.get(s.Id).Bill62__Account__c){
            updateAPI = true;
        } else if (s.Bill62__Customer__c != System.Trigger.oldMap.get(s.Id).Bill62__Customer__c){
            updateAPI = true;
        } else if (s.Carrier__c != System.Trigger.oldMap.get(s.Id).Carrier__c){
            updateAPI = true;
        } else if (s.Product_Type__c != System.Trigger.oldMap.get(s.Id).Product_Type__c){
            updateAPI = true;
        } else if (s.Order_Date__c != System.Trigger.oldMap.get(s.Id).Order_Date__c){
            updateAPI = true;
        } else if (s.Bill62__End_Date__c != System.Trigger.oldMap.get(s.Id).Bill62__End_Date__c){
            updateAPI = true;
        } else if (s.Is_Dirty__c != System.Trigger.oldMap.get(s.Id).Is_Dirty__c){
            updateAPI = true;
        }
    }
    }
    Map<Id,Bill62__Order__c> orderMap = new Map<Id,Bill62__Order__c>([SELECT Id, Bill62__Account__r.Entity_ID__c, Bill62__Customer__r.Customer_ID__c, Product_Type__r.External_ID__c, Product_Type__c,
                                                                      Carrier__r.Carrier_ID__c, Order_Date__c, Bill62__End_Date__c, Is_Dirty__c, Order_ID__c, TC_Integration_Error__r.Call_Type__c, 
                                                                      TC_Integration_Error__c, Shipping_Address__r.Address_ID__c
                                                                      FROM Bill62__Order__c WHERE ID IN: orderSet]);
    for (Bill62__Order__c s1 : Trigger.new){
        Bill62__Order__c s = orderMap.get(s1.Id);
        
        Integer orderID, entityID, custID, carrierID, productType, intShipAddress;
        String orderDate, endDate;
        if(s.Order_ID__c != null)                        orderID = integer.valueof(s.Order_ID__c);
        if(s.Bill62__Account__r.Entity_ID__c != null)    entityID = Integer.valueof(s.Bill62__Account__r.Entity_ID__c);
        if(s.Bill62__Customer__r.Customer_ID__c != null) custID = Integer.valueof(s.Bill62__Customer__r.Customer_ID__c);
        if(s.Carrier__r.Carrier_ID__c != null)           carrierID = Integer.valueof(s.Carrier__r.Carrier_ID__c);
        if(s.Product_Type__r.External_ID__c != null)     productType = Integer.valueof(s.Product_Type__r.External_ID__c);
        if(s.Order_Date__c != null)                      orderDate = s.Order_Date__c.format();//string.valueof(s.Order_Date__c);
        if(s.Bill62__End_Date__c != null)                endDate = s.Bill62__End_Date__c.format();//string.valueof(s.Bill62__End_Date__c); 
        if (s.Shipping_Address__c != null)               intShipAddress = integer.valueof(s.Shipping_Address__r.Address_ID__c);
        
        if (Trigger.isInsert  || (Trigger.isUpdate && s.TC_Integration_Error__c != null && s.TC_Integration_Error__r.Call_Type__c == 'Create Order')){   
            if (entityID != null && custID != null){
                C625LinxAPIHelper.CreateOrder(entityID,custID,productType,carrierID,orderDate,endDate,s.Is_Dirty__c,string.valueof(s.Id));
            } else {
                system.debug('Insert Failed.  Fields Required: Entity Id, Customer Id');
                s1.addError('Fields Required: Entity Id, Customer Id');
            }
        } else if(Trigger.isUpdate && C625LinxAPIHelper.isInsert == false && updateAPI == true) {
            if (entityID != null && custID != null){
                C625LinxAPIHelper.UpdateOrder(orderId,entityID,custID,productType,carrierID,orderDate,endDate,s.Is_Dirty__c, intShipAddress);
            } else {
                system.debug('Update Failed.  Fields Required: Entity Id, Customer Id, Order Id');
                s1.addError('Fields Required: Entity Id, Customer Id, Order Id');
            }
        }
    }
}