/*
@Name            : UpdateRepStatus
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to convert status values from TC numbers to text, and vice versa
*/


trigger UpdateRepStatus on Account (before update) {

    Set<String> repIds = new Set<String>();
    
    for (Account a: Trigger.New){
        repIds.add(a.Id);
        //is direct deposit stuff:
        Boolean oldDD = a.Is_Direct_Deposit__c;
        if(a.Pay_Method__c != null && a.Pay_Method__c != system.Trigger.OldMap.get(a.Id).Pay_Method__c){
            if(a.Pay_Method__c == 'ACH') a.Is_Direct_Deposit__c = true;
            else if(a.Pay_Method__c == 'Pay Card') a.Is_Direct_Deposit__c = false;
        }
        if(a.Is_Direct_Deposit__c != null && oldDD != System.Trigger.OldMap.get(a.Id).Is_Direct_Deposit__c){
            if(a.Is_Direct_Deposit__c) a.Pay_Method__c = 'ACH';
            else a.Pay_Method__c = 'Pay Card';
        }
        
        
        
        if (a.Status_ID__c != null && a.Status_ID__c != System.Trigger.OldMap.get(a.Id).Status_ID__c){
            if (a.Status_ID__c == '1') a.Status__c = 'Active';
            if (a.Status_ID__c == '2') a.Status__c = 'Inactive';
            if (a.Status_ID__c == '3') a.Status__c = 'Suspended';
            if (a.Status_ID__c == '4') a.Status__c = 'Hold No Cab';
            if (a.Status_ID__c == '5') a.Status__c = 'Newly Expired';
            if (a.Status_ID__c == '6') a.Status__c = 'Expired';
            if (a.Status_ID__c == '7') a.Status__c = 'Deactivated';
            if (a.Status_ID__c == '8') a.Status__c = 'Cancelled';
            if (a.Status_ID__c == '9') a.Status__c = 'Deleted';
            if (a.Status_ID__c == '10') a.Status__c = 'Chargeback';
            if (a.Status_ID__c == '11') a.Status__c = 'Hold Pay Cab';
        }
        if (a.Status__c != System.Trigger.OldMap.get(a.Id).Status__c){
            if (a.Status__c == 'Active') a.Status_ID__c = '1';
            if (a.Status__c == 'Inactive') a.Status_ID__c = '2';
            if (a.Status__c == 'Suspended') a.Status_ID__c = '3';
            if (a.Status__c == 'Hold No Cab') a.Status_ID__c = '4';
            if (a.Status__c == 'Newly Expired') a.Status_ID__c = '5';
            if (a.Status__c == 'Expired') a.Status_ID__c = '6';
            if (a.Status__c == 'Deactivated') a.Status_ID__c = '7';
            if (a.Status__c == 'Cancelled') a.Status_ID__c = '8';
            if (a.Status__c == 'Deleted') a.Status_ID__c = '9';
            if (a.Status__c == 'Chargeback') a.Status_ID__c = '10';
            if (a.Status__c == 'Hold Pay Cab') a.Status_ID__c = '11';
        }
    }
    
    //only have this validation for the UI, not for the API User:
    if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
    if(C62Utilities.cSetExecuted.contains('dontFireTrigger')) return;
    Map<Id, Account> repList = new Map<Id, Account>([SELECT ID, BillingState, Address_ID__r.State__r.Abbreviation__c,
        Address_ID__c, Address_ID__r.State__c
        FROM Account WHERE ID IN :repIds]);
    
    for (Account a: Trigger.New){
        if(a.BillingState != null){
            if(a.BillingState == 'MT' || a.BillingState == 'ND' || a.BillingState == 'SD'){
                a.addError('Representatives cannot live in Montana, North Dakota, or South Dakota.');
            }
        }
        
        if(a.Birth_Date__c != null){
            if(system.date.TODAY().year() - a.Birth_Date__c.year() < 18){
                a.addError('Representatives must be at least 18 years of age.');
            }
        }
        
        //not sure why this map thing is here:
        if(repList.containsKey(a.Id)){
            if(repList.get(a.Id).Address_ID__c != null){
                if(repList.get(a.Id).Address_ID__r.State__c != null){
                    if(repList.get(a.Id).Address_Id__r.State__r.Abbreviation__c == 'MT' ||
                    repList.get(a.Id).Address_Id__r.State__r.Abbreviation__c == 'ND' ||
                    repList.get(a.Id).Address_Id__r.State__r.Abbreviation__c == 'SD'){
                        a.addError('Representatives cannot live in Montana, North Dakota, or South Dakota.');
                    }
                }
            }
        }
    }
}