/*
@Name            : C62CustomerIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to handle Customer integration to 5Linx TC
*/


trigger C62CustomerIntegration on Contact (after insert, after update) {
    if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
    if(C62Utilities.cSetExecuted.contains('dontFireTrigger') || C62Utilities.cSetExecuted.contains('paymentBatch')) return;
    Set<Id> contactSet = new Set<Id>();
    Set<Id> contactAddys = new Set<Id>();
    Boolean updateAPI = false;
    for (Contact c : Trigger.new){
        contactSet.add(c.Id);
        contactAddys.add(c.Address__c);
        if (trigger.IsUpdate){ 
            // checks if API needs to be updated 
            if (c.Bill62__Last_Name__c != System.Trigger.oldMap.get(c.Id).Bill62__Last_Name__c){
                updateAPI = true;
            } else if (c.Bill62__First_Name__c != System.Trigger.oldMap.get(c.Id).Bill62__First_Name__c){
                updateAPI = true;
            } else if (c.Middle_Initial__c != System.Trigger.oldMap.get(c.Id).Middle_Initial__c){
                updateAPI = true;
            } else if (c.Bill62__Birthdate__c != System.Trigger.oldMap.get(c.Id).Bill62__Birthdate__c){
                updateAPI = true;
            } else if (c.Bill62__SSN__c != System.Trigger.oldMap.get(c.Id).Bill62__SSN__c){
                updateAPI = true;
            } else if (c.Bill62__Home_Phone__c != System.Trigger.oldMap.get(c.Id).Bill62__Home_Phone__c){
                updateAPI = true;
            } else if (c.Email != System.Trigger.oldMap.get(c.Id).Email){
                updateAPI = true;
            } else if (c.Address__c != System.Trigger.oldMap.get(c.Id).Address__c){
                updateAPI = true;
            }
        }
    }
    Map<Id,Bill62__Address__c> addyMap = New Map<Id,Bill62__Address__c>([SELECT Address_ID__c FROM Bill62__Address__c WHERE Id IN :contactAddys]);
    Map<Id,Contact> addMap = new Map<Id,Contact>([SELECT ID, LastName , FirstName , Middle_Initial__c, Name, Bill62__Birthdate__c, Bill62__Legacy_Customer_Id__c, Business_Name__c,
                                                             Bill62__SSN__c, Bill62__Home_Phone__c, Email, Address__c, Address__r.Address_ID__c, Customer_ID__c, TC_Integration_Error__c, TC_Integration_Error__r.Call_Type__c
                                                             FROM Contact 
                                                             WHERE ID IN: contactSet]);
           
      for (Contact c1 : Trigger.new){
          Contact c = addMap.get(c1.Id);
          if (Trigger.isInsert || (Trigger.isUpdate && c.TC_Integration_Error__c != null && c.TC_Integration_Error__r.Call_Type__c == 'Create Customer')){
               if( c.Bill62__Legacy_Customer_Id__c == null && c.Customer_ID__c == null){
                    if(c.Bill62__Birthdate__c != null && c.Address__c != null && !Test.isRunningTest()){
                      system.debug('Calling: C625LinxAPIHelper.CreateCustomer');
                      C625LinxAPIHelper.CreateCustomer(c.LastName , c.FirstName , c.Middle_Initial__c, 
                                                      c.Business_Name__c, c.Bill62__Birthdate__c, c.Bill62__SSN__c, c.Bill62__Home_Phone__c, 
                                                      c.Email, Integer.valueOf(addyMap.get(c.Address__c).Address_ID__c), String.valueof(c.Id));
                    } else {
                        c1.AddError('Fields Required: Birthdate, Address.');
                    }
                }    
           } else if (Trigger.isUpdate && C625LinxAPIHelper.isInsert == false && updateAPI == true && !Test.isRunningTest()){
                system.debug('Calling: C625LinxAPIHelper.UpdateCustomer');   
                C625LinxAPIHelper.UpdateCustomer(integer.valueof(c.Customer_ID__c), c.LastName , c.FirstName, c.Middle_Initial__c, 
                                                 c.Business_Name__c, c.Bill62__Birthdate__c, c.Bill62__SSN__c, c.Bill62__Home_Phone__c, 
                                                 c.Email, Integer.valueOf(addyMap.get(c.Address__c).Address_ID__c));
                
           }
      }
}