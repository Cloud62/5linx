/*
@Name            : C62Payment
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to handle Payment updating and error checking
*/


trigger C62Payment on Bill62__Payment__c (after update) {
    if(C62Utilities.cSetExecuted.contains('rescheduleBilling')) return;
    if(RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;

        C62Utilities.cSetExecuted.add('rescheduleBilling');
        Set<ID> payIDs = new Set<ID>();
        Set<Id> orderIDs = new Set<ID>();
        Set<ID> orderLineIDs = new Set<ID>();
        Set<String> NOPIDs = new Set<String>();
        
        for(Bill62__Payment__c payInd : Trigger.New){
            payIDs.Add(payInd.ID);
            if(!C62Utilities.cSetExecuted.contains('paymentBatch')){
                if(payInd.Bill62__Amount__c != Trigger.oldMap.get(payInd.Id).Bill62__Amount__c)
                    payInd.addError('You cannot manually edit the Amount on Payments!');
            }
            if(payInd.Bill62__Amount__c != Trigger.oldMap.get(payInd.Id).Bill62__Amount__c && Trigger.oldMap.get(payInd.Id).Bill62__Status__c == 'Paid')
                payInd.addError('You cannot edit the Amount on Paid Payments!');
        }

        //All the below code has been moved to FiveLinxBatchRescheduleBilling
        /* list of payments with status rejected or exception for retry
        List<Bill62__Payment__c> paymentsToProcess = [SELECT ID, Bill62__Payment_Date__c, Bill62__Status__c, Bill62__Order__c,
                                                             Bill62__Subscription__r.Order__r.Bill62__End_Date__c,
                                                             Bill62__Subscription__r.Bill62__Product__r.Product_Category__r.Name,
                                                             Bill62__Subscription__r.Bill62__Product__r.Name,
                                                             Bill62__Order__r.NOP_ID__c
                                                      FROM   Bill62__Payment__c
                                                      WHERE  ID IN :payIDs];
                                                             //AND (Bill62__Status__c = 'Rejected' OR Bill62__Status__c LIKE 'exception' OR Bill62__Status__c = 'Queued Retry' OR Bill62__Status__c = 'Paid')];
        
        //reschedule billing
        for(Bill62__Payment__c payInd : paymentsToProcess){
            if(payInd.Bill62__Status__c != 'Paid'){
                Bill62__Subscription__c sub = payInd.Bill62__Subscription__r;
                payInd.Bill62__Payment_Date__c = FiveLinxCalculatePaymentDate.calculatePaymentDate(payInd, sub);
            } else {
                if(payInd.Bill62__Order__r.NOP_ID__c == null){
                    orderIDs.add(payInd.Bill62__Order__c);
                    orderLineIDs.add(payInd.Bill62__Subscription__c);
                } else {
                    NOPIDs.add(payInd.Bill62__Order__r.NOP_ID__c);
                }
            }
        }
        List<Bill62__Order__c> nopOrders = [SELECT ID FROM Bill62__Order__c WHERE NOP_ID__c IN :NOPIDs];
        for(Bill62__Order__c ord : nopOrders){
            orderIDs.add(ord.Id);
        }
        
        //List<Bill62__Subscription__c> orderLineList = [SELECT ID, Bill62__Product__r.Changes_To__c
        //    FROM Bill62__Subscription__c WHERE Order__c IN :orderIDs];
        //orderLineList = FiveLinxCalculatePaymentDate.mutateProducts(orderLineList); we do this first now, not after processing
        //update orderLineList;
        
        List<Bill62__Order__c> orderList = [SELECT ID, Has_Current_Payment__c, Bill62__End_Date__c 
            FROM Bill62__Order__c WHERE ID IN :orderIDs];
        for(Bill62__Order__c ord: orderList){
            ord.Has_Current_Payment__c = false;
        }
        orderList = FiveLinxCalculatePaymentDate.advanceEndDate(orderList);
        
        try{
            update paymentsToProcess;
            update orderList;
        }catch(Exception e){
            System.Debug('Issue Updating: ' + e);    
        }*/
        
}