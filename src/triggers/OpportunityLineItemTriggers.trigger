trigger OpportunityLineItemTriggers on OpportunityLineItem (after insert, after update, before update) {

	if(Trigger.isBefore && Trigger.isUpdate) {    
	
		Set<String> lineItemIds = new Set<String>();
		for(OpportunityLineItem oli : Trigger.new) lineItemIds.add(oli.Id);
		
		// delete the shipping and one time charge line items  
		// they will be created
		delete [select Id from OpportunityLineItem where Creator__c in :lineItemIds];
		
	}

	if(Trigger.isAfter) {

		List<OpportunityLineItem> newLineItems = new List<OpportunityLineItem>();
	
		List<Id> pbeIds = new List<Id>();
		for(OpportunityLineItem oli : Trigger.new) pbeIds.add(oli.PricebookEntryId);
		Map<Id, PricebookEntry> pbes = new Map<Id, PricebookEntry>([select Id, Product2.Name from PricebookEntry where Id in :pbeIds]);
	
		List<PricebookEntry> fees = [select Id, Product2.Name, Pricebook2Id from PricebookEntry where Product2.Name in ('Shipping Fee', 'One Time Fee')];
		
		PricebookEntry shippingFee, oneTimeFee;
		
		Map<Id, OpportunityLineItem> oliRef = new Map<Id, OpportunityLineItem>([select Id, PricebookEntry.Pricebook2Id from OpportunityLineItem where Id in :Trigger.new]);
		
		for(OpportunityLineItem oli : Trigger.new) {
			
			shippingFee = null;
			oneTimeFee = null;
			
			for(PricebookEntry fee : fees) {
				if(fee.Product2.Name == 'Shipping Fee' && oliRef.get(oli.Id).PricebookEntry.Pricebook2Id == fee.Pricebook2Id) shippingFee = fee;
				if(fee.Product2.Name == 'One Time Fee' && oliRef.get(oli.Id).PricebookEntry.Pricebook2Id == fee.Pricebook2Id) oneTimeFee = fee;		
			}
			
			// create line items for any OLI that has Shipping__c or One_Time_Charge__c value specified
			if(oli.Shipping__c != null && oli.Shipping__c > 0 && shippingFee != null) {
				newLineItems.add(
					new OpportunityLineItem(
						Quantity = 1,
						UnitPrice = oli.Shipping__c,
						Description = 'Shipping Fee: ' + pbes.get(oli.PricebookEntryId).Product2.Name,
						PricebookEntryId = shippingFee.Id,
						OpportunityId = oli.OpportunityId,
						Shipping__c = 0,
						One_Time_Fee__c = 0,
						Creator__c = oli.Id
					)
				);
			}
			if(oli.One_Time_Fee__c != null && oli.One_Time_Fee__c > 0 && oneTimeFee != null) {
				// one time charge OLI
				newLineItems.add(
					new OpportunityLineItem(
						Quantity = 1,
						UnitPrice = oli.One_Time_Fee__c,
						Description = 'One Time Fee: ' + pbes.get(oli.PricebookEntryId).Product2.Name,
						PricebookEntryId = oneTimeFee.Id,
						OpportunityId = oli.OpportunityId,
						Shipping__c = 0,
						One_Time_Fee__c = 0,
						Creator__c = oli.Id
					)
				);
			}
	 	}
	 	
	 	if(newLineItems.size() > 0) insert newLineItems;
 	
 	}

}