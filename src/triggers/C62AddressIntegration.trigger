/*
@Name            : C62AddressIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Address Trigger to integrate changes to 5Linx TC
*/


trigger C62AddressIntegration on Bill62__Address__c (after Update) {
    if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
    if(C62Utilities.cSetExecuted.contains('dontFireTrigger') || C62Utilities.cSetExecuted.contains('paymentBatch')) return;
   
    Set<String> addIDs = new Set<String>();
    
    for(Bill62__Address__c a : Trigger.New){
        addIDs.add(a.Id);
    }
    
    List<Bill62__Address__c> addList = [SELECT Address_ID__c, Bill62__Street_1__c, Bill62__Street_2__c, Bill62__City__c, State_Code__c,
        Bill62__State_Province__c, Bill62__Zip_Postal_Code__c, Country_Code__c
        FROM Bill62__Address__c WHERE ID IN :addIDs];
        
    for(Bill62__Address__c addr : addList){
        C625LinxAPIHelper.UpdateAddress(addr.Address_ID__c, addr.Bill62__Street_1__c, addr.Bill62__Street_2__c, addr.Bill62__City__c, 
            String.valueOf(addr.State_Code__c), addr.Bill62__State_Province__c, addr.Bill62__Zip_Postal_Code__c, addr.Country_Code__c);
    }
}