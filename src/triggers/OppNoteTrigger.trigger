trigger OppNoteTrigger on Note (after insert, after update) {
    List<Note> oppNotes = new List<Note>();
    String oppPrefix = Opportunity.SObjectType.getDescribe().getKeyPrefix();
    
        for(Note n : Trigger.new) {
        if(n.IsPrivate) continue;
        if(String.valueOf(n.ParentId).startsWith(oppPrefix)) oppNotes.add(n);
    }
    
    if(oppNotes.size() <= 0) return;

    // once we have the Opportunity-related notes, we need to get the Lead Id from each Opportunity
    Set<Id> relatedOppIds = new Set<Id>();
    for(Note n : oppNotes) relatedOppIds.add(n.ParentId);
    
    Map<Id, opportunity> relatedOpps = new Map<Id, opportunity>([select Id, Lead_ID__c from opportunity where Id in :relatedOppIds]);
    
    List<String> Ids= new List<String>();
    List<String> Notes= new List<String>();
    List<Id> Sfids= new List<Id>();
    Opportunity relatedOpp;
    
    for(Note n : oppNotes) {
        relatedOpp = relatedOpps.get(n.ParentId);
        Ids.add(relatedOpp.Lead_ID__c);
        Notes.add(n.Body);
        Sfids.add(relatedOpp.Id);  
    }
    
    system.debug('QUEUING NOTE CALLOUT TO 5LINX...' + Ids);
    system.debug('QUEUING NOTE CALLOUT TO 5LINX...' + Notes);
    system.debug('QUEUING NOTE CALLOUT TO 5LINX...' + Sfids);
    
    SendNotes.sendNotes(Ids, Notes, Sfids);
}