/*
@Name            : UpdateOrderName
@Author          : customersuccess@cloud62.com
@Date            : September 26, 2014
@Description     : If a customer's name changes, update the names of their orders.
*/
trigger UpdateOrderName on Contact (after update) {
    Set<Contact> ConSet = New Set<Contact>();
    Set<Id> idSet = New Set<Id>();
    for (Contact c: Trigger.New){
        if ( c.LastName != System.Trigger.oldMap.get(c.Id).LastName || c.FirstName != System.Trigger.oldMap.get(c.Id).FirstName){
            idSet.add(c.Id);
        }       
    }
    
    List<Bill62__Order__c> orderUpdate = [SELECT Id, Name, Bill62__Customer__c, Bill62__Customer__r.Name, Carrier__r.Name, Bill62__Customer__r.Business_Name__c
                                          FROM Bill62__Order__c 
                                          WHERE Bill62__Customer__c IN :idSet];   
    Map<Id, String> OLmap = New Map<Id, String>();
    for (Bill62__Subscription__c ol : [SELECT Bill62__Product__r.Name, Order__c 
                                       FROM Bill62__Subscription__c 
                                       WHERE Order__c IN :orderUpdate]){
        if (!OLMap.containsKey(ol.Order__c)){
            OLMap.put(ol.Order__c, ol.Bill62__Product__r.Name);
        }
    }
    for (Bill62__Order__c o : orderUpdate){
        String newName;
        //TODO: truncate this to the correct size if the new name is too long
        if(o.Bill62__Customer__r.Business_Name__c != null){
            if(o.Bill62__Customer__r.Business_Name__c.Length() > 23){
                newName = o.Bill62__Customer__r.Business_Name__c.Substring(0,23) + ' - ';
            } else {
                newName = o.Bill62__Customer__r.Business_Name__c + ' - ';
            }
        } else {
            if(o.Bill62__Customer__r.Name.Length() > 23){
                newName = o.Bill62__Customer__r.Name.Substring(0,23) + ' - ';
            } else {
                newName = o.Bill62__Customer__r.Name + ' - ';
            }
        }
        if(o.Carrier__c != null){
            if (o.Carrier__r.Name.Length() > 23){
                newName += o.Carrier__r.Name.Substring(0,23) + ' - ';
            } else {
                newName += o.Carrier__r.Name + ' - ';
            }
            if (OLMap.get(o.Id) != null && OLMap.get(o.Id).Length() > 23){
                newName += OLMap.get(o.Id).Substring(0,23);
            } else {
                newName += OLMap.get(o.Id);
            }
        }
        o.Name = newName;
    }           
    update orderUpdate;
}