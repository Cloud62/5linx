/*
@Name            : OrderLineProductType
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to handle Order Line Product type checking, rolling up Points to parent Orders
*/


trigger OrderLineProductType on Bill62__Subscription__c (after insert, after update) {
    // get a list of all updated subscriptions
    Set<Id> orderIDs = New Set<Id>();
    List<Bill62__Order__c> orderList = New List<Bill62__Order__c>();
    for (Bill62__Subscription__c ol : Trigger.New){
        if (!orderIDs.contains(ol.Order__c) && ol.Order__c != null) orderIDs.add(ol.Order__c);
    }
    
    // map order lines to order
    Map<Id,List<Bill62__Subscription__c>> olMap = New Map<Id,List<Bill62__Subscription__c>>();
    for (Bill62__Subscription__c ol: [SELECT Id, Order__c, Order_Item_Points__c, Bill62__Product__r.Product_Type__r.External_ID__c 
                                      FROM Bill62__Subscription__c 
                                      WHERE Order__c IN :orderIDs LIMIT 9950]){
        if(!olMap.containsKey(ol.Order__c)){
            olMap.put(ol.Order__c, New List<Bill62__Subscription__c>());
        }       
        olMap.get(ol.Order__c).add(ol);         
    }
    
    // verify all order lines on order are same product type.
    for (Bill62__Subscription__c ordLine : Trigger.New){
        if(!olMap.containsKey(ordLine.Order__c)) continue;
        Decimal extId = olMap.get(ordLine.Order__c).get(0).Bill62__Product__r.Product_Type__r.External_ID__c;
        for(Bill62__Subscription__c ol : olMap.get(ordLine.Order__c)){
            if (ol.Bill62__Product__r.Product_Type__r.External_ID__c != extId){
                ordLine.addError('Order must have Products of Same Type.');
            }
            
        }
    }
    
    // updates order points total - added 12/19/14
    Map<Id, Bill62__Order__c> orderMap = New Map<Id, Bill62__Order__c>([SELECT Id, Count_Factor__c FROM Bill62__Order__c WHERE Id IN :orderIDs]);
    
    for (Bill62__Order__c o : orderMap.values()){
        Integer totalPoints = 0;
        for (Bill62__Subscription__c ol : olMap.get(o.Id)){
            if (ol.Order_Item_Points__c != null) totalPoints += integer.valueof(ol.Order_Item_Points__c);
            system.debug('totalPts: '+totalPoints);
        }
        system.debug('cfPts: '+totalPoints);
        o.Count_Factor__c = totalPoints;
    }
    if(orderMap.values().size() > 0) update orderMap.values();
}