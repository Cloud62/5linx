/*
@Name            : OtherAssignmentRule 
@Author          : customersuccess@cloud62.com
@Date            : September 8, 2014
@Description     : If none of the case assignment rules apply, assign to the creator's manager.
@Revision History: 
*/

trigger OtherAssignmentRule on Case (before update) {
    
    Map<Case, Id> caseOwnerMgr = New Map<Case, Id>();
    for(case c : Trigger.New){
        if(c.AssignmentRule__c == true){
            caseOwnerMgr.put(c, c.CreatedById);
        }
    }
    
    Map<Id, User> mgrMap = New Map<Id, User>([SELECT ManagerId FROM User where Id IN :caseOwnerMgr.values()]);
    
    for (Case c : caseOwnerMgr.keyset()){
        if( mgrMap.get(c.CreatedById) != null){
            c.OwnerId = mgrMap.get(c.CreatedById).ManagerId;
            c.AssignmentRule__C = false;
        }
        system.debug(c.OwnerId);
    }
}