/*
@Name            : C62OrderItemIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to handle Order Line integration to 5Linx TC
*/


trigger C62OrderItemIntegration on Bill62__Subscription__c (after insert, after update) {
  if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
  if(C62Utilities.cSetExecuted.contains('dontFireTrigger') || C62Utilities.cSetExecuted.contains('paymentBatch')) return;
        Set<Id> orderItemSet = new Set<Id>();
        for (Bill62__Subscription__c s : Trigger.new){
            orderItemSet.add(s.Id);
        }
        Map<Id,Bill62__Subscription__c> orderMap = new Map<Id, Bill62__Subscription__c>([SELECT Id, Order__r.Order_ID__c, Bill62__Product__r.External_ID__c, GUID__c, TC_Integration_Error__c, Count_Factor__c,
                                                                                                Bill62__Product__r.Name, Status_ID__c, Reason__c, Is_Additional_Line__c, Order_Line_ID__c, Retention_Points_Override__c,
                                                                                                Equipment__r.External_ID__c, Equipment__r.Name, Service_Number__c, TC_Integration_Error__r.Call_Type__c, Exempt_from_Charge_Until__c
                                                                                         FROM Bill62__Subscription__c WHERE ID IN: orderItemSet]);
        
        
        for (Bill62__Subscription__c s1 : Trigger.new){
            Bill62__Subscription__c s = orderMap.get(s1.Id);     
            Integer orderID = integer.valueof(s.Order__r.Order_ID__c);
            Integer orderLineId = integer.valueof(s.Order_Line_ID__c);
            Integer productID = integer.valueof(s.Bill62__Product__r.External_ID__c);
            String productName = s.Bill62__Product__r.Name;
            String GUID = s.GUID__c;
            Integer equipNum = 0;
            String equipName = '';
            if(s.Equipment__c != null){
                equipNum = integer.valueof(s.Equipment__r.External_ID__c);
                equipName = s.Equipment__r.Name;                    
            }
            Integer statusId = integer.valueof(s.Status_ID__c);
            
            if(Trigger.isInsert || (Trigger.isUpdate && s.TC_Integration_Error__c != null && s.TC_Integration_Error__r.Call_Type__c == 'Create Order Line')){
                if (orderID != null && productID != null && orderLineId != null){    
                    system.debug('making create order item call');
                    C625LinxAPIHelper.CreateOrderItem(orderID,productID,productName,s.GUID__c,s.Service_Number__c,statusId,s.Reason__c,s.Is_Additional_Line__c,equipNum,equipName,string.valueof(s.Id));  
                } else {
                    s1.addError('Fields Required: Order ID (on Order), Product External ID');
                }
            } else if (Trigger.isUpdate && C625LinxAPIHelper.isInsert == false){
                 system.debug('making an update order item call');
                 C625LinxAPIHelper.UpdateOrderItem(orderLineId, productID, productName,s.GUID__c,s.Service_Number__c, statusId, s.Reason__c, false, integer.valueof(s.Count_Factor__c) , integer.valueof(s.Retention_Points_Override__c), equipNum,equipName, string.valueof(s.Exempt_from_Charge_Until__c));      
            }       
        }
}