/*
@Name            : populateSupplier 
@Author          : customersuccess@cloud62.com
@Date            : September 8, 2014
@Description     : Populates the Supplier field with the Vendor that is linked the the Carrier's foreign code.
@Revision History: 
*/

trigger populateSupplier on Bill62__Order__c (before insert, before update) {
    
    Map<Id, Carrier__c> carMap = New Map<Id, Carrier__c>([SELECT Id, Foreign_Code__c FROM Carrier__c]);
    Map<String, Vendor__c> vendMap = New Map<String,Vendor__c>();
    for (Vendor__c v : [SELECT Vendor_Code__c, Id FROM Vendor__c]){
        vendMap.put(v.Vendor_Code__c, v);
    }
    
    for ( Bill62__Order__c o : Trigger.New){
        if(o.Carrier__c != null){
            o.Supplier__c = vendMap.get(carMap.get(o.Carrier__c).Foreign_Code__c).Id;
        }
    }
    
    if(trigger.isUpdate){
        List<Bill62__Order__c> mutateList = new List<Bill62__Order__c>();
        Set<ID> orderIds = new Set<ID>();
        for(Bill62__Order__c o : Trigger.New){
            if(o.Rollback__c){
                o.Rollback__c = false;
                o.Has_Current_Payment__c = false;
                orderIds.add(o.Id);
                mutateList.add(o);
            }
        }
        mutateList = FiveLinxCalculatePaymentDate.rollbackEndDate(mutateList);
        List<Bill62__Payment__c> p = [SELECT ID FROM Bill62__Payment__c WHERE Bill62__Order__c IN :orderIds];
        delete p;
    }
}