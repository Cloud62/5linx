trigger NoteTriggers on Note (after insert, after update) {
    
    List<Note> leadNotes = new List<Note>();
    
    String leadPrefix = Lead.SObjectType.getDescribe().getKeyPrefix();
    
    for(Note n : Trigger.new) {
        if(n.IsPrivate) continue;
        if(String.valueOf(n.ParentId).startsWith(leadPrefix)) leadNotes.add(n);
    }
    
    if(leadNotes.size() <= 0) return;
    
    // once we have the lead-related notes, we need to get the Lead Id from each lead
    Set<Id> relatedLeadIds = new Set<Id>();
    for(Note n : leadNotes) relatedLeadIds.add(n.ParentId);
    
    Map<Id, Lead> relatedLeads = new Map<Id, Lead>([select Id, Lead_ID__c from Lead where Id in :relatedLeadIds]);
    
    
    List<String> Ids = new List<String>();
    List<String> Notes = new List<String>();
    List<Id> Sfids = new List<Id>();
    Lead relatedLead;
    
    for(Note n : leadNotes) {
        relatedLead = relatedLeads.get(n.ParentId);
        Ids.add(relatedLead.Lead_ID__c);
        Notes.add(n.Body);
        Sfids.add(relatedLead.Id);  
    }
    
    system.debug('QUEUING NOTE CALLOUT TO 5LINX...' + Ids);
    system.debug('QUEUING NOTE CALLOUT TO 5LINX...' + Notes);
    system.debug('QUEUING NOTE CALLOUT TO 5LINX...' + Sfids);
    
    SendNotes.sendNotes(Ids, Notes, Sfids);
    
}