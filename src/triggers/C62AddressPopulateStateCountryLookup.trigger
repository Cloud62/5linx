/*
@Name            : C62AddressPopulateStateCountryLookup 
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to populate State and Country information on change
*/


trigger C62AddressPopulateStateCountryLookup on Bill62__Address__c (before insert, before update) {
    if(C62Utilities.cSetExecuted.contains('dontFireTrigger')) return;
    Map<String,Country__c> CountryMap = New Map<String,Country__c>();
    for (Country__c c : [SELECT ID, Name FROM Country__c]){
        CountryMap.put(c.Name, c);
    }
    Map<String,State__c> StateMap = New Map<String,State__c>();
    for (State__c s : [SELECT ID, Name, Abbreviation__c FROM State__c]){
        StateMap.put(s.Abbreviation__c , s);
    }
    Map<Id,Country__c> cMap = new Map<Id,Country__c>([SELECT Id, Name FROM Country__c]);
    Map<Id,State__c> sMap = new Map<Id,State__c>([SELECT Id, Name, Abbreviation__c FROM State__c]);
    
    for (Bill62__Address__c a : Trigger.new){
        if(Trigger.isInsert){
            if (a.Bill62__State_Province__c != null && StateMap.containsKey(a.Bill62__State_Province__c)){
                a.State__c = StateMap.get(a.Bill62__State_Province__c).Id;
            } else if (a.State__c != null && sMap.containsKey(a.State__c)) {
                a.Bill62__State_Province__c = sMap.get(a.State__c).Abbreviation__c;
            } 
            
            if (a.Bill62__Country__c != null && CountryMap.containsKey(a.Bill62__Country__c)){
                a.Country__c = CountryMap.get(a.Bill62__Country__c).Id;
            } else if (a.Country__c != null && cMap.containsKey(a.Country__c)){
                a.Bill62__Country__c = cMap.get(a.Country__c).Name;
            }
        }
        if(Trigger.isUpdate){
            if (a.Bill62__State_Province__c != System.Trigger.oldMap.get(a.Id).Bill62__State_Province__c && StateMap.containsKey(a.Bill62__State_Province__c)){
                a.State__c = StateMap.get(a.Bill62__State_Province__c).Id;
            } else if (a.State__c != System.Trigger.oldMap.get(a.Id).State__c && sMap.containsKey(a.State__c)) {
                a.Bill62__State_Province__c = sMap.get(a.State__c).Abbreviation__c;
            } 
            
            if (a.Bill62__Country__c != System.Trigger.oldMap.get(a.Id).Bill62__Country__c && CountryMap.containsKey(a.Bill62__Country__c)){
                a.Country__c = CountryMap.get(a.Bill62__Country__c).Id;
            } else if (a.Country__c != System.Trigger.oldMap.get(a.Id).Country__c && cMap.containsKey(a.Country__c)){
                a.Bill62__Country__c = cMap.get(a.Country__c).Name;
            }
        }
        
    }
}