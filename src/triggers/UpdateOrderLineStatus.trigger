/*
@Name            : UpdateOrderLineStatus
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to convert status values from TC Numbers to text, and vice versa
*/


trigger UpdateOrderLineStatus on Bill62__Subscription__c (before update) {
    /*if(Trigger.IsInsert){    
        for (Bill62__Subscription__c ol: Trigger.New){
            if (ol.Status__c == null && ol.Status_ID__c != null){
                if (ol.Status_ID__c == '-6') ol.Status__c = 'Pending Payment Verification';
                if (ol.Status_ID__c == '-5') ol.Status__c = 'Pending Payment Approval';
                if (ol.Status_ID__c == '-4') ol.Status__c = 'VERIFIED';
                if (ol.Status_ID__c == '-3') ol.Status__c = 'VERIFYING';
                if (ol.Status_ID__c == '-2') ol.Status__c = 'RECORDED';
                if (ol.Status_ID__c == '-1') ol.Status__c = 'SUBMITTED';
                if (ol.Status_ID__c == '0') ol.Status__c = 'PENDING';
                if (ol.Status_ID__c == '10') ol.Status__c = 'ACTIVE';
                if (ol.Status_ID__c == '20') ol.Status__c = 'REWORK';
                if (ol.Status_ID__c == '40') ol.Status__c = 'RESUBMIT';
                if (ol.Status_ID__c == '50') ol.Status__c = 'SHIPPED';
                if (ol.Status_ID__c == '60') ol.Status__c = 'CLOSED';
                if (ol.Status_ID__c == '70') ol.Status__c = 'SUSPENDED';
                if (ol.Status_ID__c == '80') ol.Status__c = 'PROVISIONING';
                if (ol.Status_ID__c == '90') ol.Status__c = 'BACKORDERED';
                if (ol.Status_ID__c == '91') ol.Status__c = 'UNDELIVERABLE';
                if (ol.Status_ID__c == '95') ol.Status__c = 'RMA HOLD';
                if (ol.Status_ID__c == '96') ol.Status__c = 'RMA TESTED GOOD';
                if (ol.Status_ID__c == '99') ol.Status__c = 'PURGED';
                if (ol.Status_ID__c == '100') ol.Status__c = 'FAST PURGE';
                if (ol.Status_ID__c == '101') ol.Status__c = 'MANUAL PURGE';
                if (ol.Status_ID__c == '103') ol.Status__c = 'CHARGEBACK PURGE';
            }
            if (ol.Status_ID__c == null && ol.Status__c != null){
                if (ol.Status__c == 'Pending Payment Verification') ol.Status_ID__c = '-6';
                if (ol.Status__c == 'Pending Payment Approval') ol.Status_ID__c = '-5';
                if (ol.Status__c == 'VERIFIED') ol.Status_ID__c = '-4';
                if (ol.Status__c == 'VERIFYING') ol.Status_ID__c = '-3';
                if (ol.Status__c == 'RECORDED') ol.Status_ID__c = '-2';
                if (ol.Status__c == 'SUBMITTED') ol.Status_ID__c = '-1';
                if (ol.Status__c == 'PENDING') ol.Status_ID__c = '0';
                if (ol.Status__c == 'ACTIVE') ol.Status_ID__c = '10';
                if (ol.Status__c == 'REWORK') ol.Status_ID__c = '20';
                if (ol.Status__c == 'RESUBMIT') ol.Status_ID__c = '40';
                if (ol.Status__c == 'SHIPPED') ol.Status_ID__c = '50';
                if (ol.Status__c == 'CLOSED') ol.Status_ID__c = '60';
                if (ol.Status__c == 'SUSPENDED') ol.Status_ID__c = '70';
                if (ol.Status__c == 'PROVISIONING') ol.Status_ID__c = '80';
                if (ol.Status__c == 'BACKORDERED') ol.Status_ID__c = '90';
                if (ol.Status__c == 'UNDELIVERABLE') ol.Status_ID__c = '91';
                if (ol.Status__c == 'RMA HOLD') ol.Status_ID__c = '95';
                if (ol.Status__c == 'RMA TESTED GOOD') ol.Status_ID__c = '96';
                if (ol.Status__c == 'PURGED') ol.Status_ID__c = '99';
                if (ol.Status__c == 'FAST PURGE') ol.Status_ID__c = '100';
                if (ol.Status__c == 'MANUAL PURGE') ol.Status_ID__c = '101';
                if (ol.Status__c == 'CHARGEBACK PURGE') ol.Status_ID__c = '103';
            }
        }
    }*/
    
    if(Trigger.IsUpdate){    
        for (Bill62__Subscription__c ol: Trigger.New){
            if (ol.Status_ID__c != System.Trigger.OldMap.get(ol.Id).Status_ID__c){
                if (ol.Status_ID__c == '-6') ol.Status__c = 'Pending Payment Verification';
                if (ol.Status_ID__c == '-5') ol.Status__c = 'Pending Payment Approval';
                if (ol.Status_ID__c == '-4') ol.Status__c = 'VERIFIED';
                if (ol.Status_ID__c == '-3') ol.Status__c = 'VERIFYING';
                if (ol.Status_ID__c == '-2') ol.Status__c = 'RECORDED';
                if (ol.Status_ID__c == '-1') ol.Status__c = 'SUBMITTED';
                if (ol.Status_ID__c == '0') ol.Status__c = 'PENDING';
                if (ol.Status_ID__c == '10') ol.Status__c = 'ACTIVE';
                if (ol.Status_ID__c == '20') ol.Status__c = 'REWORK';
                if (ol.Status_ID__c == '40') ol.Status__c = 'RESUBMIT';
                if (ol.Status_ID__c == '50') ol.Status__c = 'SHIPPED';
                if (ol.Status_ID__c == '60') ol.Status__c = 'CLOSED';
                if (ol.Status_ID__c == '70') ol.Status__c = 'SUSPENDED';
                if (ol.Status_ID__c == '80') ol.Status__c = 'PROVISIONING';
                if (ol.Status_ID__c == '90') ol.Status__c = 'BACKORDERED';
                if (ol.Status_ID__c == '91') ol.Status__c = 'UNDELIVERABLE';
                if (ol.Status_ID__c == '95') ol.Status__c = 'RMA HOLD';
                if (ol.Status_ID__c == '96') ol.Status__c = 'RMA TESTED GOOD';
                if (ol.Status_ID__c == '99') ol.Status__c = 'PURGED';
                if (ol.Status_ID__c == '100') ol.Status__c = 'FAST PURGE';
                if (ol.Status_ID__c == '101') ol.Status__c = 'MANUAL PURGE';
                if (ol.Status_ID__c == '103') ol.Status__c = 'CHARGEBACK PURGE';
            }
            if (ol.Status__c != System.Trigger.OldMap.get(ol.Id).Status__c){
                if (ol.Status__c == 'Pending Payment Verification') ol.Status_ID__c = '-6';
                if (ol.Status__c == 'Pending Payment Approval') ol.Status_ID__c = '-5';
                if (ol.Status__c == 'VERIFIED') ol.Status_ID__c = '-4';
                if (ol.Status__c == 'VERIFYING') ol.Status_ID__c = '-3';
                if (ol.Status__c == 'RECORDED') ol.Status_ID__c = '-2';
                if (ol.Status__c == 'SUBMITTED') ol.Status_ID__c = '-1';
                if (ol.Status__c == 'PENDING') ol.Status_ID__c = '0';
                if (ol.Status__c == 'ACTIVE') ol.Status_ID__c = '10';
                if (ol.Status__c == 'REWORK') ol.Status_ID__c = '20';
                if (ol.Status__c == 'RESUBMIT') ol.Status_ID__c = '40';
                if (ol.Status__c == 'SHIPPED') ol.Status_ID__c = '50';
                if (ol.Status__c == 'CLOSED') ol.Status_ID__c = '60';
                if (ol.Status__c == 'SUSPENDED') ol.Status_ID__c = '70';
                if (ol.Status__c == 'PROVISIONING') ol.Status_ID__c = '80';
                if (ol.Status__c == 'BACKORDERED') ol.Status_ID__c = '90';
                if (ol.Status__c == 'UNDELIVERABLE') ol.Status_ID__c = '91';
                if (ol.Status__c == 'RMA HOLD') ol.Status_ID__c = '95';
                if (ol.Status__c == 'RMA TESTED GOOD') ol.Status_ID__c = '96';
                if (ol.Status__c == 'PURGED') ol.Status_ID__c = '99';
                if (ol.Status__c == 'FAST PURGE') ol.Status_ID__c = '100';
                if (ol.Status__c == 'MANUAL PURGE') ol.Status_ID__c = '101';
                if (ol.Status__c == 'CHARGEBACK PURGE') ol.Status_ID__c = '103';
            }
        }
    }
}