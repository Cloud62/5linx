/*
@Name            : ShippingChangeCheck 
@Author          : customersuccess@cloud62.com
@Date            : September 10, 2014
@Description     : Disallows change to Shipping Address on Order if:
                        1) The Product Type is Wellness AND
                        2) Any of the Order Lines Status is PROVISIONING or BACKORDERED
@Revision History: 
*/

trigger ShippingChangeCheck on Bill62__Order__c (after update) {
    if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
    if(C62Utilities.cSetExecuted.contains('dontFireTrigger')) return;
    //C62Utilities.cSetExecuted.add('dontFireTrigger');
    Set<Id> orderIds = New Set<Id>();
    //Product_Type__c pt = [SELECT Id FROM Product_Type__c WHERE Name = 'Wellness'];
    for ( Bill62__Order__c o : Trigger.New ){
        if ( o.Shipping_Address__c != System.Trigger.oldMap.get(o.id).Shipping_Address__c && o.NOP_ID__c != null){ // && o.Product_Type__c != null && o.Product_Type__c == pt.Id){
            orderIds.add(o.Id);
        }
    }
    
    Map<Id, List<Bill62__Subscription__c>> olMap = New Map<Id, List<Bill62__Subscription__c>>();
    for (Bill62__Subscription__c ol : [SELECT Id, Order__c, Status__c FROM Bill62__Subscription__c WHERE Order__c IN :orderIds AND (Status__c = 'PROVISIONING' OR Status__c = 'BACKORDERED')]){
        if (olMap.get(ol.Order__c) == null){
            olMap.put(ol.Order__c, New List<Bill62__Subscription__c>());
        }
        olMap.get(ol.Order__c).add(ol);
    }
    
    for(Bill62__Order__c o : Trigger.New){
        if ( olMap.get(o.id) != null && olMap.get(o.id).size() > 0){
            o.AddError('Cannot change the Shipping address if one or more order lines is PROVISIONING or BACKORDERED.');
        }
    }
}