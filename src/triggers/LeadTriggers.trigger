trigger LeadTriggers on Lead (before insert, before update, before delete, after insert, after update) {

    Messaging.SingleEmailMessage mail;
    Messaging.SingleEmailMessage[] mails = new Messaging.SingleEmailMessage[]{};

    List<RecordType> leadRecordTypes = [select Id, Name from RecordType where sObjectType = 'Lead' and DeveloperName in ('X921', 'X922')];
    Set<Id> leadRecordTypeIds = new Set<Id>();
    for(RecordType rt : leadRecordTypes) leadRecordTypeIds.add(rt.Id);

    List<Lead> triggerLeads;
    if(Trigger.isDelete) triggerLeads = Trigger.old;
    else triggerLeads = Trigger.new;

    if(Trigger.isAfter) {
        
        // map of old and new RINs, checking for changes  
        Map<String, String> changedRINs = new Map<String, String>();
          
        // loop through and handle email delivery   
        for(Lead l : triggerLeads) {
        
            if(Trigger.isUpdate && l.RIN__c != Trigger.oldMap.get(l.Id).RIN__c) {
                changedRINs.put(Trigger.oldMap.get(l.Id).RIN__c, l.RIN__c);
            }
        
            // filter by Record Type
            if(!leadRecordTypeIds.contains(l.RecordTypeId)) continue;
        
            List<String> emails = new List<String>();
        
            String leadEmail = l.Email;
            if(leadEmail != null) emails.add(leadEmail);
            
            String rinEmail = l.RIN_Email__c;
            if(leadEmail == null) emails.add(rinEmail);
        
            if(emails.size() == 0) continue;
        
            mail = new Messaging.SingleEmailMessage();
            mail.setSenderDisplayName('5Linx');
            mail.setReplyTo('info@5linx.com');
            // mail.setToAddresses(new String[] {'tom@redargyle.com'}); // for testing
            mail.setToAddresses(emails); // for production: lead's email and RIN's email
          
            if(Trigger.isInsert) {
                // send "created" email
                //mail.setSubject('Salesforce Lead created from RIN#' + l.RIN__c);
                //mail.setPlainTextBody('Salesforce Lead created from RIN#' + l.RIN__c);
            }
            
            else if(Trigger.isUpdate) {
                // send "updated" email
                mail.setSubject('Salesforce Lead updated (RIN#' + l.RIN__c + ')');
                mail.setPlainTextBody('Salesforce Lead updated (RIN#' + l.RIN__c + ')');
            }
            
            else if(Trigger.isDelete) {
                // send "removed" email
                mail.setSubject('Salesforce Lead deleted (RIN#' + l.RIN__c + ')');
                mail.setPlainTextBody('Salesforce Lead deleted (RIN#' + l.RIN__c + ')');
            }
            
            else {
                continue;
            }
            
            mails.add(mail);
    
        }
        
        // update RIN objects where applicable
        List<RIN__c> rins = [select Id, RIN__c from RIN__c where RIN__c in :changedRINs.keySet()];
        
        for(RIN__c r : rins) r.RIN__c = changedRINs.get(r.RIN__c);
        update rins;
        
        try {
            if(mails.size() > 0) Messaging.sendEmail(mails);
        }
        catch(Exception ex) {
            system.debug(ex);
        }

    }

    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
          
        // assign the RIN SFID to the lookup field based on the RIN Number field
          
        Set<String> rinIds = new Set<String>();
        
        for(Lead l : Trigger.new) {
            if(l.RIN__c == null) continue;
            rinIds.add(l.RIN__c);
        }  
        
        List<RIN__c> rins = [select Id, RIN__c from RIN__c where RIN__c in :rinIds];
        
        // create a map, using the RIN Number as the key
        Map<String, RIN__c> rinMap = new Map<String, RIN__c>();
        
        for(RIN__c r : rins) rinMap.put(r.RIN__c, r);
        
        // assign RIN IDs to Leads   
        for(Lead l : Trigger.new) {
            if(l.RIN__c == null || !rinMap.containsKey(l.RIN__c)) continue;
            l.RIN_Contact__c = rinMap.get(l.RIN__c).Id;
        }
        
    }

}