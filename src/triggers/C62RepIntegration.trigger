/*
@Name            : C62RepIntegration
@Author          : customersuccess@cloud62.com
@Date            : December 22, 2014
@Description     : Trigger to handle Rep integration to 5Linx TC
*/


trigger C62RepIntegration on Account (before update) {    
    if (RunTrigger__c.getInstance(UserInfo.GetUserID()).NoFire__c == true) return;
    if(C625LinxAPIHelper.IsInsert == true) return;
    if(C62Utilities.cSetExecuted.contains('dontFireTrigger') || C62Utilities.cSetExecuted.contains('paymentBatch')) return;
    Map<Id, Marketing_Plan__c> mpIDmap = new Map<Id, Marketing_Plan__c>([SELECT Id, Marketing_Plan_ID__c FROM Marketing_Plan__c]);
    Map<Id, Position__c> posIDmap = new Map<Id, Position__c> ([SELECT Id, Position_ID__c FROM Position__c]);
       
    for (Account a : Trigger.New){
        // If the position has changed, call createPositionHistory
        if(a.Position__c != trigger.oldmap.get(a.Id).Position__c){
            integer entityid;
            if (a.entity_id__c != null){
                entityid = integer.valueof(a.entity_id__c);
            }
            integer position;
            if (a.Position__c != null){
                position = integer.valueof(posIDmap.get(a.Position__c).Position_ID__c);
            }
            integer qualposition;
            if (a.Position_Qualified__c != null){
                qualposition = integer.valueof(posIDmap.get(a.Position_Qualified__c).Position_ID__c);
            }
            integer marketplan;
            if (a.Marketing_Plan__c != null){
                marketplan = integer.valueof(mpIDmap.get(a.Marketing_Plan__c).Marketing_Plan_ID__c);
            }
            C625LinxAPIHelper.createPositionHistory(entityid, position, qualposition, 11691, marketplan, a.id);            
        }
        
        if(a.Pay_Method__c != System.Trigger.oldMap.get(a.Id).Pay_Method__c ){
            if (a.Pay_Method__c == 'ACH') a.Is_Direct_Deposit__c = true;
            if (a.Pay_Method__c == 'Pay Card') a.Is_Direct_Deposit__c = false;
        }
        
        C625LinxAPIHelper.UpdateRep(string.valueof(a.Id));
    }
}